# Giuseppe Ungaretti

È un poeta che tiene conto della *lezione delle avanguardie*; in Italia il Futurismo.

In letteratura si hanno come conseguenze:

* Sintassi che svanisce, così come la punteggiatura e il metro

Grazie al manifesto del futurismo letterario.

## Biografia

Nasce ad Alessandria d'Egitto nel 1888, studia a Parigi, centro culturale del '900, fondamentale perché incontra dei poeti francesi e va in contatto con la cultura del tempo.

La prima raccolta si chiama "Il porto sepolto" nel 1916, già influenzata dall'esperienza della Grande Guerra, dato che egli fu volontario e combatté al Carso.

Il porto sepolto rappresenta come nome ciò che è nascosto dalla poesia.

Le Poesie de "Il porto sepolto" seguono una poetica "Della Parola" (è stato considerato comunque un poeta ermetico) altre raccolte in "allegria dei naufragi".

Allegria dei Naufragi è un ossimoro come il "Tacito Tumulto" di Pascoli in "Il Lampo".

I termini (Allegria e Naufragi) sono spiegati dal poeta, l'allegria è l'esultanza di un attimo, mentre il naufragio è la morte, il suo effetto distruttivo.

Nelle edizioni successive continua a elaborare le poesie scritte nelle raccolte precedenti.

La raccolta poetica finale "Allegria" è del 1931, il titolo sottilinea più l'aspetto positivo rispetto all'edizione precedente.

L'altra opera importante di Ungaretti è "Sentimento del Tempo" 1933, dove nella forma recupera una struttura sintattica e il metro tradizionale (Petrarca e Leopardi come modelli). 

Il tempo distrugge e ricostruisce.

I racconti successivi al 1933 sono raccolti in "Il Dolore", è morto il figlio ed il fratello e un senso di angoscia data dall'instaurarsi dei totalitarismi.

Infine c'è stata una raccolta totale chiamata "Vita di un Uomo".

### Poetica della parola

La Parola è scelta, concentrata e carica di significati; non si può togliere neanche un termine senza perdere significato.

Lo stesso titolo è fondamentale, utile e necessario per comprendere l'intero testo.

È una poetica Chiara ed Esplicita.

#### Retorica

```mermaid
graph TB;
    A[Analogia]-->B[Similitudine];
    A-->C[Metafora];
```

## San Martino del Carso

*Di* queste case
**Non è rimasto**
*Che* qualche
**Brandello** di muro
*Di* tanti
*Che* mi corrispondevano
**Non è rimasto**
Neppure tanto
Ma nel **cuore**
Nessuna croce manca
È il mio **cuore**
Il paese più straziato

Forte analogia tra il cuore e il paese.

## I Fiumi

Parla anche della vita del poeta, parla dei "fiumi della sua vita".

Egli si ritrova a contare i suoi fiumi sull'Isonzo, dove ha combattuto.

Il poeta è diventato un sasso mentre il fiume ha delle mani nascoste che accarezzano e levigano il poeta regalandogli una "rara felicità".

## Veglia

Un’intera nottata
buttato vicino
a un compagno
massacrato 
con la sua bocca
digrignata 
volta al plenilunio
con la congestione 
delle sue mani
penetrata
nel mio silenzio
ho scritto
lettere piene d’amore.
Non sono mai stato
tanto
attaccato alla vita



Buttato - Come una cosa.

Per la poesia Veglia si parla di Espressionismo, grazie agli aggettivi molto chiari, mani congestionatte, la luna piena.

## Soldati

Si sta come
d'autunno
sugli alberi
le foglie

Fragilità - Caducità

```mermaid
graph LR
    A[Fragilità] --> C[Condizione del Soldato]
    B[Caducità] --> C
```

## In Memoria

## Mattino

