'''
source.py
Plotting the necessary graphs
'''
from matplotlib import pyplot as plt
import numpy as np

def slr(x_p):
    return 1.743 * x_p + 8.3995

if __name__ == '__main__':
    x = np.linspace(1, 6, 6, endpoint = True)
    y = [10, 13, 15, 11, 18, 20]

    x_slr = np.linspace(0, 15, 100, endpoint = True)
    y_slr = slr(x_slr)

    plt.scatter(x, y, c="g", alpha=0.5)
    plt.plot(x_slr, y_slr)
    plt.xlabel("Numero di Semestre")
    plt.ylabel("Numero di Dipendenti")
    plt.legend(loc=2)
    plt.grid(b=True, which='both', color='0.65',linestyle='-')

    plt.show()
