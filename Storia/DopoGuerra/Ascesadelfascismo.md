# Storia

| Anni        | Italia | USA  | Germania | URSS |
| ----------- | ------ | ---- | -------- | ---- |
| Dopo Guerra |        |      |          |      |
| Anni '20    |        |      |          |      |

Mussolini nasce come socialista.

Ascesa del Fascismo

* 1919 - Fasci di Combattimento
* 1921 - Partito Fascista

Nel movimento fascista erano presenti sopratutto gli Arditi (soldati di prima linea della prima guerra mondiale).

1921 - Nasce il PCI (Partito Comunista Italiano)

Formato da Antonio Gramsci (filosofo, politico, linguista), scrisse "Le Lettere Dal Carcere" e fu il fondatore de L'Unità, giornale ufficiale del PCI. 

## Elezioni del 1921

Paura del "contagio rosso", i fascisti si inserirono nelle liste liberali e presero 35 sedute in parlamento, cambiando anche radicalmente gli obiettivi fascisti.

Il PNF (Partito NazionalFascista) venne supportato molto dai Borghesi Medio-Alti.

### 1922 - Marcia su Roma



### 1924 - Assassinio di Matteotti

Nel Novembre 1923 si approva una nuova legge elettorale, la "Legge Acerbo", che introdusse un sistema maggioritario secondo cui la lista che otteneva almeno il 25% con maggioranza relativa prendeva i 2/3 delle sedute parlamentarie.

Giacomo Matteotti denuncia le scorrettezze avvenute durante l'elezione del '23; sparito Matteotti avviene il "Ritiro Sull'Aventino" dove l'opposizione abbandona Montecitorio.

### 1925 - 1926 - Leggi Fascistissime

## Trattato di Versailles

Nasce la società delle Nazioni al fine di arginare la soppraffazione da parte di stati su altri stati, inefficientemente.

1933 - Hitler prende il potere.

## 1935 - Leggi di Norimberga

## Guerra di Etiopia 1935 - 1936

L'Italia attacca l'Etiopia che comunque doveva essere tutelata dalla Società delle Nazioni, essa sanziona leggermente l'Italia. Questo porta comunque all'isolamento dell'Italia.

## 1936

Asse Roma-Berlino che evolve poi nel Patto D'Acciaio e Guerra Civile Spagnola.

## 1938

Leggi Raziali e Anschluss. Trattato di Monaco.

Pag: 123 - 171. 

2 Domande:

* Illustra le fasi che portarono alla nascita dell'Unione Sovietica.
* Analizza le cause della crisi del 1929.

