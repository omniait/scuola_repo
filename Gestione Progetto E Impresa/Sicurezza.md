# Sicurezza

## Diritti e Doveri

I Lavoratori hanno diritto ad un luogo di lavoro che rispetta le norme, hanno il dovere di partecipare alla formazione, seguire le norme del datore di lavoro e di utilizzare i dispositivi di sicurezza. Il lavoratore ha il dovere di segnalare o al responsabile per la sicurezza o al datore di lavoro eventuali carenze nel sistema Sicurezza.

I datori di lavoro hanno il dovere di considerare salute e sicurezza del lavoratore, valutare il rischio e prevenirlo tramite delle misure di prevenzione degli infortuni.

Le Amministrazioni pubbliche devono inoltre promuovere e finanziare campagne di comunicazione e formazione per realizzare la tutela negli ambienti di lavoro.

## Normativa

- Prevenzione: disposizioni e misure necessarie secondo la peculiarità del lavoro, l'esperienza e la tecnica, per evitare o diminuire i rischi.
- Salute: Stato di completo benessere fisico, mentale e sociale.
- Pericolo: Proprietà o qualità intrinseca di un determinato fattore che può creare danni.
- Rischio: Probabilità di raggiungimento del livelllo di danno nelle condizioni di impiego o esposizione ad un fattore o agente o entrambi.
- Ergonomia: Tecniche, strumenti e accorgimenti che creano un ambiente di lavoro sicuro e confortevole.

In Italia le norme sono raccolte nel Testo Unico della sicurezza (Decreto legislativo n81 del 2008). 

## Figure

* Datore di Lavoro: il soggetto titolare del rapporto di lavoro col lavoratore o comunque colui che ha poteri decisionali e di spesa. Nelle pubbliche Amministrazione il datore di lavoro è il dirigente a cui spettano i poteri di gestione.
* Responsabile del Servizio di Prevenzione e Protezione dai Rischi (RSPP): Persona incaricata dal datore di lavoro per coordinare il servizio di prevenzione e protezione dai rischi, può coincidere col datore di lavoro.
* Rappresentante dei Lavoratori per la Sicurezza (RLS): Persona designata o eletta per rappresentare i lavoratori per quanto concerne gli aspetto della salute e della sicurezza.
* Medico Competente: Medico che collabora col datore di lavoro al fine di valutare i rischi ed effettua sorveglianza sanitaria
* Addetti al servizio di prevenzione e protezione: Persone addette alle emergenze.

## Prevenzione degli infortuni e Valutazione dei rischi

