# Avanguardie

Rottura di ogni regola, rottura con le epoche precedenti, di tutte le arti.

Si vuole spazzare il passato per inaugurare una nuova epoca, si collocano all'inizio del 1900.

Si parla di Cubismo, nella pittura, grazie a Picasso e Braque dove le forme si scompongono.

## Futurismo

### Italiano

* Filippo Tommaso Marinetti - Pittore e Scultore, autore del manifesto del futurismo.
* Aldo Palazzzeschi - Scrittore.

#### Manifesto Del Futurismo

È una dichiarazione pubblica di intenti e valori.

Pubblicato sul quotidiano "Le Figarò" nel 1909.

Parole e Concetti Chiave:

* Dinamicità, Velocità
* Aggressione, Lotta, Ribellione
* "Glorificare la guerra - sola igiene del mondo" - Collegamento col nazionalismo
* Rivolta, Odio della Cultura del Passato
* Esaltazione della modernità

#### Manifesto Tecnico della letteratura del futurismo

È la spiegazione delle conseguenze del Manifesto del Futurismo sulla scrittura.

Questo indica quindi ricadute sintattiche (organizzazione del periodo e della frase) e metriche.

Grammaticale - Verbo all'Infinito. Spariscono Avverbi, Aggettivi, Punteggiatura

Analogie più vaste.

### Russo



## Dadaismo

Nasce a Zurigo da Tristan Tzara e un gruppo di artisti, la stessa parola Dada non vuol dire nulla e questa è una caratteristica del movimento, cioè non dar significato.

## Metafisica

De Chirico è l'inventore di questo stile che però si oppone ai concetti futuristi di dinamismo e velocità, si parla di staticità e immobilità.

Importante l'uso della prospettiva che si piega.

## Surrealismo

Il Surrealismo si occupa dei tempi del sogno e dell'inconscio, si cerca una surrealtà.

Emblematici Juan Mirò e Salvador Dalì.

## Astrattismo

Kandisnkij.