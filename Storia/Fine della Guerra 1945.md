# Fine della Guerra 1945

1949 : NATO

1955 : Patto di Varsavia

URSS - USA

Momento in cui i rapporti tra questi paesi hanno avuto rapporti migliori:

* Coesistenza Pacifica di URSS e USA
* I primi segni di Disgelo

## Paesi satellite URSS

* Cecoslovacchia
* Germania-Est (DDR - Demokratitsche Deutsche Republik)
* Ungheria
* Romania
* Bulgaria
* Albania
* Polonia

## Jugoslavia

Tito - Cerca la via "nazionale" al comunismo, prendendo le distanze.

## Kruscev

Finita la guerra Coreana escono di scena Stalin e Truman (Dall'Aprile 1945 presidente americano).

1953 - Muore Stalin

Riconosce l'indipendenza e la neutralità dell'Austria ed inizia la coesistenza pacifica tra i due stati.

### Fatti di Ungheria 1956

In Ungheria ed in Polonia ci sono delle rivolte, che vengono represse dall'URSS.

Italo Svevo rinunciò alla tessera del PCI.

* Muro di Berlino 1961 - 1989
* Crisi di Cuba - Installazioni missilistiche

## USA

### MCarty

Caccia alle streghe - Assassinio dei comunisti

