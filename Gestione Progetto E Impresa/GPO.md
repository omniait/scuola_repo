# GPO

## Sistema Informativo Aziendale

Insieme di dati e flussi che riguardano la raccolta, la produzione, l'archiviazione, l'elaborazione e la distribuzione dei dati nelle attività operative e di controllo dell'azienda.

I dati possono provenire anche dall'esterno.

I dati sono una materia prima in continua crescita, raccolta in archivi e distribuita ai clienti.

Le informazioni sono il valore dei dati, diventano il supporto per i processi operativi.

La conoscenza è messa a disposizione a chi prende decisioni ed è la finalità del SIA (Sistema Informativo Aziendale).

La scienza che si occupa di studiare i dati per ricavarne informazioni e conoscenza al fine di ottimizzare il profitto si chiama Data Science.

## Funzioni Aziendali

L'impresa è organizzata in funzioni aziendali. Sono principalmente 5:

* Logistica
* R&D (produzione)
* Amministrazione, finanza e controllo
* HR
* Marketing, Vendite e Servizi post-vendita

Organigramma: Diagramma che rappresenta un'organizzazione gerarchico-funzionale.

## Processi Aziendali

Il processo è lo svolgimento di un'attività aziendale nel tempo. Ha obiettivi precisi da rispettare e degli ingressi definiti.

## Sistema informatico

È l'insieme delle risorse messe a disposizione dalla tecnologia (SW e HW) e dalle persone che utilizzano questa tecnologia al fine di automatizzare l'archiviazione, la produzione, l'elaborazione e la distribuzione dei dati.

* Grazie all'informatica si possono inserire e condividere i dati
* Si rispettano le norme UE
* I dipendenti devono capire come funzionano le applicazioni

Gli aspetti applicativi riguardano:

* L'automazione dei lavori d'ufficio (Office Automation) e la dematerializzazione che ne consegue
* Implementazione dei Database
* L'utilizzo di strumenti informatici da parte di tutti

## Outsourcing

Outsourcing: l'azienda si affida ad altre aziende per svolgere alcune parti gestionali. 

Ci sono due tipi di Outsourcing: Globale (Tutto il processo) o Selettiva (Alcune attività).

## Soluzioni Informatiche

La soluzione informatica è la costruzione o riorganizzazione di un sistema informatico che risponde alle esigenze attuali e prevede quelle future.

Le applicazioni necessitano di due caratteristiche:

* Interoperabilità: Comunicazione tra applicazioni locali e remote
* Portabilità: Possibilità d'operare tra piattaforme HW diverse

## ERP - Enterprise Resource Planning

Sono le soluzioni SW che gestiscono in modo ottimizzato e bilanciato il patrimonio delle risorse aziendali.

I Sistemi ERP usano l'interoperabilità per ottimizzare i tempi, inoltre devono essere modulari perché questi moduli sono impiegati in attività diverse, come Produzione e Logistica.

I produttori di SW ERP inoltre considerano la specificità dell'azienda che può operare in campi industriali diversi (chimica, tessile); garantiscono anche l'integrazione coi SW di altre aziende grazie ad un rapporto di Partnership.

Un sistema ERP è complesso perché necessità di un'analisi approfondita di tutte le procedure aziendali ed eventualmente una riformulazione di queste.