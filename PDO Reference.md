<style> .page-break { page-break-after: always; } </style> 

# PHP

## ini_set

```php
// dev
ini_set('display_errors', 1);
// prod
ini_set('display_errors', 0);
ini_set('log_errors', 1);
```

## Switch / If - Else

```php
if(condition){
	...
} else {
	...
}
switch(var) {
	case value:
        break;
    default:
        break;
}
```

## Cicles

```php
while($bool)
{
    ...
```

```php
do
{
    ...
}while($bool)
```

```php
for($i = 0; $i < $max; $i++)
{
    ...
```

```php
foreach($array as $el)
{
    ...
```

## Try Catch

```php
try 
{
    ...
} 
catch (Exception $e) 
{
    ...
}
```

## Arrays

```php
// numeric
$arr[0] = value;
$arr[1] = value;
// assoc
$arr['key1'] = value;
$arr['key2'] = value;
```

## Array Functions

```php
$exist = in_array($el, $array);
$pos = array_search($el, $array);
array_splice($array, $start, $len);
$arr = sort($arr); // +
$arr = rsort($arr); // -
```

<span class='page-break' /> 

## Classes

```php
class ClassName extends AnotherClass
{
    public, protected, private $vars;
    // method 1
    public __construct($vars)
    {
        ...
    }
    // method 2
    public ClassName($vars)
    {
        ...
    }
    public fun($param)
    {
        ...
    }
}

$Obj = new ClassName($vars);
$var = $Obj->$vars;
```

## Session

```php
session_start();
$_SESSION['key'] = value;
...
session_unset();
session_destroy(); 
```

## Other

```php
require_once(path);
include_once(path);
rand(min, max);
echo "string";

> html
<form action="path" method="TYPE">
	<input type="type" name="name">
	<input type="submit">
</form>
        
> php
$var = $_METHOD[name];
```

# PDO Reference

## Login

```php
$pdo = new PDO('mysql:host=localhost; port=3305; dbname=db', "username", "password");
$pdo->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
```

## Try Catch

```php
catch(PDOException $e)
{
    $err = $e->getMessage();
}
```

## DB Query

```php
// TYPE = EMAIL, NUMBER_INT, NUMBER_FLOAT, STRING, URL
$prepared = filter_var($string, FILTER_SANITIZE_TYPE);

$query = "
	SELECT attribute
	FROM db.table
";
$r = $pdo->exec($query);
```

## Prepare & Execute

```php
$st = $pdo->prepare("
	INSERT INTO family (id, name)
	VALUES (:id, :name)
	");
// NULL, BOOL, INT, STR, LOB
$stmt->bindParam(':id', $id_value, PDO::PARAM_TYPE);
$stmt->bindParam(':name', $name_value);
$stmt->execute();
// NUM ASSOC BOTH OBJ LAZY
$r = $stmt->fetch_all(PDO::FETCH_TYPE);
```

## Rows Count

```php
$st = $db->prepare('DELETE FROM ... WHERE ...');
$st->execute(array('Fredo'));
echo "Deleted rows: " . $st->rowCount();

$st = $db->query('SELECT ... FROM ...');
$all= $st->fetchAll(PDO::FETCH_COLUMN, 1);
echo "Retrieved ". count($all) . " rows";
```

# MySQL

## Data Types

```mysql
VARCHAR (Lenght 0-255)
TEXT
LONGTEXT
INT (Lenght 0-255)
BIGINT 
FLOAT
DOUBLE
DECIMAL (Lenght, AfterPoint) [UNSIGNED, ZEROFILL]
SET (Array)
```

## Create

```mysql
CREATE DATABASE dbname
CREATE TABLE dbname.tname
(
    id int NOT NULL PRIMARY KEY AUTO_INCREMENT,
    col1 data_type options,
    ...
)
```

## Delete

```mysql
DELETE FROM tname
WHERE col=val
```

## Drop

```mysql
DROP DATABASE dbname
DROP TABLE tname
```

## Select

```mysql
SELECT cols
FROM tname1
INNER, LEFT, FULL JOIN tname2
ON tname1.pk=tname2.pl
WHERE condition
```

## Where

```mysql
BETWEEN val1 AND val2
IN (val1,val2,..)
col LIKE (pattern)
```

