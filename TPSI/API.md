# API

## Applicazioni di Rete

Ci si riferisce a un insieme di programmi eseguiti contemporaneamente su almeno 2 computer con risorse comuni (database).

I processi devono comunicare tra più pc anche se la rete che li include non è locale e pertanto è necessaria un'identificazione univoca tramite IP e numero di porta dell'applicazione, il socket.

Il Socket è pertanto ciò che consente di comunicare attraverso la rete.

## API - Application Programming Interface

Le API sono un insieme di procedure usabili dal programmatore volte alla stesura di applicazioni di rete (per brevità le identificherò con WebApp).

Si pongono tra l'application e il transport layer realizzando l'astrazione tra HW e Developer svincolando quindi i Developer da problemi di comunicazione e trasferimento dati degli strati inferiori.

Un'applicazione di rete è composta da due parti:

* User Agent - Interfaccia tra User e gli aspetti comunicativi.
* Implementazione dei protocolli che permettono all'applicazione di integrarsi con la rete.

In particolare un Browser ha:

* l'interfaccia utente che visualizza i documenti ricevuti dai client, permettendo navigazione e richieste di nuovi documenti.
* Il motore che si preoccupa di inviare le richieste e ricevere le risposte

## Scelta dell'architettura

Il primo passo è la scelta dell'architettura dell'applicazione, le principali sono:

* Client-Server: Il server dev'essere operativo 24h, possibilmente avere un IP Statico Pubblico e offre un servizio, esso attende che almeno un client si connetta per rispondere. Il client può comunicare solo col server; la congestione viene evitata usando una server farm, cioè vi sono più server, possibilmente sparsi geograficamente, che contengono la risorsa richiesta (per esempio AWS offre la creazione di più VPS sparse e si può effettuare il Load Balancing con Elastic Load Balancing, lo stesso servizio è offerto da Microsoft Azure e Google Cloud Platform ma non so come si chiama e non ho motivo di esplorarli). Si può effettuare un'analogia con la CDN (amazon cloudfront).
* P2P: Tutti gli host sono chiamati peer, ogni peer è collegato con tutti gli altri peer, i peer si auto-organizzano e le risorse sono distribuite su ogni peer in modi diversi. Ad oggi questa architettura non è usata molto, per ovvi motivi, ma può essere applicata all'interno di un ecosistema IoE (per esempio rilevatori di umidità per far partire gli spruzzatori).
* P2P Decentralizzato: Ogni peer è sia Client sia Server e le risorse non sono localizzabili tramite IP, vengono infatti individuate nel livello superiore (Application Layer), i peer condividono tutte le risorse: dati, memoria, banda, questo sistema si adatta al cambiamento dei nodi partecipanti senza la necessità di un server/entità centralizzata, un esempio sono le criptovalute come Bitcoin e Ethereum.
* P2P Centralizzato: È presente un server centrale chiamato Directory Server che conserva le informazioni sui peer ma non contiene le informazioni stesse che sono presenti solo sui peer, un esempio è il servizio Torrent.
* P2P Ibrido: Vi sono dei Supernodi che indicizzano, gli altri nodi sono chiamati Leaf Peer.

Considerato che internet è eterogeneo e che i protocolli Transport non possono garantire la presenza di una certa quantità di banda per tutta la durata nasce la necessità di implementare a livello Application flessibile che realizza un circuito virtuale che si adatta.

Alcune applicazioni ammettono piccoli ritardi, come VoIP, videogames, ambienti virtuali, e si ammette un certo ritardo tra le applicazioni.

Il Protocollo TCP garantisce la consegna del pacchetto ma né il TCP né l'UDP sono temporalmente affidabili.

Di conseguenza nasce un terzo Transport Protocol come il RTP che studia i ritardi di rete calibrando gli apparati e i collegamenti garantendo i limiti di tempo prefissati scegliendo alternandoli i due protocolli TCP e UDP.

## XML

L'XML è un Meta markup language, permette di definire altri linguaggi di Markup.

Infatti non serve a programmare e non ha tag predefiniti; XML è un insieme standard di regole sintattiche per modellare la struttura di documenti e dati, descrive la natura dei dati che compongono le informazioni. Unito all'HTML permette di limitare l'HTML a compiti di layout e presentazione. È usato anche per scambiare informazioni leggibili su diversi sistemi di elaborazione.

È stato per molti anni uno standard ma JSON è cresciuto molto rapidamente superandolo.

## JSON

JSON è un formato standard, aperto e usato per immagazzinare informazioni e scambiarle sia tra applicazioni stand-alone che web.

## Confronto

JSON è molto più semplice strutturalmente, caratteristica importante dato che riduce il consumo di banda(!), è più compatto, consente di comprendere il significato dei dati immediatamente, inoltre essendo basato su JS ci si integra direttamente ed è pertanto utilizzabile in tutte le WebApp direttamente ma questo implica una conoscenza pregressa. Il Parsing su JSON è molto più veloce rispetto a quello su XML.

XML però è più versatile e flessibile e si adatta ad ogni situazione, è possibile nidificare nuovi tag immediatamente.

In ogni caso le API REST consentono di ottenere le stesse informazioni sia in formato XML che JSON e per questo spesso si può scegliere indipendentemente quale formato scegliere, per esempio le API Yahoo permettono richieste sia in XML e JSON.

C'è anche da considerare comunque che l'XML ha delle falle di sicurezza (Billion Laughs attack e External Entity attacks).