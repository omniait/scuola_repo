# La Repubblica di Weimar

Nasce dopo la fine della prima guerra mondiale 1918/1919.

La Germania fu indicata come principale responsabile del conflitto, si impone la restituzione dell'Alsazia e Lorena, l'occupazione francese della Saar.

Nasce il NSDAP - NationalSozialistische Deutsche ArbeiterPartei o in breve DAP - Deutsche ArbeiterPartei.

1923 - Tentato di Golpe a Monaco, fallito e scrittura del Mein Kampf, con contenuti razzisti e anti-semitici.

## Ideologia

Anti-Capitalismo e Anti-Plutocrazia; Lotta contro il Socialismo; Rifiuto del sistema parlamentare e della democrazia rappresentativa; Revanscismo (senso di rivincita, sopratutto verso la Francia a causa dell'Alsazia e della Lorena); Antisemitismo.

1926 - Entra la Germania nella Società delle Nazioni

1929 - Crisi americana che si ripercuote in Germania, Crollo delle banche, fallimenti, disoccupazione, etc.

Da qui prendono più potere i partiti estremisti, sia il Partito Comunista sia il NSDAP.

Settembre 1930 - Elezioni

Perdono i social-democratici e non si uniscono contro il NSDAP che guadagna consenso velocemente tra le classi più deboli e nelle parti conservatiste.

Iniziano scontri nei gruppi paramilitari delle due fazioni opposte; SA e SS avevano appreso i metodi delle squadre d'azione del 1919/1920.

1932 - Hindenburg viene eletto come Capo di Stato, nel 1933 Hitler diventa Cancelliere, capo del governo, che smantella la repubblica.

1933 Febbraio - Incendio del Reichstag.

Hitler prende pieni poteri divenendo Cancelliere, vengono messi fuori legge tutti i partiti.

1934 - Notte dei Lunghi coltelli - I vertici delle SA vengono massacrati per evitare potenziali opposizioni interne, muore Hindenburg e Hitler si proclama anche come capo di stato iniziando il Terzo Reich.

1936 - Asse Roma-Berlino.

L'antisemitismo ha molte radici, sia religiose sia pseudo-scientifiche e infine venivano accusati di essere Plutocratici, li si accusava di accumulare la ricchezza.

1) Sacro Romano Impero 800d.C Natale Carlo Magno

2) Impero Germanico Wilhelm II/Bismark

3) Reich Hitler