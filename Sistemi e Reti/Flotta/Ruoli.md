Ruoli (

id_ruolo, PK, INT, AUTO_INCREMENT

descrizione, CHAR[50]

livello, INT

)

Operatori (

id_operatore, PK, INT, AUTO_INCREMENT

nome, CHAR[20]

cognome, CHAR[20]

username, CHAR[15]

password, CHAR[15]

id_ruolo, FK, INT

id_azienda, FK, INT

)

Aziende (

id_azienda, INT, AUTO_INCREMENT

ragione_sociale, CHAR[30]

indirizzo, CHAR[50]

)

Veicoli (

id_veicolo, INT, AUTO_INCREMENT

descrizione, CHAR[50]

targa, CHAR[10]

)

