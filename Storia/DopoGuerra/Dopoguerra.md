Domande

- Illustra le fasi che portarono alla nascita dell'Unione Sovietica.
- Analizza le cause della crisi del 1929.

Le fasi principali che portarono alla formazione dell'Unione Sovietica sono:

* Le rivoluzioni di Febbraio e Ottobre
* L'affermazione della politica Leninista con le Tesi d'Aprile (che sono scritte tra le due rivoluzioni), il Comunismo di Guerra e la NEP

Nella Rivoluzione di Febbraio scoppiò un grande sciopero a Pietrogrado formando i Soviet, ovvero consigli di operai e soldati, questi costringono l'abdicazione di Nikolay II e si forma il governo provvisorio liberale.

La Rivoluzione di Ottobre si ottiene con l'assalto del palazzo d'Inverno e il successivo genocidio degli Zaristi e dell'intera famiglia dello Zar Nikolay II Romanov, si afferma la politica leninista limitando le libertà e fermando l'Assemblea Costituente.

Lenin, leader bolscevico, con le Tesi d'Aprile afferma la necessità di una rivoluzione interrompendo i rapporti con il governo provvisorio.

Nel 1918 si firma la pace di Brest-Litovsk e Lenin vara le prime misure eccezionali raccolte nel Comunismo di Guerra, si limitano libertà economiche (i contadini non possono vendere i prodotti agricoli) e politiche.

Nel 1922 nasce l'URSS contenente i territori dell'Impero Russo in seguito anche alla NEP del 1921 che reintroduce alcune libertà e prosegue la collettivizzazione.

La "Grande Depressione" è una crisi economica del 1929. Per analizzarla bisogna prima fare delle considerazioni.

Anzitutto il Gold Standard, che ancorava le monete alle riserve auree, fu superato nel 1922 col Gold Exchange Standard dove le emissioni di carta moneta potevano esser coperte anche dalle riserve in dollari o sterline, ed inoltre Wall Street andava ad affermarsi sempre di più.

Il potere d'acquisto delle famiglie diminuì mentre la produzione industriale aumentò, le famiglie iniziarono a far ricorso al credito in modo da continuare a comprare, il credito però necessitava di una sicurezza come un'ipoteca sulla casa.

Nel 1929 scoppiò la crisi a Wall Steet, i titoli in borsa crollarono, le banche e le imprese fallirono, e i cittadini si ritrovarono senza casa e senza lavoro. La conseguenza fu l'affermazione di una politica economica non più "Laisser Faire" ma con intervento statale, nacque la politica protezionista.

La crisi negli USA continuò anche fino al 1940, nel 1932 Roosevelt, il neo-presidente democratico, iniziò il programma "New Deal" che tentò di far ripartire la produzione attraverso la costruzione di opere pubbliche. Inoltre furono create delle commissioni di controllo accanto agli investimenti e fu svalutato il dollaro incoraggiando le esportazioni.

# Rivoluzione Sovietica

2 fasi effettive:

* Rivoluzione di Febbraio
* Rivoluzione d'Ottobre

La prima guerra mondiale mise in crisi la Russia, che non riusciva a gestire lo sforzo bellico a causa dell'arretratezza dell'industria.

La Grande Guerra non sembrava mai finire e la crisi agricola provocata dalla guerra, dato che furono mobilitati soldati anche dalle campagne, portò ad una grave crisi alimentare.

Tutto questo portò dal 1916 scioperi e manifestazioni operaie.

Alla fine del Febbraio 1917 (secondo il calendario russo) a San Pietroburgo (allora Pietrogrado/Petrograd) uno scioperò assunse carattere politico, gli operai e le truppe diedero vita ad un Soviet che votò per mettere agli arresti gli zaristi.

Lo Zar Nikolay II fu costretto ad abdicare e si formò un governo provvisorio che però andava contro le idee dei Soviet.

Lenin, leader Bolscevica tornato dall'esilio pubblicò le "Tesi di Aprile" con cui si diede vita al Partito Bolscevico fornendo una strategia politica precisa.

Gli zaristi tentarono un colpo di stato sventato grazie ai Bolscevichi che guadagnarono consenso, grazie al consenso la posizione Bolscevica si affermò e ad Ottobre Lenin iniziò la rivoluzione che portò all'assalto del palazzo d'inverno a San Pietroburgo dove si affermò il governo Leninista.

Nel 1918 la Russia leninista firmò il trattato di pace di Brest-Litovsk ponendo fine alla guerra.

Lenin iniziò il periodo del "Comunismo di guerra", un insieme di misure eccezionali che instaurarono il controllo sull'economia e sulla società russa. Iniziò la collettivizzazione e furono ridotte le libertà civili e anche politiche.

Nel 1921 Lenin vara la NEP reintroducendo parzialmente il libero commercio a seguito di diverse proteste. A differenza delle idee iniziali in Russia si stabilisce un sistema monopartito gerarchico, nel 1922 si forma l'URSS comprendendo praticamente tutti gli ex-stati dell'impero Russo.

# Ascesa del Fascismo

I trattati di pace di Versailles lasciarono l'opinione pubblica italiana insoddisfatta che si aspettava un'espansione nei Balcani, in Africa e l'annessione di Fiume.

Nel 1919 D'Annunzio con un drappello di soldati occupa e "annette" Fiume, il governo resta incerto su come reagire.

Nelle elezioni del '19 vincono il PSI e il Partito Popolare (di matrice Cattolica), alle elezioni si presentano i Fasci di combattimento di Mussolini senza ottenere neanche un seggio.

Il Biennio Rosso del 1919/1920 è un periodo di occupazioni da parte degli operai istigati da socialisti e sindacalisti delle fabbriche che vengono in seguito autogestite dagli stessi creando dei consigli di fabbrica simili ai Soviet.

Nel 1920 viene richiamato Giolitti che fa sgomberare Fiume, il trattato di Rapallo annette Istria e Zara ma rinuncia alla Dalmazia e a Fiume, e attende l'autorisoluzione delle occupazioni delle fabbriche.

Ma nel frattempo i Fasci di combattimento ottengono l'appoggio dei capitalisti, i Fascisti iniziano raid punitivi contro scioperanti, sindacalisti e partiti, vengono distrutte le sedi del PSI e del PP. La violenza fascista viene tollerata da parte del governo e dai liberali.

Nel 1921 si tengono nuove elezioni, i fascisti fan parte della lista liberale di Giolitti, entrano in parlamento con 35 deputati. Mussolini trasforma i Fasci di combattimento nel Partito Nazional Fascista (PNF) e cancella dal programma gli aspetti anti-clericali e socialisti, mantenendo il nazionalismo e l'anti-parlamentarismo.

Il 28 Ottobre del 1922 avviene la Marcia su Roma. Il re Vittorio Emanuele III non proclama lo stato d'assedio e incarica Mussolini di formare un nuovo governo. Il PNF giunto al potere governa con autorità, nel 1924 il PNF prende il 65% dei consensi. 

Il deputato socialista Giacomo Matteotti denuncia le violenze e i brogli elettorali e viene assassinato, in seguito a ciò inizia il "ritiro sull'aventino" dove molti partiti di sinistra formano un nuovo parlamento, però così facendo i fascisti riescono ad affermare completamente il loro potere.

Nel 1925/26 iniziano ad essere emesse le "Leggi Fascistissime" che trasformano l'Italia in uno stato totalitario, sono messi fuori legge i partiti (tranne il PNF), le libertà politiche e d'espressione vengono annientate. La stampa viene censurata, il parlamento non ha piu potere, nasce una polizia di stato (OVRA).

Il fascismo inizia una forte propaganda grazie al controllo sui media e nella scuola. La repressione rende difficile se non impossibile ogni opposizione al Fascismo, molti intellettuali come Gramsci, fondatore dell'Unità che scriverà le famose "Lettere dal Carcere", vengono imprigionati o costretti alla clandestinità o uccisi.

Nel 11 Febbraio 1929 vengono firmati i patti laternansi con la Chiesa sanando la frattura creata dall'Unità.

Non si ottiene un vero e proprio totalitarismo in realtà poiché i borghesi, la chiesa e la monarchia mantengono una certa autonomia dal Regime.

Dopo la crisi del '29 il fascismo si afferma ulteriormente anche in economia, si cerca di ottenere l'Autarchia aiutando le imprese in difficoltà tramite l'IRI e cercando di rendere auto-sufficiente l'Italia.

Nel 1935 Inizia la guerra di Etiopia, la condanna della società delle nazioni allontana l'Italia, che fino a quel momento rimaneva un buoni rapporti, Francia e Gran Bretagna avvicinando quindi la Germania e formazione Asse Roma-Berlino 1936.

A partire dal 1938 vengono attuate le leggi razziali e antisemite. Gli ebrei vengono allontanati dalla società (Enrico Fermi ad esempio Emigra in USA e aiuterà molto la ricerca nucleare) e si inizieranno a costruire i campi di concentramento.