# Interrogazioni
## Italiano

17 - Giovedi
Martinenghi
Mano
De Felice
Bocci

19 - Sabato
Guastapaglia
Scannagatti
Luca Pardini

24 - Giovedi
Ilie
Boccuto
Natali
Gabriele Pardini

26 - Sabato
Wyrebek
Leynes
Romano

## Storia
18 - Venerdi
Gabriele Pardini
Natali
Boccuto
Ilie

25 - Venerdi
Scannagatti
Luca Pardini
Guastapaglia

28 - Lunedi
Martinenghi
Mano
De Felice
Bocci

31 - Giovedi
Wyrebek
Leynes
Romano

## Argomenti Italiano
Ungaretti e Montale, vita, opere (ossi di seppia, le occasioni, la bufera e altro, satura), nobel per la letteratura.

Ossi di Seppia - titolo, motivo dell'aridità, connessione con Leopardi, importante la poetica e le soluzioni stilistiche.

## Argomenti Storia
2a guerra mondiale

Guerra fredda