| Subnet       | Subnet IP      | Range                        | Subnet Mask     | Broadcast     |
| ------------ | -------------- | ---------------------------- | --------------- | ------------- |
| 0 (DMZ ETH0) | 192.168.0.0/28 | 192.168.0.1 -> 192.168.0.14  | 255.255.255.240 | 192.168.0.15  |
| 1 (RA ETH1)  | 192.168.1.0/26 | 192.168.1.1 -> 192.168.1.62  | 255.255.255.192 | 192.168.1.63  |
| 2 (RD ETH2)  | 192.168.2.0/24 | 192.168.2.1 -> 192.168.2.254 | 255.255.255.0   | 192.168.2.255 |

Gestiamo i dispositivi mobili con il DHCP con un indirizzo di classe B 168.254.0.0.

La connessione WAN-Internet ha 2 indirizzi pubblici, una per ciascuna delle due linee della scuola, chiamate P2PP0 P2PP1 (Point To Point Protocol).

Il Router NAT effettuerà la traslazione degli indirizzi pubblico/privato dei pacchetti in entrata ed in uscita dalla rete.

DMZ, Firewall, NAT e Server di Autenticazione rappresentano la protezione della rete dagli attacchi esterni.

Mentre il server di Backup del DB ed i dati e gli UPS garantiscono la continuità dei servizi.

3) Crittografia Simmetrica e Asimmetrica.

SNMPv3 - serve per far parlare manager e user agent.

