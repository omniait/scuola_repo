# La Guerra Fredda

## Il termine

Il termine "Guerra Fredda" si deve al giornalista americano Lippmann nel 1947.

Winston Churchill invece coniò il termine "Cortina di ferro" per descrivere la situazione post seconda guerra mondiale.

## La Germania

La Germania fu divisa in vari blocchi controllati ed occupati dai vincitori della guerra: francesi, americani, britannici e sovietici; si formarono due repubbliche: la Repubblica Federale Tedesca con capitale a Bonn delle forze liberali e la Repubblica Democratica Tedesca con capitale Berlino "est" delle forze sovietiche. 

Particolarmente la stessa Berlino fu suddivisa in 4 settori.

Nel 1948 i sovietici imposero un blocco merci sbarrando le strade di accesso a Berlino ma i liberali organizzarono un ponte aereo per fornire la città, un anno dopo il blocco fu smantellato.

Nel 13 Agosto 1961 fu eretto il Muro di Berlino.

Gli accordi di Yalta impedirono la nascita di un conflitto diretto in quanto comunque da entrambi i blocchi ci furono tentativi rivoluzionari (Grecia e Ungheria ad esempio) obbligando le due parti politiche a non partecipare in nessun modo a queste rivoluzioni.

##Truman e piano Marshall e COMECON

Nel 1947 il presidente Truman chiese al Congresso di stanziare aiuti militari a favore delle forze anticomuniste greche; questo fondò le basi della politica estera americana. 

Il piano Marshall o European Recovery Program è un piano che prevedeva l'intervento economico a sostegno dei paesi europei devastati dalla guerra per aiutare la ricostruzione, i paesi del blocco sovietico non accettarono.

Analogamente nel 1949 nel blocco sovietico fu messo appunto un Consiglio di mutua assistenza economica chiamato COMECON con caratteristiche simili al piano Marshall.

## Patto Atlantico o NATO e Patto di Varsavia

Nell'Aprile 1949 fu firmato il patto Atlantico che fece nascere la NATO: un'alleanza militare difensiva antisovietica.

Nel 1955 viene invece firmato il Patto di Varsavia analogo e contrapposto alla NATO. Nello stesso anno in Austria, che rimase fuori dalla NATO, l'occupazione sovietica venne ritirata.

## Paesi Comunisti non filosovietici

Seppur la maggior parte dei paesi comunisti fossero alleati e vicini all'Unione Sovietica 2 paesi comunisti si allontanarono da essa: Yugoslavia con Tito e la Cina con Mao Tse Tung.

## Oppressioni di vario genere nei due blocchi

Se da una parte il blocco sovietico mantenne una politica autoritaria e totalitaria sopprimendo le rivolte, come quella ungherese, dall'altro blocco si ha nel 1950 l'istituzione del International Security Act su proposta di McCarthy che prevedeva una commissione con poteri speciali per indagare su attività antiamericane.

Questo avviò una "caccia alle streghe", arrestando, interrogando e addirittura condannando a morte le persone sospettate di attività filo-sovietiche.

Durò 2 anni a causa degli eccessi e lo stesso McCarthy fu costretto a dimettersi e a lasciare la politica.

Negli USA si ha anche la segregazione razziale, famoso è il discorso di Martin Luther King.

