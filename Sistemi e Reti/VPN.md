# VPN

È possibile creare una rete privata reale, con collegamenti fisici, o una rete privata virtuale, tramite VPN al fine di costruire una WAN.

Una rete privata reale è sicura, prestante e garantisce una banda garantita ma costa sia nell'immediato che nel lungo periodo, non è scalabile e manca la ridondanza.

La soluzione migliore è una VPN, cioè una rete privata creata all'interno di una pubblica, spesso Internet.

Le VPN sono:

* Scalabili
* Funzionali
* Configurabili e Riconfigurabili
* Ridondanti

Il fatto che siano presenti su una rete pubblica fa sorgere dei problemi:

* Non garantiscono la banda
* Bisogna controllare gli accessi con l'autorizzazione
* I dati viaggano su una rete pubblica quindi è necessaria una sicurezza (Cifratura e/o tunneling)

Principalmente esistono due tipi di VPN

## Remote-Access VPN

Gli utenti possono accere in totale mobilità, gli utenti possono accedere alle risorse condivise della rete locale come se fossero all'interno.

Questo crea la necessità, a causa dell'insicurezza, di una barriera realizzata con un Firewall.

La R-A VPN è adatta per singoli e pochi dipendenti o per piccole filiali.

Vi sono due componenti fondamentali: Un server di autenticazione e una VPN Client.

### Server Autenticazione

Il NAS verifica le credenziali di accesso per accedere alla VPN, questo processo può essere implementato sullo stesso NAS (Network Access Server) o su un AAA separato (Authentication, Authorization, Accounting), per esempio un RADIUS.

Sia NAS che AAA possono essere dedicati o essere servizi sullo stesso server condiviso.

### VPN Client

Serve un'applicazione VPN Client (es. Cisco VPN Connect), che può essere già integrato nell'OS.

## Site-to-Site VPN

La SS VPN permette di stabilire connessioni sicure tra LAN, creando quindi collegamenti tra LAN e quindi formando una WAN.

Ci sono due tipi:

* Intranet-Based: Si riuniscono sedi remote in una rete privata in una WAN, nell'intranet si usa il protocollo TCP/IP all'interno di una LAN
* Extranet-based: La extranet si interfaccia con l'esterno, quindi la VPN Extranet permette di creare una rete condividendo risorse senza autenticarsi nella Intranet.

## Sicurezza delle VPN

L'integrità e l'autenticità è verificata tramite firma digitale o certificato digitale.

### Authentication

L'autenticazione è il processo con cui ci si assicura dell'identità di un sistema informatico o di un utente che vuole comunicare.

Esiste anche un altro NAS (Network Attached Storage), un dispositivo con OS Linux, un microprocessore, e una memoria di massa spesso configurata in RAID per garantire prestazioni e ridondanza, per accederci bisogna autenticarsi con i soliti metodi.

### Authorization

Una volta che l'utente è autenticato è possibile inserire un altro livello di sicurezza che può prevedere una One Time Password (OTP) o l'impronta digitale tramite smart card.

### Accounting

Al fine di prevenire azioni indesiderate o non autorizzate si prevedono meccanismi di accounting che possono riguardare la durata della sessione di lavoro e/o il quantitativo di dati inviati e ricevuti.

Le VPN possono essere protette tramite Tunneling o Transport.

### Tunneling

Il tunneling funziona con l'incapsulamento di un protocollo di livello inferiore con uno superiore.

Svolgono un ruolo importante Firewall e Router, dove gli apparati sono End-to-End e si occupano loro di trasformare e codificare il traffico evitando quindi la necessità di software.

### Transport

È necessario un software client, l'utente si collega alla sede centrale tramite internet, cifratura e decifratura avviene dal client al server e sulla rete pubblica viagga in chiaro solo l'informazione di routing (Header).

### Protocolli di sicurezza

I protocolli sono: IPsec, SSL/TLS, BGM/MPLS.

#### IPsec

È un'architettura di sicurezza contenente più protocolli, più usata perché consente di usare più tipi di VPN, sia Site to Site sia Remote Access.

I protocolli IPsec sono:

* Authentication Header (AH) - Garantisce autenticazione e integrità ma non la confidenzialità.
* Encapsulating Security (ESP) - Garantisce autenticazione, integrità e confidenzialità (non si può risalire al contenuto originale dei dati).
* Inter Key Exchange (IKE) - Implementa lo scambio di chiavi per realizzare il flusso crittografato (crittografia asimmetrica), la connessione logica al Network Layer è detta Security Association (SA).
  IKE usa UDP ma è affidabile e sicuro perché la SA ha chiavi temporanee.


Le SA sono unidirezionali e di conseguenza due SA sono necessarie per comunicare.

Tutte le SA attive sono memorizzate o su un Host o in un database (SAD).

Le politiche di sicurezza (pacchetti da scartare, IP validi, etc) sono presenti su un SPD.

##### Vantaggi e Svantaggi

Vantaggi:

* È sicuro e affidabile
* Diversi metodi di autenticazione

Svantaggi:

* L'IKE è complesso per l'autenticazione e lo scambio delle chiavi
* C'è forse necessità di modificare l'OS
* IPsec ha un'architettura complessa
* IPsec rende sicuro solo il traffico tra host o tra sottoreti

#### VPN SSL/TLS

La VPN SSL/TLS usa il protocollo TLS, un protocollo client/server, al posto dell'IKE per la fase di autenticazione e negoziazione delle chiavi.

L'autenticazione è basata sui certificati digitali riconosciuti da una Certification Authority(CA).

L'autenticazione funziona così: il Server invia il certificato dalla CA al client che lo verifica con la firma digitale della CA a lui nota. Se questa è valida viene accettato il certificato.

##### Vantaggi e Svantaggi

Vantaggi:

- Offre un canale sicuro tra due applicazioni
- Consente l'autenticazione asimmetrica (autenticazione solo del server)

Svantaggi:

- Consente solo l'uso di certificati digitali per l'autenticazione
- Esclude applicazioni UDP

#### VPN BGP/MPLS (Multi-protocol label Switching)

Le VPN BGP/MPLS sono offerte dal Service Provider come servizio. Usano un modello P2P(peer to peer).

BGP è un protoccolo EGP Path Vector che elenca tutti gli Autonomous System(AS) da attraversare per raggiungere la destinazione.

MPLS usa una Label per muovere i pacchetti all'interno del suo dominio grazie a delle apposite table con il next hop.

Il protocollo MPLS non è concorrente dell'IPsec perché lavorano su due livelli diversi e possono essere complementari.

Le VPN BGP/MPLS necessita però di una rete circoscritta di un Service Provider che ne fornisce l'infrastruttura.

IPsec invece funziona su una normale connessione internet.

## Tipi di VPN

Le reti VPN possono essere di 3 tipi

### Trusted VPN ATM o BGP

Bisogna fidarsi dell'ISP in quanto la riservatezza dei dati è controllata da essa.

Non sono usati i protocolli di cifratura e il tunneling.

L'ISP garantisce che nessun altro abbia accesso al canale usato.

### Secure VPN

Sono reti che usano protocolli IPsec e SSL/TLS. Assicurano la cifratura dei dati ma non assicurano i percorsi (no tunneling).

### Hybrid VPN

Trusted + Secure.

# SSL/TLS

TLS è un protocollo crittografico successore di SSL che oggi è considerato insicuro.

Il TLS permette una comunicazione sicura end-to-end su una rete TCP/IP che fornisce Autenticazione, Integrità e Cifratura sopra al Transport Layer, nel Session Layer.

L'SSL usava chiavi a 40b per consentire la forzatura delle chiavi da parte del governo mentre il TLS usa chiave simmetrica ad almeno 128b.

Il TLS lavora sia unilateralmente, dove il server si autentica, sia bilateralmente, dove sia client che server si autenticano, è quindi necessario che anche il client abbia un certificato digitale.

Le fasi sono 3:

* Negoziazione tra le parti per decidere l'algoritmo
* Scambio delle chiavi in RSA e autenticazione
* Cifratura simmetrica e autenticazione dei messaggi