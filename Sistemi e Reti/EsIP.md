| IP            |                        |
| ------------- | ---------------------- |
| 200.110.12.32 | Indirizzo di Rete      |
| 200.110.12.63 | Indirizzo di Broadcast |

200.110.12.00 10 00 00

200.100.12.00 11 11 11

Subnet IP: 200.110.12. SS SH HH HH

---

Considerare che non sono indirizzi di host.

---

IP: 131.175.21.1 / 22

SM: 255.255.11 11 11 00.0 = 255.255.252.0

Si tratta di un indirizzo di classe B.
$$
NSubnet = 2^6 = 64; \\
NHost = 2^{10}-2 = 1024 - 2 = 1022;
$$
Broadcast = 131.175. SS SS SS 11. 11 11 11 11;

---

```sql
SELECT *
FROM proprietari INNER JOIN giardini
ON proprietari.id_p = giardini.fk_idp
WHERE giardini.dimensione = ( SELECT MAX(dimensione)
 FROM proprietari INNER JOIN giardini
 ON proprietari.id_p = giardini.fk_idp
 WHERE proprietari.nome = "Mario")
```

