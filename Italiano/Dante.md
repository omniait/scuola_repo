# Viaggio della Divina Commedia

Inizia dall'Inferno, sotto Gerusalemme, qui si intravede col racconto d'Odisseo il Purgatorio.

Passa per il Purgatorio e finisce al Paradiso.

La sfera del fuoco separa il Trascendente e l'Intrascendente.

## Politica e Dante

Espresso nel De Monarchia. Opera scritta nel periodo dell'esilio in cui scrive in Latino (Sulla Monarchia) la sua teoria politica.

Anzitutto c'è la teoria dei due Soli, la separazione dei poteri temporali e spirituali.

Papa Teocratico, ovvero seguivano l'idea di Innocenzo III di avere entrambi i poteri Temporale e Spirituale: Bonifacio VIII.

Cosa che va contro l'idea di Dante dove crede che si formi uno squilibrio.

Dante spera nell'intervento di Arrigo VII di Lussemburgo che tenta una spedizione in Italia.

###VI Canto Inferno

Ciacco - Goloso - Profezia dell'esilio da Firenze.

Dante inveisce contro Firenze.

### VI Canto Purgatorio

Sordello da Goito - "Ahi serva Italia...".

Dante inveisce contro l'Italia.

### VI Canto Paradiso

Giustiniano - imperatore Bizantino VI secolo D.C.

Si parla dell'Impero Romano d'Oriente - Impero Bizantino.

Aquila - Simbolo del potere imperiale.

VV 1 - 96

Glorificazione della storia romana, rassegna di com'era nato e formato l'impero Romano.

Primi versi (27) è una sintesi dell'impero sotto Giustiniano e della sua vita, si converte al cristianesimo dopo un'eresia, non credeva nella trinità, porta la capitale a Costantinopoli, stesura del Corpus Iuris Civilis (rimette insieme tutte le leggi dell'Impero Romano).

VV 97 - 117

Invettiva contro Firenze.

## I Canto Paradiso

Dante dichiara di essere stato nel Cielo del Paradiso (l'Empireo) che riceve maggiormente la luce divina che si diffonde nell'Universo: lì ha visto cose difficili da riferire a parole, poiché l'intelletto umano non riesce a ricordare ciò che vede quando penetra in Dio. 

Il poeta tenterà di descrivere il regno santo nella III Cantica e per questo invoca l'assistenza di Apollo, in quanto l'aiuto delle Muse non gli è più sufficiente. Il dio pagano dovrà ispirarlo col suo canto, come fece quando vinse il satiro Marsia, tanto da permettergli di affrontare l'alta materia del Paradiso e meritare così l'alloro poetico. 

Apollo dovrebbe essere lieto che qualcuno desideri esserne incoronato, poiché ciò accade raramente nei tempi moderni.

Dante distoglie lo sguardo dal sole e osserva Beatrice, che a sua volta fissa il Cielo. Il poeta si perde a tal punto nel suo aspetto che subisce una trasformazione simile a quella di Glauco quando divenne una creatura marina: è impossibile descrivere a parole l'andare oltre alla natura umana (trasumanare). 

Dante non sa dire se, in questo momento, sia ancora in possesso del suo corpo mortale o sia soltanto anima, ma di certo fissa il suo sguardo nei Cieli che ruotano con una melodia armoniosa e gli sembra che la luce del sole abbia acceso in modo straordinario tutto lo spazio circostante. 

## III Canto Paradiso Primo cielo (Luna)

Beatrice ha svelato a Dante col suo ragionamento logico la verità circa l'origine delle macchie lunari, quindi il poeta leva il capo per rivolgersi alla donna, ma un'improvvisa visione attira il suo sguardo e lo distoglie dal suo proposito. Dante vede le figure di spiriti pronti a parlare, talmente evanescenti da sembrargli il riflesso di un'immagine sul pelo dell'acqua, così il poeta cade nell'errore opposto a quello che indusse Narciso a innamorarsi della propria immagine riflessa. Infatti Dante si volta per vedere le figure reali che pensa siano dietro di lui, senza però vedere nulla. La donna spiega che le figure che vede sono creature reali, relegate in questo Cielo per non aver rispettato il voto. Beatrice lo invita a parlare liberamente con loro, in quanto la luce di Dio che li illumina non gli consente di allontanarsi dalla verità.

### Piccarda Donati 

Rivela di essere posta lì con gli altri spiriti difettivi e di essere relegata nel Cielo più basso, quello della Luna, benché lei e gli altri gioiscano di partecipare all'ordine voluto da Dio. 

Essi hanno il grado più basso di beatitudine perché i loro voti furono non adempiuti o trascurati in parte.

La beata spiega che un Cielo più alto ospita santa Chiara d'Assisi, fondatrice dell'Ordine delle Clarisse alla cui regola molte donne si votano e prendono il velo. 

Piccarda indossò quell'abito e pronunciò i voti monastici, ma degli uomini più avvezzi al male che al bene la rapirono dal convento e la obbligarono a una vita diversa. 

Piccarda indica poi un'anima splendente alla sua destra, che ha vissuto la stessa esperienza poiché fu suora e le fu tolto forzatamente il velo, anche se in seguito rimase in cuore fedele alla regola monastica: è l'imperatrice **Costanza d'Altavilla**, che con Enrico VI generò Federico II di Svevia.

## VI Canto Paradiso Secondo cielo (Mercurio)

Giustiniano risponde alla prima domanda di Dante, spiegando che dopo che Costantino aveva portato l'aquila imperiale (la capitale dell'Impero) a Costantinopoli erano passati più di duecento anni, durante i quali l'uccello sacro era passato di mano in mano giungendo infine nelle sue. 

Egli si presenta dunque come imperatore romano e dice di chiamarsi Giustiniano, colui che su ispirazione dello Spirito Santo riformò la legislazione romana. 

Prima di dedicarsi a tale opera egli aveva aderito all'eresia monofisita, credendo che in Cristo vi fosse solo la natura divina, ma poi papa Agapito lo aveva ricondotto alla vera fede e a quella verità che, adesso, egli legge nella mente di Dio. 

Non appena l'imperatore fu tornato in seno alla Chiesa, Dio gli ispirò l'alta opera legislativa e si dedicò tutto ad essa, affidando le spedizioni militari al generale Belisario che ebbe il favore del Cielo.

vv 97

Terminata la sua digressione, Giustiniano invita Dante a giudicare l'operato di Guelfi e Ghibellini che è causa dei mali del mondo: i primi si oppongono al simbolo imperiale dell'aquila appoggiandosi ai gigli d'oro della casa di Francia, i secondi se ne appropriano per i loro fini politici, per cui è arduo stabilire chi dei due sbagli di più. I Ghibellini dovrebbero fare i loro maneggi sotto un altro simbolo, poiché essi lo separano dalla giustizia; Carlo II d'Angiò, d'altronde, non creda di poterlo abbattere coi suoi Guelfi, dal momento che l'aquila coi suoi artigli ha scuoiato leoni più feroci di lui. I figli spesso pagano le colpe dei padri e Dio non cambierà certo il simbolo dell'aquila con quello dei gigli della monarchia francese.

Giustiniano indica a Dante l'anima di Romeo di Villanova, che splende in questo stesso Cielo e la cui grande opera fu sgradita ai Provenzali, che tuttavia hanno pagato cara la loro ingratitudine nei suoi confronti. Raimondo Berengario IV, conte di Provenza, ebbe quattro figlie e grazie all'opera dell'umile Romeo tutte furono regine; poi le parole invidiose degli altri cortigiani lo indussero a chiedere conto del suo operato a Romeo, che aveva accresciuto le rendite statali. Egli se n'era andato via, vecchio e povero, e se il mondo sapesse con quanta dignità si ridusse a chieder l'elemosina, lo loderebbe assai più di quanto già non faccia.

## Profezie Dell'esilio

* Inferno VI - Ciacco
* Inferno X - Farinata degli Uberti
* Inferno XV - Brunetto Latini
* Inferno XXIV - Vanni Fucci
* Purgatorio XI - Odenisi da Gubbio


## XV - XVII Canto Paradiso

Cacciaguida - Trisavoro di Dante, vissuto durante il tempo delle crociate (1090).

Cacciaguida dà l'ultima e definitiva profezia dell'esilio (Finale, ce ne sono state molte altre).

Nel tempo delle crociate la morale è correlata all'ideale cavalleresco, valori morale e civile saldi.

Mentre la gente al tempo di Dante è avida, guidata dagli ideali borghesi (seize the means of production).

Si trova nel quinto cielo di Marte, dove vi sono le anime combattenti per la fede (Cacciaguida è stato un crociato).

Nel paradiso la sede dei beati è la Rosa dei beati ma in realtà essi si muovono tra i cieli.

Si instaura un colloquio intessuto di rimandi affettivi che prendono l'apparenza di un amore Padre-Figlio.

### Analisi Versi

Cacciaguida spiega come i beati riescano a vedere il futuro e il presente.

> Ti sarà inevitabile lasciare Firenze, come Ippolito lasciò Atene a causa della spietata e perfida matrigna Fedra. 
>
> Si vuole questo e si cerca di attuarlo, e verrà **presto compiuto**, da chi (Bonifacio VIII) pensa a ciò là (nella Curia papale) dove si **mercifica Cristo** (le cose sacre) ogni giorno.

Nel 1300 viveva ancora il classicismo e si andava a riusare e apprezzare anche i miti classici, notabile anche in Dante in questi versi.

Nei versi 51 - 99 c'è praticamente tutta la storia dell'esilio, la compagine dei guelfi bianchi viene descritta con aggettivi negativi, viene detto che Dante dovrà andare di Corte in Corte per cercare vitto e alloggio. Si fa riferimento a Cangrande e Bartolomeo. 

Dante si trova un dilemma davanti, pensando al futuro se deve star muto o riportare tutto facendosi molti nemici.

> Coscienza fusca 
> o de la propria o de l’altrui vergogna 
> pur sentirà la tua parola brusca.                                   
>
> Ma nondimen, rimossa ogne menzogna, 
> tutta tua vision fa manifesta; 
> e lascia pur grattar dov’è la rogna.

Le anime sono state mostrate per avere degli esempi forti dei vizi o delle virtù degli uomini, noti al loro tempo.

## XXXIII Canto Paradiso

Ineffabilità (indescribilità) e indicibilità della visione di Dio.

Due mezzi importanti per tentare di descrivere: Neologismo (inventare nuove parole), Metafora della Luce.

È presente la preghiera alla Vergine, che farà da tramite.

Ricorre quindi il tema della preghiera, che già il Purgatorio è pieno.

L'ultima visione è la trinità, colta e descritta in 3 cerchi sovrapposti ma di colori diversi.

