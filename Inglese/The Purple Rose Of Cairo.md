# The Purple Rose Of Cairo

## Un eterno altalenarsi tra due realtà

### Perché questo film?

Si tratta del film che mi ha dato più interesse rispetto agli altri, pur non coinvongendomi completamente nel film questo mi ha lasciato una sensazione, che ancora mi logora.

### Finzione nella realtà fittizzia

Il film è in qualche modo strettamente collegato a Pirandello, al "teatro nel teatro", qui si parla di cinema nel cinema, senza andare troppo nei dettagli, i personaggi del film riusciranno a interagire con i personaggi del film che i primi stavano guardando da normali spettatori, rompendo di fatto la "Fourth Wall".

Contemporaneamente inizio e fine sono circolari, cosa che ricorda le poesie di Pascoli.

La stessa realtà che vivono i protagonisti del film però è fittizzia, questo è uno dei motivi che mi lascia insoddisfatto riguardo al film, ma non insoddisfatto nel senso di un film carente, un altro tipo di insoddisfazione forse come quella di non completare la cena.

### There is no "Last Course"

Durante il film lo spettatore "vero" che guarda il film di Allen capisce di star guardando un film, non tanto dalla realtà descritta dal film, dato che è realistica e anche storicamente accurata, ma da quello che succede nel film.

Il fatto che si rompa il quarto muro fa capire allo spettatore di essere nella finzione, sopratutto quello che mi ha impressionato è il modo in cui i personaggi (di Allen) rimangano non colpiti, non impressionati, dalla cosa successa e inspiegabile, anzi il modo in cui agiscono è razionale solo se questi siano coscienti d'esser in un film, forse è così? Sono coscienti? Beh in ogni caso noi lo siamo.

Ed ecco, ecco che manca il "Last Course", lo spettatore capisce di star guardando un film e si attende cose da film, ma questo "dulcis in fundo", l'ultimo piatto, non arriva mai lasciandoci "a bocca asciutta".

Deliberatamente il finale rimane aperto, senza sequel, e con una profonda delusione perché quell'ambiente da film viene frantumato negli ultimi minuti da un colpo di scena nella trama.

### Allora mancano certezze

Proprio così, mancano certezze, non siamo sicuri di nulla, un po' come nella nostra realtà quotidiana.

Ma noi guardiamo film, leggiamo libri e fantastichiamo proprio per evadere dalla realtà quotidiana eppure questo film ci tira un pugno allo stomaco finale, catapultandoci di nuovo nel quotidiano, fermando i sogni e i pensieri che avremmo potuto costruire sul film.

Allo stesso tempo però, il fatto che mi ha lasciato così impressionato e comunque carico di un'emozione particolare mi obbliga a dire che sia di fatto un ottimo film, cosa che fa arrabbiare proprio per il motivo sopracitato!

*Simone Romano*

