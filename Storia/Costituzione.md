# Costituzione

Concetto di costituzione - specifica i poteri del re, se c'è, al parlamento, se c'è, al governo, se c'è, alla magistratura, se c'è.

Questi vengono inseriti nella costituzione.

Di conseguenza la costituzione diventa un testo che limita i poteri a determinati organi. Le prime costituzioni esplicitano il potere legislativo (oggi gli organismi con potere legislativo sono organi eletti dal popolo).

Le Costituzioni scritte prime sono state: La costituzione degli USA e la costituzione della Francia post-rivoluzione.

Queste sono i primi esempi di atto normativo che limita i poteri.

* Repubblica - Il capo dello stato è eletto e non per diritto divino o di nascita (dal popolo, dal parlamento) ed ha una carica temporanea.
* Monarchia - Il capo dello stato è tale per diritto divino o di nascita ed ha una carica infinita.

In Italia la prima costituzione è lo statuto albertino, concesso nel 1848 e quella della Repubblica del 1948.

## Lo statuto albertino

Lo statuto albertino è concessa ed ha una premessa: Il re concede una limitazione del proprio potere. È inoltre una costituzione flessibile poiché il parlamento con la maggioranza può cambiare direttamente un articolo della costituzione.

Questo rese possibile la creazione di una dittatura in modo progressivo e non esagerato.

## Costituzione della Repubblica

Nasce come atto di compromesso con tre matrici ideologiche importanti del 900 e 800 che sono: Socialista, Cattolica e Liberista. Nel 900 si ha l'affermazione delle grandi masse nella Res Publica. 

Invece ora ci vuole la maggioranza di 2/3 che se manca serve il referendum, allo stesso tempo va votato il cambiamento due volte da entrambe le camere senza quindi modifiche. È pertanto una costituzione Lunga e Rigida.

Dal 02/06/1946 al 01/01/1948.

Dichiarazione Universale dei Diritti Umani - 10/12/1948.



## Articoli

Articoli 1 - 12 Principi Fondamentali

Articoli 13 - 54 Diritti e Doveri dei cittadini, Rapporti Civili

54 - 139 Ordinamento della Repubblica

Laicità dello stato - Liberali (Libera chiesta in Libero stato). Art 7-8