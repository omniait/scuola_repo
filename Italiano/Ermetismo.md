# Giuseppe Ungaretti

È un poeta che tiene conto della *lezione delle avanguardie*; in Italia il Futurismo.

In letteratura si hanno come conseguenze:

* Sintassi che svanisce, così come la punteggiatura e il metro

Grazie al manifesto del futurismo letterario.

## Biografia

Nasce ad Alessandria d'Egitto nel 8 Febbraio 1888, studia a Parigi, centro culturale del '900, fondamentale perché incontra dei poeti francesi e va in contatto con la cultura del tempo.

La prima raccolta si chiama "Il porto sepolto" nel 1916, già influenzata dall'esperienza della Grande Guerra, dato che egli fu volontario e combatté al Carso.

Il porto sepolto rappresenta come nome ciò che è nascosto dalla poesia.

Le Poesie de "Il porto sepolto" seguono una poetica "Della Parola" (è stato considerato comunque un poeta ermetico) altre raccolte in "allegria dei naufragi".

Allegria dei Naufragi del 1919 è un ossimoro come il "Tacito Tumulto" di Pascoli in "Il Lampo".

I termini (Allegria e Naufragi) sono spiegati dal poeta, l'allegria è l'esultanza di un attimo, mentre il naufragio è la morte, il suo effetto distruttivo.

Nelle edizioni successive continua a elaborare le poesie scritte nelle raccolte precedenti.

La raccolta poetica finale "Allegria" è del 1931, il titolo sottilinea più l'aspetto positivo rispetto all'edizione precedente.

L'altra opera importante di Ungaretti è "Sentimento del Tempo" 1933, dove nella forma recupera una struttura sintattica e il metro tradizionale (Petrarca e Leopardi come modelli). 

Il tempo distrugge e ricostruisce.

I racconti successivi al 1933 sono raccolti in "Il Dolore", è morto il figlio ed il fratello e un senso di angoscia data dall'instaurarsi dei totalitarismi.

Infine c'è stata una raccolta totale chiamata "Vita di un Uomo".

Ungaretti muore nel 1970.

### Poetica della parola

La Parola è scelta, concentrata e carica di significati; non si può togliere neanche un termine senza perdere significato.

Lo stesso titolo è fondamentale, utile e necessario per comprendere l'intero testo.

È una poetica Chiara ed Esplicita.

#### Retorica

```mermaid
graph TB;
    A[Analogia]-->B[Similitudine];
    A-->C[Metafora];
```

## San Martino del Carso

*Di* queste case
**Non è rimasto**
*Che* qualche
**Brandello** di muro
*Di* tanti
*Che* mi corrispondevano
**Non è rimasto**
Neppure tanto
Ma nel **cuore**
Nessuna croce manca
È il mio **cuore**
Il paese più straziato

Forte analogia tra il cuore e il paese.

## I Fiumi

Parla anche della vita del poeta, parla dei "fiumi della sua vita".

Egli si ritrova a contare i suoi fiumi sull'Isonzo, dove ha combattuto.

Il poeta è diventato un sasso mentre il fiume ha delle mani nascoste che accarezzano e levigano il poeta regalandogli una "rara felicità".

## Veglia

Un’intera nottata
buttato vicino
a un compagno
massacrato 
con la sua bocca
digrignata 
volta al plenilunio
con la congestione 
delle sue mani
penetrata
nel mio silenzio
ho scritto
lettere piene d’amore.
Non sono mai stato
tanto
attaccato alla vita



Buttato - Come una cosa.

Per la poesia Veglia si parla di Espressionismo, grazie agli aggettivi molto chiari, mani congestionatte, la luna piena.

## Soldati

Si sta come
d'autunno
sugli alberi
le foglie

Fragilità - Caducità

```mermaid
graph LR
    A[Fragilità] --> C[Condizione del Soldato]
    B[Caducità] --> C
```

## In Memoria

## Mattino

# Ermetismo

Ermetismo - Tendenza degli anni '30, che richiama all'indecifrabilità, il termine deriva da "Ermete", un compositore esoterico.

## Salvatore Quasimodo

### Alle fronde dei salici

Che cosa vuol dire "alle fronde dei salici"? Cosa significa il titolo della poesia.

> "E come potevano noi cantare  
> Con il piede straniero sopra il cuore,  
> fra i morti abbandonati nelle piazze  
> sull'erba dura di ghiaccio, al lamento  
> d'agnello dei fanciulli, all'urlo nero  
> della madre che andava incontro al figlio  
> crocifisso sul palo del telegrafo?  
> Alle fronde dei salici, per voto,  
> anche le nostre cetre erano appese,  
> oscillavano lievi al triste vento."

Alle fronde dei salici indica l'atto compiuto nella Bibbia dagli ebrei schiavizzati che appesero le cetre al salice rifiutandosi di "cantare" per gli oppressori e invasori.

Lo stesso accade in Italia in quanto siamo nel periodo del nazifascismo e quindi lui e gli altri poeti italiani, invasi dai nazisti, appendono simbolicamente le lire come gli ebrei oppressi, impossibilitati quindi a cantare.

## Eugenio Montale

### Biografia

Eugenio Montale (Genova, 12 ottobre 1896 – Milano, 12 settembre 1981), premio Nobel per la letteratura nel 1975 (altri letterati italiani che hanno ricevuto il premio nobel sono: Pirandello, Carducci e Quasimodo). 

Dà importanza a Italo Svevo grazie alla sua passione per la letteratura contemporanea.

Montale è un intellettuale impegnato politicamente e si dichiara apertamente anti-fascista con ricadute sulla propria vita lavorativa, scrive Ossi di Seppia nel 1925, anni delle leggi fascistissimee anno in cui firma il Manifesto Croce/Manifesto degli intellettuali antifascisti (1925-1926).

Nel 1927 dirige il Gabinetto letterario Vieusseux, carica che gli verrà sottratta nel 1938 perché non si iscrisse al partito fascista.

### Poetica dell'oggetto

La poetica di Montale è illustrata ne "I Limoni" e ne "Non chiederci la parola", vuole scegliere piante non rare, classiche.

Vuole parlare di una realtà quotidiana, semplice e normale rappresentata da immagini quotidiane.

Montale vuole esser innovativo più nei contenuti che nel metro e nella sintassi che rimangono quelle tradizionali.

### Ossi di Seppia

Il titolo di questa raccolta, che rappresenta la prima fase, gli ossi di seppia sono residui animali lasciati dal mare, alludendo ad una vitalità minima e ridotta.

| Ungaretti                                | Montale                                                      |
| ---------------------------------------- | ------------------------------------------------------------ |
| Porto Sepolto (1916) e L'Allegria (1931) | Ossi di Seppia (1925)                                        |
| Abbandono della metrica tradizionale     | Riprende la metrica tradizionale                             |
| Mancanza di punteggiatura                | Sintassi regolare                                            |
| Analogia, <br />importanza della Poetica | Abbandono dell'analogia metafora, <br />non importanza della Poetica |

#### Non chiederci la parola

> Non chiederci la parola che squadri da ogni lato  
> l'animo nostro informe, e a lettere di fuoco  
> lo dichiari e risplenda come un croco  
> perduto in mezzo a un polveroso prato.     
>
> Ah l'uomo che se ne va sicuro,  
> agli altri ed a se stesso amico,  
> e l'ombra sua non cura che la canicola 
> stampa sopra uno scalcinato muro!    

Non domandarci la formula che mondi possa aprirti,  
sì qualche storta sillaba e secca come un ramo.  
Codesto solo oggi possiamo dirti,  
ciò che non siamo, ciò che non vogliamo."

Non chiederci - A noi poeti - una parola che esprima la nostra anima informe con lettere di fuoco, chiare.

Si parla di **poetica in negativo**, perché la poetica non è più capace di illustrare delle realtà ben definite, non è più una "formula magica", non può dare una rivelazione e il poeta quindi non può descrivere la realtà.

Parla proprio del ruolo dei poeti, questi non sono più detentori della realtà, togliendo valore al valore del poeta.

#### I Limoni

Non sceglie "Le piante dai nomi poco usati", in riferimento diretto a "La pioggia nel pineto" di D'Annunzio.

Sceglie una pianta comune della sua terra, la Liguria, le piante dei limoni.

La poesia è una dichiarazione di ciò di cui il poeta vuole parlare.

####  Forse un mattino andando in un'aria di  vetro         

> Forse un mattino andando in un'aria di vetro, 
> arida, rivolgendomi, vedrò compirsi il miracolo: 
> il nulla alle mie spalle, il vuoto dietro 
> di me, con un terrore di ubriaco. 
> Poi come s'uno schermo, s'accamperanno di gitto 
> alberi case colli per l'inganno consueto. 
> Ma sarà troppo tardi; ed io me n'andrò zitto 
> tra gli uomini che non si voltano, col mio segreto.

https://avolodigabbiano.wordpress.com/2014/05/14/commento-a-forse-un-mattino-andando-in-unaria-di-vetro-di-e-montale

#### Spesso il male di vivere ho incontrato

> Spesso il male di vivere ho incontrato:
> era il rivo strozzato che gorgoglia,
> era l’incartocciarsi della foglia
> riarsa, era il cavallo stramazzato.
>
> Bene non seppi, fuori del prodigio
> che schiude la divina Indifferenza:
> era la statua nella sonnolenza
> del meriggio, e la nuvola, e il falco alto levato.

La divina indifferenza è l'unica alternativa al male di vivere, concetto centrale, cita probabilmente la teoria di Epicuro, quindi indifferenza che è solo propria degli dei.

##### Correlativi oggettivi

È il rapporto costruito tra la parola, un concetto astratto, e l'oggetto concreto, sono, riguardo al male di vivere:

1. Rivo strozzato
2. Foglia accartocciata
3. Cavallo stramazzato

Invece rispetto la divina indifferenza, unico bene possibile da contrapporre al male di vivere:

1. Statua nella sonnolenza del mezzogiorno
2. La nuvola
3. Falco alto levato