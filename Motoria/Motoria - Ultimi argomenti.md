# Motoria - Ultimi argomenti

* Ormone - Sostanza chimica secreta da una giandola endocrina
* Ghiandola Endocrina - È una ghiandola che secerne sostanze direttamente nel sangue
* Ghiandola Esocrina - Secerne sostanze all'esterno del corpo
* Anabolismo - Atto di costruire nuove proteine, si oppone al Catabolismo
* Catabolismo - Demolisce le proteine già esistenti formando strutture più semplici
* Forza - Capacità dell'apparato neuromuscolare di vincere o contrapporsi ad un carico esterno con un impegno muscolare
* Forza Massima - La capacità del muscolo di esprimere la massima tensione possibile con una contrazione volontaria
* Forza Veloce - La capacità di esprimere elevate tensioni muscolari nel minor tempo possibile
* Forza Resistente - La capacità di esprimere elevate tensioni per un tempo relativamente lungo
* Allenamento - Metodi
* Metodo degli sforzi massimali - Lavoro che impiega il 90/100% della forza massima, 1 / 3 sollevamenti
* Metodo degli sforzi ripetuti - Lavoro che impiega circa il 70% del massimale, si usano molte ripetizioni
* Velocità - Compiere azioni motorie, in particolare lo spostamento dell'intero corpo, nel minor tempo
* Rapidità - Compiere azioni motorie nel minor tempo possibile
* Resistenza - Capacità di protrarre un'attività fisica nel tempo senza diminuire l'intensità del lavoro
* Fattori fisiologici - Si suddividono in componenti centrali, ovvero la capacità di trasportare ossigeno dai polmoni ai muscoli, ed in componenti periferiche, ovvero la capacità di distribuire il sangue ai muscoli
* Linfa - Di colore trasparente, giallo paglierino o lattescente a seconda dei casi, la linfa contiene zuccheri, proteine, sali, lipidi, amminoacidi, ormoni, vitamine, globuli bianchi ecc. Rispetto al sangue, la linfa è particolarmente ricca di lipidi
* Stretching - (significa allungamento, stiramento) usato nella pratica sportiva per indicare un insieme di esercizi finalizzati al miglioramento muscolare e della flessibilità. 