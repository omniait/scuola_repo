# La regola di Simpson

Sappiamo che con 3 punti non lineari (ovvero che non sono tutti sulla stessa retta) si identifica univocamente un cerchio, ma lo stesso vale per la parabola. 

Questo concetto geometrico è la base teorica della regola di Simpson che usa le parabole al fine di approssimare l'area o la lunghezza di una curva o di una funzione la cui primitiva non è elementare, altrimenti avrebbe poco senso usare uno qualunque dei 3 metodi di approssimazione.

Consideriamo un intervallo solo:

![mg](img1.svg)

Una parabola quadratica è definita da:
$$
P(x) = Ax^2+Bx+C
$$

Troviamo l'area della parabola quadratica nel nostro intervallo:
$$
\int_{-h}^h P(x)dx = 
\int_{-h}^h Ax^2+Bx+C \ dx =
\frac{A}{3}x^3 + \frac{B}{2}x^2 + Cx |_{-h}^h =  \\
\frac{A}{3}x^3 + \frac{B}{2}x^2 + Cx |_0^h \  nota^1 (!)\\
nota^1: Ax^2 \and \ C \ pari \rightarrow posso \ considerare \ S = 2f \\
Bx \ è \ una \ retta \ passante \ per (0;0) \rightarrow  S(Bx) = 0 \\
\frac{2Ah^3}{3}+2Ch = 
\frac{h}{3} \left(2Ah^2 + 6C \right)
$$

Ora, tornando alla formula quadratica, posso trovare C, che corrisponde semplicemente a y1.

Per trovare A mi affido al seguente sistema:
$$
C = y_1 \\
P(h) = Ah^2 + Bh + C = y_2 \\
P(-h) = Ah^2 - Bh + C = y_0 \\
-------------- \\
P(h) + P(-h) \\ 
2Ah^2 + 2C = y_0 + y_2 \\
-------------- \\
2Ah^2 = y_0 + y_2 - 2 y_1
$$

$$
\frac{h}{3} \left(y_0 + y_2 - 2y_1 + 6y_1 \right) = 
\frac{h}{3} \left(y_0 + 4y_1 +  y_2 \right) \\
Nota : h= \Delta x
$$

Ne consegue che, prendendo un certo numero di intervalli, la formula è la seguente:
$$
\int_{x_0}^{x_n}f(x)dx = 
\frac{\Delta x}{3}  \{[ f(x_0)+4f(x_1)+f(x_2)] + [f(x_2) + 4f(x_3) + f(x_4)]... +2f(x_{n-2})+4f(x_{n-1})+f(x_n)\} = \\
= \frac{\Delta x}{3}  [f(x_0)+4f(x_1) + 2 f(x_2) + 4f(x_3) + ... +2f(x_{n-2})+4f(x_{n-1})+f(x_n)]
$$


Esempio:

Approssima l'area della funzione data in 0; pi greco usando 4 intervalli.![mg](img2.svg)
$$
f(x)=sin(x) \\
S[0;\pi]=? \\
$$
Intanto la calcolo normalmente:
$$
\int f(x) = -cos(x) + c \\
\int_0^\pi f(x)= -cos(\pi) - (-cos(\pi))  = 
 2
$$
Costruisco una tabella con i valori che mi servono:

![ab](tab1.png)
$$
\int_0^\pi f(x) \approx 
\frac{\Delta x}{3} (y_0 + 4y_1 + 2y_2 + 4y_3 + y_4) = \\
\frac{\pi}{12}(0+2\sqrt2+2+2\sqrt2+0) =
\frac{\pi}{12}(4\sqrt2+2) =
\frac{\pi}{6}(1+2\sqrt2)
$$
Comparo anche col risultato usando la regola del trapezoide:
$$
TR = \frac{1}{4}\pi(1+\sqrt2) \\
SR = \frac{\pi}{6}(1+2\sqrt2) \\
R = 2 \\
--------------- \\
errori: \\
TR \approx 1.8961188979370398 \\
SR \approx 2.0045597549844207 \\
eSR = -0.1038811020629602 \\
eTR = 0.0045597549844207386 
$$
