# Sicurezza e Crittografia

## Il Problema Sicurezza

Vi sono 3 aspetti da considerare riguardo la sicurezza:

* Segretezza - Le informazioni devono essere leggibili e comprensibili solo agli autorizzati.
* Autenticazione - Per accedere alle informazioni (lettura o modifica) bisogna assicurarsi dell'identità.
* Affidabilità - Il documento dev'essere quello corretto e non un falso (questo si ottiene con la firma digitale).

Autenticazione e affidabilità sono gestite dal Application Layer e la segretezza si può implementare nel Data Link o nel Physical Layer.

## Crittografia Classica e Crittanalisi

La crittografia ha una lunga storia, ad esempio Ebrei e Spartani.

Uno dei Cifrari più classici è quello di Giulio Cesare, un cifrario a scorrimento/sostituzione dove le parole vengono traslate di un valore prestabilito che diventa la "chiave" per decifrare il messaggio.

La Cifratura è il processo attraverso il quale si ottiene il messaggio crittato, le regole applicate devono essere conosciute sia al mittente sia al destinatario in modo da cifrare e decifrare correttamente.

Una cifratura è generalmente composta da due elementi:

* L'algoritmo.
* Almeno un parametro chiamato "Chiave".

La Crittanalisi è lo studio dei metodi per ottenere il messaggio cifrato senza conoscere l'informazione segreta (Password o Chiave).

## Crittografia Simmetrica

Quanto la chiave di Cifratura e Decifratura è la stessa si parla di Crittografia Simmetrica, il problema fondamentale è quindi il metodo con cui si viene a conoscenza della chiave.

Un'importante caratteristica degli algoritmi di crittografia è l'Effetto Valanga, ovvero il cambiamento di pochi bit deve corrispondere a quanti più possibili bit nel messaggio.

### DES

Il DES è un algoritmo simmetrico a chiave di 64bit, 8 però sono di controllo e quindi sono 56 quelli che compongono la chiave.

Le 16 trasformazioni sono sia di Trasposizione sia di Sostituzione e quindi il DES è ritenuto un cifrario misto.

La decifratura avviene usando gli stessi passaggi all'inverso.

Il DES è stato considerato sicuro fino al '98 quando fu sconfitto nell'arco di 60 ore. 

###Triple - DES

Il funzionamento è lo stesso del DES solo che viene applicato 3 volte con una chiave lunga 168 bit (56x3), in particolare nella cifratura si Cifra, Decifra e Cifra di nuovo, decifrare è il percorso inverso.

### IDEA

IDEA è un cifrario molto usato nei software commerciali di crittografia grazie alla sua velocità di codifica e decodifica e alla sua elevata sicurezza, è immune agli Brute-Force Attack e la chiave è di 128b.

È un cifrario a blocchi di 64b dove questi sono cifrati in 8 iterazioni usando operazioni di XOR, somma, moltiplicazioni e modulo 216.

### AES

L'AES è un altro algoritmo veloce e robusto.

Ha tre caratteristiche fondamentali:

* Resistenza a tutti gli attacchi
* Velocità e Compattezza del codice
* Semplicità Progettuale

È un cifrario a blocchi da 128b, le chiavi possono essere di 128, 192, 256 bit ed effettua una combinazione di permutazioni e sostituzioni.

È l'attuale standard, approvato anche dalla NSA.

## Crittografia Asimmetrica

Si usano due chiavi diverse, una pubblica (conosciuta) e una privata (solo del proprietario) per cifrare e decifrare.

Ha 4 caratteristiche fondamentali:

* Si possono usare le due chiavi indifferentemente per cifrare o decifrare
* La chiave utilizzata per crittare non può essere usata per decrittare
* La complessità computazionale per la chiave pubblica è polinomiale
* La complessità computazionale per la chiave privata è esponenziale

È svantaggiosa rispetto la crittografia simmetrica in quanto richiede molta potenza computazionale, in particolare all'aumentare della lunghezza della chiave.

Al fine di accorciare il messaggio si usano le funzioni di hash il quale digest corrisponde ad una sequenza di numeri più corta della lunghezza del messaggio.

Ci sono 3 Metodi:

* Autenticazione del destinatario e riservatezza del messaggio: Il messaggio viene crittato con la chiave pubblica del destinatario il quale decritta con la propria chiave privata.
* Autenticazione della Sorgente: Dato che nel primo metodo chiunque può spedire il messaggio non si sa se il mittente è affidabile e autenticato con quello che ci si aspetta. Il mittente quindi cifra il messaggio con la propria chiave privata, il destinatario quindi dovrà decifrare con la chiave pubblica del mittente.
* Si usano entrambi i metodi, in particolare: il mittente usa la chiave pubblica del destinatario per cifrare, il mittente usa la propria chiave privata per cifrare una seconda volta. Il destinatario usa la chiave pubblica del mittente per decifrare e infine usa la propria chiave privata per decifrare e ottenere il messaggio originale.

### RSA

Le chiavi binarie devono essere di almeno 2'048b.

Le chiavi sono dipendenti tra loro ma non dev'essere possibile risalire all'altra da una.

L'RSA prende due numeri primi grandi e come chiave usa il loro prodotto.

Per decifrare il messaggio è necessario quindi scomporre il prodotto in fattori, cosa che è quasi impossibile da fare in tempi accettabili.

1. Il destinatario sceglie i numeri primi e genera la chiave col prodotto tra loro
2. Il destinatario invia il prodotto al mittente
3. Il mittente usa il prodotto per cifrare e lo spedisce
4. Il destinatario usa i due fattori per decifrare

## Firma e Certificato Digitale

Cifrare un messaggio è un processo spesso lento e pesante e spesso non serve la segretezza ma basta l'autenticazione e la certezza che il messaggio non venga modificato, per esempio le pagine web.

La firma digitale si basa sulla crittografia asimmetrica e consente:

* La sottoscrizione di un documento
* La verifica, da parte del destinatario, dell'identità del mittente
* La certezza che l'informazione del documento non sia stata manomessa

La firma digitale si basa su un dispositivo rilasciato da enti certificatori che accertano l'identità del richiedente.

L'utente viene dotato anche di un PIN da usare.

Il risultato di un documento firmato è un file in formato p7m che consente di firmare qualunque documento informatico.

La Smart Card è il supporto più diffuso.

È fondamentale calcolare il Fingerprint con una funzione Hash, le più comuni sono l'MD5 e SHA.

Assieme al documento viene infatti spedito il fingerprint (cifrato) a fine messaggio col certificato del firmatario.

Il destinatario dovrà decifrare con la chiave pubblica di A e l'integrità si verifica tramite il Fingerprint.

### Approfondimento TLS

Il TLS è l'evoluzione del SSL che è diventato illegale, è un protocollo crittografico che assicura la sicurezza della comunicazione in una rete.

Molti servizi web, come email, chat, voip e i siti normali, sfruttano questo protocollo che punta a garantire privacy e l'integrità dei dati.

* La connessione è privata perché i dati sono crittati in modo simmetrico, ad ogni nuova sessione sono generate nuove chiavi basate su un segreto comune negoziato all'inizio della sessione (questo processo si chiama TLS Handshake); è sicuro in quanto il segreto non può essere ottenuto da terzi e non si può modificare la comunicazione durante l'Handshake.
* L'identità di destinatario e mittente è verificata tramite la crittografia a chiave pubblica, è necessaria e obbligatoria per il server, il quale deve ottenere prima un certificato digitale da terzi (il più comune è let's encrypt della Linux Foundation), e facoltativa per i client.
* La connessione assicura l'integrità in quanto ogni messaggio contiene un codice di controllo.

Il TLS assicura opzionalmente anche che se venisse scoperta una chiave di crittazione questa non può essere usata comunque per decrittare messaggi del passato (forward secrecy).

Anzitutto bisogna capire se è necessaria o meno una connessione sicura, quindi o il client fa la richiesta usando la porta 443 HTTPS o il server rimanda tutto il traffico alla 443.

* L'handshake inizia col client che richiede una connessione sicura al server presentando una lista di cifrari e funzioni hash che supporta (Cipher Suite), che dipende dallo standard TLS del server e dal browser.
* Dalla Cipher Suite il server sceglie un cifrario e una funzione hash e lo notifica al client.
* Il server fornisce anche un certificato di autenticità tramite il certificato digitale con la chiave pubblica di crittazione.
* Il client controlla il certificato.
* Il client per generare le chiavi di sessione ha due possibilità:
  * Si critta un numero casuale con la chiave pubblica del server mandando il risultato al server che decritta con la propria privata, il client e il server generano una chiave unica per la sessione usata con la crittografia simmetrica dei dati.
  * Si usa lo scambio Diffie Hellman per generare in modo sicuro una chiave casuale e unica per la sessione, questo garantisce anche la "forward secrecy".

Finito l'handshake correttamente inizia la comunicazione con i record, che contengono.

Attualmente il 3DES, l'IDEA e molti altri algoritmi sono stati eliminati tra le varie implementazioni dei Cipher Suite TLS in quanto insicuri, attualmente sono consentiti: AES, Camellia e ARIA che sono dei Block Cipher (TLS 1.3).

Le funzioni Hash (MD5, SHA, GOST) invece saranno eliminate a breve lasciando per ora l'AEAD (TLS 1.3).

Il protocollo TLS ha molti attacchi (sia MiM sia di altro genere) che vengono mitigati e sconfitti dai browser o dal protocollo stesso.

È molto importante usare un browser aggiornato che supporti le Cipher Suite più aggiornate.

