# Compiti Storia pg 121

1) i grafici ci informano sia sull'andamento della borsa di New York sia dell'andamento della produzione, dei salari e quindi degli effetti della crisi in USA e Europa. Ma non ci informano precisamente sulla produzione mondiale in quanto si sta lasciando da parte l'URSS e il Giappone che o hanno iniziato o sono già in intensa attività industriale.

2) c'è un crollo dei valori delle azioni delle aziende americane, nell'arco di un anno c'è quasi una dimezzazione del valore.

Durante la crisi la produzione, i salari e l'occupazione crollano.

3) è chiaro il collegamento tra la produzione e l'andamento della borsa; anzitutto i grafici possono esser sovrapposti ma inoltre consideriamo che nel capitalismo il valore di un'impresa alla fine è il valore dei beni o dei servizi che produce, il fatto che la produzione cali dà come conseguenza un crollo.

Dal 1932 inizia una ripresa o meglio rallenta la crisi, si nota che dal '31 al '32 i valori si stabilizzano rispetto ad una caduta esponenziale precedente. Si deve considerare inoltre che in Europa erano comunque presenti delle regolamentazioni protezioniste che riuscirono ad arginare l'effetto della crisi americana (che aveva portato ad un aumento eccessivo della produzione).

