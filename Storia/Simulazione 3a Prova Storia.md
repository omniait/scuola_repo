# Simulazione 3a Prova Storia

1. Il 1919 come anno cruciale
2. Crisi del 1929 in Europa
3. Fascismo come regime totalitario

1)

Il 1919 dal punto di vista mondiale sono discussi i trattati di Versailles che decidono sul nuovo assetto Geopolitico Europeo con l'importanza dell'autodeterminazione dei popoli.

Per l'Italia i patti hanno come ripercussione un malumore che porta alla questione di Fiume, occupata militarmente da un drappello di irredentisti tra cui D'Annunzio nel 1919.

Nel 1919 inoltre in un clima di grande mobilitazione (agitazioni nelle fabbriche 1919-1920 detto Biennio Rosso), nascono i Fasci di Combattimento fondato da Benito Mussolini; assieme nasce anche il Partito Popolare, primo partito cattolico in Italia da Don Luigi Sturzo.

2)

La crisi del 1929 fu determinata da una produzione (quindi offerta) eccessiva a cui non corrispose la domanda, si creò una crisi finanziaria iniziata dal crollo della borsa di New York che ebbe serie conseguenze anche in Europa e ciò accadde per la prima volta nella storia.

Una delle più importanti conseguenze fu la cessazione degli aiuti economici (Piano Dawes) ai paesi tra cui la Germania, nella quale si aggiunse una crisi ancor più grave già aggravata dall'eccessivo pagamento imposto dai trattati di Versailles. Questo aiutò l'ascesa del nazismo.

3) 

Per totalitarismo si intende un tipo di regime che inquadra l'individuo e la società fin dalla nascita e investe tutte le parti della sua vita, in cui vi sia un partito unico al potere e in cui vi sia un controllo centralizzato dell'economia, della politica, della cultura e della società.

In questo senso il fascismo può essere considerato un regime totalitario perché i giovani erano inquadrati nel regime fin dalla più tenera età, perché furono eliminate le opposizioni politiche, come Matteotti; anche l'uso della propaganda e controllo su scuole e cultura sono elementi determinanti.

Il Fascismo è considerato però un totalitarismo imperfetto in quanto è limitato dalla Monarchia e dalla Chiesa a differenza del Nazismo.