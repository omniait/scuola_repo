#1)

Trova la lunghezza della curva definita da:
$$
f(x)=1+x \sqrt x = 1 + x^{\frac{3}{2}}\\
0 \le x \le 1
$$
##Formula:

$$
L=\int_a^b \sqrt{1+[f'(x)^2]}
$$
##Regole Utili:

$$
f(x)=x^a \rightarrow f'(x) = ax^{a-1} \\
\int[f(x)]^nf'(x)dx = \frac{[f(x)]^{n+1}}{n+1}+c
$$
##Risoluzione:

$$
f'(x) = 0+ \frac{3}{2} * x^{\frac{1}{2}} = \frac{3}{2}\sqrt x \\
[f'(x)]^2=\frac{9}{4}x
$$

$$
D\left[1+\frac{9}{4}x\right]  = \frac{9}{4}
$$


$$
\int \sqrt{1+\frac{9}{4}x} = 
\int \left( 1 + \frac{9}{4}x  \right)^{\frac{1}{2}} * \frac{9}{4} * \frac{4}{9} = 
\frac{4}{9} \int \left( 1 + \frac{9}{4} x  \right)^{\frac{1}{2}} * \frac{9}{4} = \\
\frac{4}{9} * 2\frac{\sqrt{(1+\frac{9}{4}x)^3}}{3}+c = 
\frac{8}{27}\sqrt{\left(1+\frac{9}{4}x\right)^3}+c
$$

$$
\frac{8}{27}\sqrt{1+\frac{9}{4}x^3}|_0^1 =
\frac{8}{27}\sqrt{(1+9/4)^3} - \frac{8}{27} = 
\frac{8}{27} \left(\sqrt{(1+9/4)^3} - 1\right) = 
\frac{8}{27} \left(\sqrt{(13/4)^3} - 1\right)
$$

#2)

Trova l'area della superficie generata in una rotazione completa intorno l'asse x dall'arco di curva:
$$
f(x) = \sqrt{x+1} \\
0 \le x \le 3
$$
##Formula:

$$
S = \int_a^b 2 \pi f(x) \sqrt{1+[f'(x)]^2}
$$

##Regole Utili:

$$
Chain\ Rule: \\
y = g[f(x)] \\
y' = {g'[f(x)]}[f'(x)]
$$
##Risoluzione:

$$
f(x) = \sqrt{x+1} = (x+1)^{1/2} \\
f'(x) = \frac{1}{2} (x+1)^{-\frac{1}{2}} = 
\frac{1}{2} \left( \frac{1}{x+1} \right)^\frac{1}{2} = 
\frac{1}{2} \frac{1}{\sqrt{x+1}} =
\frac{1}{2\sqrt{x+1}} \\
[f'(x)]^2 = \frac{1}{4 (x+1)} \\
$$

$$
\int 2 \pi f(x) \sqrt{1+[f'(x)]^2} dx = 
\int 2 \pi  (x+1)^{1/2} \sqrt{ 1 + {\frac{1}{4 (x+1)}}} dx = 
2 \pi \int (x+1)^{1/2} \sqrt{ \frac{4(x+1) + 1}{4(x+1)}} dx = \\
2 \pi \int \sqrt{ \frac{4(x+1)^2 + (x+1) }{ 4(x+1)} } dx = 
2 \pi \int \sqrt{ \frac{4(x+1) + 1 }{ 4} } dx =  
2 \pi \int \sqrt{ x+1 +\frac{1 }{4} } dx = \\
2 \pi \int \sqrt{ x + \frac{5}{4} } dx
$$

Sostituzione:
$$
t = x + 5/4 \\
dt = 1 dx = dx \\
2 \pi \int \sqrt t \ dt = 
2 \pi \left( \frac{t^{\frac{3}{2}}}{\frac{3}{2}} \right) + c = 
4 \pi \left( \frac{(x + \frac{5}{4})^{\frac{3}{2}}}{3} \right) + c = 
\frac{4}{3} \pi \frac{\sqrt{(x+\frac{5}{4})^3}}{3} + c
$$

$$
\frac{4}{3} \pi \frac{\sqrt{(3+\frac{5}{4})^3}}{3} - \frac{4}{3} \pi \frac{\sqrt{(\frac{5}{4})^3}}{3} =
\frac{4}{3} \pi \left( \frac{\sqrt{(3+\frac{5}{4})^3}}{3} - \frac{\sqrt{(\frac{5}{4})^3}}{3} \right) =
\frac{4}{9} \pi \left( {\sqrt{(3+\frac{5}{4})^3 - {(\frac{5}{4})^3}}} \right) = \\
\frac{4}{9} \pi \left( {\sqrt{(\frac{17}{4})^3 - {(\frac{5}{4})^3}}} \right) =
\frac{4}{9} \pi \left( {\sqrt{\frac{17^3 - 5^3}{4^3}}} \right) =
\frac{4}{9} \pi \left( {\sqrt{\frac{17^2 * 17 - 5^2 * 5}{2^6}}} \right) =
\frac{4}{9} \frac{1}{8} \pi \left( \sqrt{17^2 * 17 - 5^2 * 5} \right) = \\
\frac{1}{18} \pi \left( \sqrt{17 - 5} \right)
$$

Questo è sbagliato: Dimenticato il +1(!)
$$
2 \pi \int (x+1)^{1/2} \frac{1}{2 \sqrt{x+1}} = 
\pi \int (x+1)^{1/2} \frac{1}{\sqrt{x+1}} = \\
\pi \int (x+1)^{\frac{1}{2}} * (x+1)^{-\frac{1}{2}} = \\
\pi x + c \\
S = \pi x |_0^3 = 3 \pi
$$
