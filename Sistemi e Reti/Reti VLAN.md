# Reti VLAN

Le VLAN sono definite dallo standard 802.1Q.

Gli switch consentono di suddividere la rete in domini diversi.

Si può anche operare a livello Network per avere domini di broadcast suddivisi, che è un insieme di computer che riceve un messaggio di broadcast e dove i dispositivi interni ad essa comunicano senza router.

In base al dispositivo si ha:

| Dispositivo | Dominio di Collisione  | Dominio di Broadcast   |
| ----------- | ---------------------- | ---------------------- |
| HUB         | Uno per tutte le porte | Uno per tutte le porte |
| Switch      | Uno per ogni porta     | Uno per tutte le porte |
| Router      | Uno per ogni porta     | Uno per ogni porta     |

Il fatto che gli switch inoltrino su tutte le porte un messaggio di broadcast può generare molto traffico, per questo sono state adottate le VLAN e il Layer 3 Switching.

1. VLAN - Si creano delle sottoreti che esistono solo nella topologia logica dello switch stesso
2. Layer 3 Switching - Quando un host di una VLAN deve comunicare con un host di un'altra VLAN servirebbe un router che metta in comunicazione le due (Inter-VLAN Routing), ma gli Switch Layer 3 hanno anche funzioni di router e possono leggere l'indirizzo di rete del pacchetto e "instradarlo" correttamente.

## Vantaggi e Svantaggi

Le VLAN permettono di ridurre il traffico di rete migliorando le prestazioni e aumenta la sicurezza.

È però complicato creare e gestire una VLAN.

Si può:

* Aggiungere, cambiare e spostare gli host
* Definire più domini di broadcast
* Migliorare la sicurezza
* Ridurre i costi degli apparati di rete

Una VLAN è però esposta ad attacchi di spoofing

## Creare una VLAN

Una VLAN è un insieme di dispositivi di rete raggruppati logicamente e si possono raggrupare coi seguenti metodi:

* Per gruppi di porte (Port based untagged)
* Per utenti (MAC host tagged)
* Per protocolli (IP tagged)

## Trunk

Il Trunk Link permette di ridurre il numero necessario di porte e cavi, bisogna indicare sui due switch quale è la porta Trunk e in seguito lo switch vedrà dove mandare il pacchetto.

## Untagged

Questa metodologia sfrutta i numeri delle porte degli switch (e non i dispositivi), ogni porta viene chiamata Access Port.

Operazioni compiute dallo switch:

* Ingress - Un frame in ingresso appartiene alla VLAN a cui è assegnata la porta quindi non ci sono meccanismi di riconoscimento.
* Forwarding: Il frame può essere inoltrato solo verso le porte appartenenti alla stessa VLAN della porta d'ingresso.
* Egress: Determinata la porta o le porte viene trasmesso senza modifiche.

## Tagged 802.1Q

Sono aggiunti 4 byte (VLAN-TAG) che contengono informazioni sulla VLAN, ogni porta viene anche chiamata Trunk Port.

Nasce il problema che, con l'aggiunta dei 4 byte, il pacchetto può superare la grandezza massima e quindi gli switch devono poterli accettare, interpretarli tramite delle porte speciali sullo switch (trunk, tagged, untagged).

La tecnologia che consente ciò è chiamata VLAN Trunking.

## Protocollo Cisco VPT

Diffonde in automatico delle VLAN create precedentemente su uno switch in base ai dispositivi di rete utilizzati.

## Inter VLAN Routing

Al fine di far comunicare due VLAN diverse si può usare un router connesso ad uno degli switch della LAN, ma il numero di collegamenti tra i due dev'essere pari al numero delle VLAN che devono comunicare, ad ogni porta del router corrisponde l'IP di una VLAN.

L'altro metodo è di usare il trunking (Router On A Stick) in cui si creano tante interfacce virtuali quante sono le VLAN che devono comunicare.

Ogni interfaccia virtuale ha l'IP della VLAN e la porta dello Switch a cui si collega il Router dev'essere configurata come **Trunk**.