# Guerra Civile Spagnola

1936 - 1939 Guerra Civile Spagnola

1931 - Si instaura la repubblica e iniziano le prime riforme.

1936 - Vince il "fronte popolare", coalizione di sinistra con repubblicani, anarchici, comunisti e socialisti, progressista. Continuano però le riforme senza intaccare i principi del capitalismo.

(Generalissimo) Francisco Franco Bahamonde - Con il supporto di Chiesa, monarchici e borghesi, inizia la guerra civile con truppe coloniali.

# Politica dell'Asse

Hitler si giustifica delle annessioni col "Pangermanesimo" unendo così la i popoli che parlano tedesco.

1938 - Anschluss - Annessione dell'Austria

1938 - 1939 - Trattati di Monaco - Annessione dei Sudeti in Cecoslovacchia e Annessione del rimanente lasciando la Slovacchia come stato fantoccio. Infine annessione di Memel.

1939 - Annessione da parte dell'Italia dell'Albania, firma del Patto d'Acciaio.

Domande:

* Cause della II guerra mondiale
* Schema avvenimenti 1938 - 1939 - 1940 - 1941