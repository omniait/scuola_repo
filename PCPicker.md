# Componenti PC

Type|Nome|Seriale|Price (€)
:----|:----|:----|-----
**CPU** | [Intel i3 8100](https://www.bpm-power.com/it/product/1734920/cpu_intel_core_i3_8100_3_6ghz_6m_s1151_box_8th_generazione_bx80684i38100.html) | **BX80684I38100** | 99,02 
**CPU Cooler 1** *Opzionale* | [Arctic Freezer 33](https://www.bpm-power.com/it/product/1694214/dissipatore_cpu_arctic_freezer_33_intel_acfre00028a.html) | **ACFRE00028A** | 26,40 
**CPU Cooler 2** *Opzionale* | [Arctic Freezer 33 eSports](https://www.bpm-power.com/it/product/1731962/dissipatore_cpu_arctic_intel_freezer_33_esports_e_y_acfre00034a.html) | **ACFRE00034A** | 42,38 
**CPU Cooler 3** *Opzionale* | [Alpenföhn Brocken ECO](https://www.bpm-power.com/it/product/1324896/dissipatore_alpenf_hn_brocken_eco_115x_1366_2011_am2_am3_fm1_reta_84000000106.html) | **84000000106** | 33,98 
**Motherboard** | [ASUS Prime B360 Plus](https://www.bpm-power.com/it/product/1809517/scheda_madre_intel_1151_asus_prime_b360-plus_90mb0wb0-m0eay0.html) | **90MB0WB0-M0EAY0** | 79,96 
**RAM 1** 2400MHz | [Corsair - Vengeance LPX 8GB (1 x 8GB)](https://www.bpm-power.com/it/product/1588525/ram_dimm_ddr4_8gb_corsair_2400_c16_ven_cmk8gx4m1a2400c16.html) | **CMK8GX4M1A2400C16** | 72,76 
**RAM 2** 2400MHz | [GSkill Aegis 8GB (2 x 4GB)](https://www.bpm-power.com/it/product/1726117/ram_dimm_ddr4_8gb_gskill_aegis_2400_c17_k2_f4-2400c17d-8gis.html) | **F4-2400C17D-8GIS** | 76,18 
**RAM 3** 2400MHz | [TeamGroup Vulcan (1 x 8GB)](https://www.bpm-power.com/it/product/1814423/ram_dimm_ddr4_8gb_team_vulcan_2400_c16_bulk_tlgd48g2400hc16bk.html) | **TLGD48G2400HC16BK** | 65,83 
**Storage SSD** | [Kingston A400 240GB](https://www.bpm-power.com/it/product/1703031/ssd_240gb_kingston_a400_sa400s37_240g.html) | **SA400S37/240G** | 47,47 
**Storage HDD 1** | [Seagate Barracuda 1TB](https://www.bpm-power.com/it/product/1672211/hard_disk_3_5_1tb_seagate_barracuda_st1000dm010_sata_iii_d_st1000dm010.html) | **ST1000DM010** | 36,50 
**Storage HDD 2** | [Western Digital Blue 1TB](https://www.bpm-power.com/it/product/1031742/hard_disk_3_5_1tb_western_digital_7200_64mb_sata3_wd10ezex.html) | **WD10EZEX** | 35,61 
**GPU** | Intel UHD Graphics 630 | / | 0,00 
**Case 1** | [Phanteks Eclipse P400](https://www.bpm-power.com/it/product/1626357/case_phanteks_eclipse_p400_midi-tower_window_nero.html) | **PH-EC416P_BK** | 72,14 
**Case 2** | [Aerocool LS-5200](https://www.bpm-power.com/it/product/1679994/case_aerocool_ls-5200_middle_tower_atx_bianco_en58324.html) | **EN58324** | 46,88 
**Power Supply 1** | [Corsair CX550M 550W](https://www.bpm-power.com/it/product/1590325/alimentatore_550w_corsair_cx550m_cp-9020102-eu.html) | **CP-9020102-EU** | 59,68 
**Power Supply 2** | [LEPA MXF1 600W](https://www.bpm-power.com/it/product/1298256/alimentatore_600w_lepa_n600-sb-eu_mx-f1-600w.html) | **MX-F1-600W** | 40,08 

