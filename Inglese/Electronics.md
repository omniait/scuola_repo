# Electronics

Electronics is the branch of science which deals with electricity and its applications.

We can identify 2 categories of components:

* Active - An active component amplifies an electric signal, which means that the total power is more than before the amplification. 
  This also means that it requires an external power source. It is applied to signal and information processing.
* Passive - A passive component doesn't require a power source and can reduce the power.

Digital information processing is possible thanks to electronic devices acting as switches.

Electronics is different from Electrical and Electro-Mechanical science because it studies the flow, control and different behaviours in diverse environments of electrons.

In 1906 Lee De Forest invented the Triode; it is a non-mechanical device that makes possible electrical amplification of weak signals.

Until 1950 this field of science was called radio technology because of its pricinal application.

Today most electronic devices use semiconductor components to perform electronic control; semiconductors are studied by solid-state physics but the design and construction of electronic circuits come under electronics engineering.

