# Schema Storia

## Avvenimenti

### Europa

* Triplice Alleanza (1882) - Italia, Austro-Ungheria e Germania; Patto di alleanza difensivo
* Entente Cordiale (1904) - Regno Unito e Francia
* Entente Cordiale 2.0 (1907) - Si unisce la Russia e cambia nome in Triplice Intesa
* Guerre Balcaniche (1912 - 1913) - La prima guerra balcanica vede la Lega Balcanica (Grecia, Bulgaria e Serbia) contro l'Impero Ottomano, vince la Lega, litigano tra loro però non vanno da uno psicologo di gruppo, la cosa peggiora e la Bulgaria va in guerra contro tutti e si unisce anche la Romania per rubare terre alla Bulgaria (da bravi Rom)
* Attentato di Sarajevo (28 Giugno 1914) - Muore Franz Ferdinand, principe d'Austria
* Prima guerra mondiale (28 Luglio 1914)
* Rivoluzione di Febbraio, Nikolay II abdica e Rivoluzione D'Ottobre (1917) - Grande sciopero che porta alla Rivoluzione di Febbraio a Pietrograd dove si obbliga l'imperatore ad abdicare, si forma un governo provvisorio non favorito dai soviet; Lenin torna dall'esilio e dà vita al partito Bolscevico fondato sulle sue Tesi di Aprile, ad Ottobre i bolscevichi assaltano il Palazzo d'Inverno e si afferma il governo leninista
* Trattati di Brest-Litovsk e Comunismo di Guerra (1918) - La Russia di Lenin firma un trattato di pace svantaggioso. Il Comunismo di guerra è una dottrina che prevedeva: collettivizzazione e riduzione delle libertà civili e politiche
* Occupazione di Fiume, elezioni (1919) - Nelle elezioni vincono il PSI (Partito Socialista Italiano) ed il Partito Popolare (Cattolico), i fasci di combattimento di Mussolini non prendono neanche un seggio
* Biennio Rosso (1919 - 1920) - Periodo di grandi tensioni sociali, gli operai occupano le fabbriche autogestendosi con dei consigli di fabbrica ispirati ai Soviet.
* Torna Giolitti al governo, trattato di Rapallo (1920) - Col trattato di Rapallo vengono annesse Istria e Zara e Fiume viene sgomberata
* Elezioni in Italia, fondazione del PNF, NEP in URSS (1921) - Con la NEP il libero commercio diventa legale ma continua la collettivizzazione. In Italia i fascisti si presentano con la lista di Giolitti ed entrano in parlamento
* Marcia su Roma (28 Ottobre 1922) - Il re Vittorio Emanuele III non proclama lo stato d'assedio e incarica Mussolini di formare un nuovo governo; il PNF giunto al potere governa con autorità
* Formazione dell'URSS (1922) - Si forma l'URSS con quasi tutti i territori dell'ex Impero Russo
* Elezioni in Italia, assassinio di Matteotti e ritiro sull'Aventino (1924) - Il PNF prende il 65% dei consensi tramite brogli e violenza, Matteotti denuncia tutto ciò e viene assassinato, i partiti di sinistra formano un nuovo parlamento all'Aventino.
* Leggi Fascistissime (1925 - 1926) - L'Italia si trasforma in uno stato totalitario, diventano illegali i partiti, le libertà politiche e di espressione vengono annientate, la stampa viene censurata, il parlamento non ha più potere e nasce la polizia di stato
* Primo Piano Quinquennale (1928)
* Grande Crisi economica globale e patti Lateranensi (1929)
* NSDAP guadagna molti consensi ed alle elezioni si nota che han guadagnato consensi nelle classi più deboli (1930).
  Le SA e le SS si scontrano e vincono le SS
* Incendio del Reichstag (1932)
* Hitler diventa cancelliere (1933), Hindenburg come Capo di Stato, smantella la repubblica tedesca istituendo un regime totalitario.
* Notte dei lunghi coltelli (1934) - Le SS uccidono le SA
* Leggi di Norimberga (1933 - 1935)
* Invasione e conquista dell'Etiopia (1935 - 1936)
* Occupazione della Renania (1936): Hitler remilitarizza l'area del Reno (Renania)
* Invasione e conquista dell'Etiopia (1935 - 1936)
* Leggi di Norimberga (1933 - 1935)
* Notte dei lunghi coltelli (1934) - Le SS uccidono le SA
* Hitler diventa cancelliere (1933), Hindenburg come Capo di Stato, smantella la repubblica tedesca istituendo un regime totalitario.
* Incendio del Reichstag (1932)
  Le SA e le SS si scontrano e vincono le SS
* NSDAP guadagna molti consensi ed alle elezioni si nota che han guadagnato consensi nelle classi più deboli (1930).
* Grande Crisi economica globale (1929)
* Primo Piano Quinquennale (1928)
* Patto anti-comintern (1936): I vari regimi mondiali di destra si schierano contro l'Unione Sovietica e l'Internazionale Comunista (detta appunto Comintern) con un patto difensivo, vediamo come partecipanti principali Germania e Giappone, in seguito si uniranno Italia, Spagna, Finlandia, Romania, Bulgaria, Ungheria...
* Nuova Costituzione in Unione Sovietica (1936)
* Asse Roma Berlino Tokyo (1936 - 1937)
* Guerra civile Spagnola (1936 - 1939): Diventa un campo di prova dei nuovi armamenti, sale al potere il regime autoritario del "Generalissimo" Francisco Franco Bahamonde. Si scontrano apertamente volontari Sovietici contro volontari Italiani e Tedeschi. Alcuni intellettuali come Hemingway, Orwell.


* Anschluss (Primavera 1938): Hitler occupa e annette l'Austria, azione approvata anche dagli stessi austriaci e senza conflitto


* Cecoslovacchia (Settembre 1938): Hitler riesce ad ottenere prima la zona dei Sudeti (Sudentenland), nei trattati di Monaco, e in seguito (1939) l'intera Repubblica Ceca mettendo uno stato fantoccio nella Slovacchia


* Kristallnacht / Notte dei Cristalli (Novembre 1938): Le SS prendono di mira i civili di origine ebrea commettendo stragi e saccheggi
* Leggi razziali in Italia (1938)
* Memel (1939): Hitler riesce ad ottenere Memel
* Polonia / Regno Unito (1939): Il Regno Unito garantisce l'indipendenza polacca
* Albania (1939): Mussolini invade e occupa l'Albania
* Patto d'Acciaio (1939): Italia e Germania formalizzano la propria alleanza
* Patto Molotov - Von Ribbentrop (1939): Germania e Unione Sovietica sottoscrivono segretamente un patto per l'annessione e la spartizione della Polonia, il trattato prevede pubblicamente anche un Patto di non Belligiranza tra le due nazioni che sospende temporaneamente il Patto Anti-Comintern
* Inizia la guerra (1 Settembre 1939): La Polonia non rispetta l'ultimatum per cedere il corridoio di Danzica, che tra l'altro era una città stato, ad Hitler; la Germania dichiara guerra alla Polonia, Francia e Regno Unito rispondono e inizia la Seconda Guerra Mondiale
* Guerra d'Inverno (1939 - 1940): L'Unione Sovietica dichiara guerra alla Finlandia per espandere i propri territori, riesce ad ottenere la regione della Karelia
* Danimarca / Norvegia (1940): La Germania si espande in Danimarca e Norvegia, prevedendo di usare quest'ultima per l'acqua pesante, usata nella ricerca nucleare, come base di attacco navale contro il Regno Unito e per assicurarsi il flusso di acciaio dalla Svezia dopo il blocco navale da parte inglese
* Espansione Sovietica (1940): L'Unione Sovietica occupa i paesi baltici e la Bessarabia
* Francia (1940): La Germania, dopo un anno di stallo contro la linea Maginot, decide di accerchiare le fortificazioni dichiarando guerra e conquistando Belgio, Olanda e Lussemburgo (simile al Piano Schlieffen), in pochi mesi, 3, la Francia viene conquistata e istituito il governo collaborazionista con Petain e la Francia di Vichy
* Regno Unito (1940): La battaglia per il Regno Unito si combatte principalmente per Aria e per via propagandistica facendo leva sul fatto che il Regno Unito era solo, a causa della schiacciante superiorità navale inglese, dove si scontrano RAF e Luftwaffe mantenendo però uno stallo senza nessun chiaro vincitore. La Germania inizia i bombardamenti una volta ottenuta la superiorità aerea ma il Regno Unito grazie ai radar ed ai bunker riesce a minimizzare le perdite e il morale degli inglesi rimane alto
* Patto Tripartito (1940): Germania, Giappone e Italia firmano un trattato di alleanza e cooperazione militare decidendo anche come spartirsi l'Europa e l'Asia.
  Si uniranno anche Ungheria, Romania e Jugoslavia in seguito
* Grecia e Jugoslavia (1940 - 1941): L'Italia tenta di invadere la Grecia (si parla di guerra parallela) ma fallisce, però nel 1941, grazie all'intervento tedesco l'Asse conquista questi territori assieme alla Jugoslavia affermando completamente il controllo dell'asse sui Balcani
* Operazione Barbarossa (Estate 1941): La Germania infrange il patto di non belligiranza e invade l'Unione Sovietica rompendo il patto di non-belligiranza, assieme alle truppe naziste, che sono 2 milioni, si uniscono Italia (ARMIR - Armata Italiana in Russia - 200'000), Romania, Ungheria.
  L'Operazione viene rallentata dall'inverno (Generale Inverno) a causa dell'equipaggiamento non invernale nazista, Leningrado è stata assaltata per mesi che cade non riescono però a conquistare Mosca.
  Durante l'inverno i sovietici muovono le industrie belliche verso gli Urali e iniziano una produzione di massa di carri armati e caccia
  * Ragioni per l'invasione: 
    * Ideologiche (Abbattere il comunismo)
    * Espansione verso Est ("Lebensraum" - Lo spazio vitale della Germania)
    * Timore di un'alleanza tra Comitern (URSS) e Alleati (UK)
* La Germania, attende l'estate successiva e conquista la Crimea tentando un'espansione verso sud per il petrolio di Baku dal resto del paese, e avanza fino a Stalingrado senza conquistarla (1941 - 1942)
* Il teatro di guerra europeo rimarrà il principale e vedrà 30 milioni di morti solo in Unione Sovietica e 10 milioni di nazisti, la prima sconfitta dei nazisti si ha a Stalingrado nel 1942 / 1943 che fu la più grande battaglia del conflitto (si stimano quasi 2 milioni di morti in soli 2 anni, che vuol dire 2800 morti al giorno per 2 anni!), questa battaglia segnerà l'inizio della controffensiva sovietica che non si fermò
* Sbarco americano in Sicilia (1943) - In seguito crolla il fascismo in fretta
* Offensiva in Normandia (1944) - Gli alleati nel DDay tentano di invadere la Francia nord, riuscendo, grazie al minimo dispiego nazista di truppe
* Nascono le repubbliche tedesche (1949) - Nasce la repubblica federale filo-americana e la repubblica democratica filo-sovietica
* Muore Stalin e Kruscev diventa leader (1953)
* Muro di Berlino (13 Agosto 1961)

### Asia - Oceania - Pacifico

* Seconda guerra tra Cina e Giappone (1937 - 1941): Il Giappone dichiara guerra e tenta di invadere la Cina. Prima della guerra la Germania teneva buone relazioni con la Cina e il generale Von Falkenhausen (1938) assieme a dei volontari che combattevano da parte cinese, verrà richiamato in Germania più avanti (nel 1940) quando i rapporti tra Germania e Giappone miglioreranno grazie al Patto Tripartito
  * Questa guerra fu molto sanguinosa, si stimano 20 milioni di morti cinesi e 3 milioni giapponesi, e indebolirà la portata militare "su terra" giapponese
* Il teatro di guerra Asiatico si espanderà negli anni successivi con l'annessione dell'Indocina e l'occupazione delle Filippine da parte del Giappone (dopo la caduta della Francia) e la guerra nel Pacifico con gli Stati Uniti dal 1941 (famosa è la battaglia per Pearl Harbor a fine anno e verso la fine del conflitto la conquista di Iwo Jima)
* Giappone annette l'Indocina (1941) ed attacca la flotta americana stanziata a Pearl Harbour (Dicembre 1941); questo apre il fronte pacifico e segna l'entrata in guerra degli USA.
* Contemporaneamente Il Giappone riesce ad annettere le Filippine, Nuova Guinea, Malesia; si muove verso l'Australia e la Nuova Zelanda.
* Gli americani, a fronte di una sconfitta iniziale, riescono a guadagnare terreno (Midway e Guadalcanal)

### Africa

* Guerra Italiano-Etiope (1935 - 1936): Mussolini invade l'Etiopia, uno stato riconosciuto dalla società delle nazioni che segnerà ufficialmente la fine dei rapporti tra Italia e le potenze democratiche
* Nord - Africa (1940 - 1941): L'Italia tenta un'offensiva in Egitto che viene respinta in modo decisivo dagli inglesi, l'intervento degli Afrika Korps tedeschi guidati da Erwin Rommel, la "Volpe del Deserto", riesce a far ripiegare gli inglesi. L'Italia perde nel 1941 l'Etiopia e la Somalia e perdendo quindi il controllo in Africa Orientale
* Gli Afrika Korps guidati da Rommell giungono ad El-Alamein (1942)
* Giunge l'aiuto americano in Africa guidato dal generale Patton, inizia la ritirata dall'Africa
* Conferenza a Teheran (1943)

### Globale

- Prima crisi marocchina (1905) - Germania garantisce l'indipendenza Marocchina
- Seconda crisi marocchina (1911) - Francia occupa il Marocco, la Germania interviene con una corazzata che viene ritirata in seguito all'ammonimento inglese
- USA entrano in guerra e la Russia ne esce a causa della guerra civile (1917) - Nikolay II abdica e viene istituito il soviet
- Firma dei trattati di pace di Versailles (1918 - 1919) - Viene considerata per la prima volta l'autodeterminazione dei popoli, la carta politica europea cambia aspetto, viene istituta la Società delle Nazioni, la Germania viene smilitarizzata, indebitata e umiliata
- Carta Atlantica (1941) - Si incontrano sull'isola di Terranova Roosevelt (Capo di Stato) e Churchill (Capo del governo); sia i democratici che l'Unione Sovietica si uniscono.
  - Principi della carta atlantica (sono comunque ispirati ai 14 punti di Wilson):
    1. I popoli hanno il diritto di scegliersi il proprio governo (Autogoverno).
    2. Tutti i paesi devono avere accesso al commercio e alle risorse mondiali
    3. La cooperazione è il motore del progresso economico e sociale
    4. Bisogna distruggere la tirrania nazista
    5. Tutte le nazioni devono abbandonare l'uso della forza
- Fine della guerra (1945) - Cade prima la Germania e poi il Giappone dopo il bombardamento nuculare, strettamente necessario e sufficiente, su Hiroshima e Nagasaki; Il mondo si divide in due blocchi, uno filo-sovietico ed uno filo-americano
- Piano Marshall (1947) - Aiuti economici ai paesi distrutti dalla guerra, vengono accolti dai paesi liberali ma non dai paesi comunisti
- NATO o Patto Atlantico (1949) - Alleanza difensiva anti-Comintern
- Comecon (1949) - Aiuti economici per lo sviluppo dell'Europa dell'Est
- Patto di Varsavia (1955) - Alleanza tra i paesi comunisti

## Cause della 2a Guerra Mondiale

Le Cause della 2a Guerra Mondiale sono molteplici e di varia natura.

I trattati di Versailles aiutarono l'ascesa del nazismo in Germania a causa della gravosità della pena inflitta ad essa. 

La Germania si ritrovava in uno stato di crisi economica senza precedenti, c'erano carestie e malcontento generale e Hitler riuscì a sfruttare questa situazione, individuando come nemici gli Ebrei, i quali venivano considerati sia come nemici dei cattolici (ricordiamo che la Chiesa si schierò con questi regimi di destra) sia come popolo ricco dato che possedevano la maggior parte delle imprese e delle banche tedesche, e l'occidente liberal-democratico in generale (quindi Francia, Regno Unito, USA); infatti una caratteristica del Nazismo è l'odio verso le forme liberali e democratiche.

La massima crisi economica tedesca si ha dopo il 1929 a causa della crisi di Wall Street che constrinse gli USA a ritirare gli aiuti economici in Europa.

Hitler sale al potere e instaura un regime totalitario "perfetto".

Le potenze democratiche sottostimarono la portata del nazismo e di Hitler, credendo che si sarebbe limitato e che non sarebbe durato molto, ma Hitler aveva già pianificato tutto, la Germania doveva diventare la GrossDeutscheland che aveva bisogno del suo "Spazio Vitale" (Lebensraum), un territorio che si espandeva dalla Germania agli Urali e dall'Austria alla Norvegia includendo Danimarca e Svezia. Ricordiamo che nei territori occupati i nazisti deportavano e commettevano stragi al fine di "germanizzare" questi stessi territori (es. Ucraina).

Una prova della sottostima del nazismo si ha nei trattati di Monaco perché la Francia in realtà era già garante della Cecoslovacchia, l'URSS si era data disponibile a entrare in guerra assieme alle potenze democratiche contro la Germania se la Francia fosse intervenuta ma questa decise di non intervenire e Hitler riuscì ad avere un'altra vittoria diplomatica.

La scintilla è stata data dalla dichiarazione di guerra alla Polonia da parte Tedesca in quanto la Polonia era protetta dal Regno Unito che decise assieme alla Francia finalmente di intervenire.

## Tecnica Bellica

La Seconda Guerra Mondiale si combatte per Terra, per Aria e per Mare.

Per terra i carri armati hanno raggiunto un ampio utilizzo, famosi i Panzer e i Tiger tedeschi, il T34 sovietico, lo Sherman americano.

Per mare la Germania usò una tecnica di guerra sottomarina tramite gli U-Boat con estensione dei conflitti per mare fino all'oceano.

Per aria si utilizzano molto gli aerei, caccia (Spitfire della RAF inglese, Messermicht tedeschi e Sturmovik sovietici) e paracadutisti.

Si afferma l'industria bellica pesante sia in Germania che in URSS.

## Leggi Razziali

Nei due regimi fascisti furono applicate delle leggi razziali, prima in Germania nel 1935 le leggi di Norimberga e nel 1938 le leggi Bottai.

A riguardo si pongono due visioni da parte degli storici, da una parte si crede che le leggi Bottai siano in realtà una conseguenza dell'avvicinamento Germania-Italia e che siano in qualche modo costrette ad essere applicate ed estese in Italia.

Un'altro punto di vista invece sostiene che ci sia stato davvero un problema "della razza italiana", meticci, ebrei, africani, etc. doveva essere risolto, ecco il motivo de "il manifesto della razza", firmato da degli scienziati, e della rivista "la difesa della razza" dato che comunque l'Italia fascista si rifaceva alla Romanità.
