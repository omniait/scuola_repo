# IA - Un problema etico 

## di Simone Romano

Tabella dei contenuti

[TOC]

<div style="page-break-after: always;"></div>

## Introduzione

L'intelligenza artificiale (IA) è l'intelligenza mostrata dalle macchine, a differenza dell'intelligenza naturale degli umani e degli animali.

La storia delle Intelligenze Artificiali affonda le sue radici nel mito; si è ritenuto, infatti, che nella storia di Talos possa essere individuata una forma ancestrale di IA. 

Talos era, infatti, un gigante di bronzo opera di Efesto. Da quest'ultimo e da Zeus fu mandato in dono a Minosse, perché custodisse l'sola di Creta, a tale scopo doveva percorrerla tre volte al giorno. 

Il gigante operava in maniera autonoma, per questo motivo può essere considerato una prima forma di Intelligenza artificiale. 

L'episodio di Talos rientra nel mito degli Argonauti; infatti, quando essi giunsero a Creta Talos tentò di bloccarli ma Medea riuscì a togliergli il perno sul tallone, unico punto in cui era vulnerabile e così morì.

| Illustrazione Medeia and Talus di Sybil Tawse <br> (1920 - Wikimedia) | La morte di Talos <br> (~IV secolo AC - Museo Ruvo di Puglia) |
| ------------------------------------------------------------ | ---- |
| <img src="./assets/Medeia_and_Talus.png" style="width: 100em;"> | <img src="./assets/vasoTalos.JPG" style="width: 100rem"> |

Nell'arco dei millenni le IA hanno preso sempre più piede nella narrativa o nel cinema di finzione (per esempio **Io, robot** ed altri libri di *Isaac Asimov*, **Star Wars**, **Terminator**), *Turing* diede un avvio agli studi scientifici degli anni dopo ed è famoso lo *Standard Turing Test*.

Quello che oggi viene effettivamente considerato come intelligenza artificiale è un campo complesso di cui forse il più importante è il *Machine Learning*.

<div style="page-break-after: always;"></div>

## IA moderne

Si parla di reti neurali dal 1940, sono state inventate grazie alla neuroscienza; però i primi algoritmi teorizzati furono poco usati a causa della potenza hardware e alla mancanza di dati del tempo.

L'evento fondamentale che ha risvegliato l'interesse scientifico nelle IA, visto che a causa di una mancanza di efficienza e di potenza computazionale, è stato nel 1986, quando sono state riproposte le *NN*, le reti neurali, grazie ad una nuova teoria (la *back-propagation*).

Le *ANN* si sono diffuse su larga scala a partire dal 2012 in poi grazie agli avanzamenti tecnologici a livello di potenza di calcolo e alla diffusione di massa di Internet che fornisce ogni giorno moltissimi dati.

Al giorno d’oggi le IA sono presenti in ogni campo, anche nelle attività quotidiane e primeggiano nei videogiochi, come avevano previsto anni prima dai ricercatori nel campo dell'Intelligenza Artificiale, anche se bisogna considerare il tipo di Intelligenza Artificiale implementata che può essere la più classica (ovvero un Albero Decisionale) o una *ANN* come si può vedere in *Blietzkrieg 2*. 

Vi sono programmi che sono stati in grado di confrontarsi con campioni di scacchi, quali *Deep Blue*, un'implementazione *ANN*; altri che sono stati impiegati nelle missioni spaziali, come nel 1998 quando la NASA utilizzò un programma chiamato *Remote Agent* in grado di gestire le attività relative a un sistema spaziale; alcune auto sono oggi dotate di un sistema in grado di guidarle senza l’uso di un conducente umano e quindi in maniera del tutto autonoma.  Nell’ambito di scenari più quotidiani si pensi, invece, ai termostati per il riscaldamento e l’aria condizionata in grado di anticipare il cambio di temperatura, gestire i bisogni degli inquilini e di interagire con altri dispositivi. 

Le *ANN* sono state inventate grazie ad una collaborazione tra la neuroscienza e la matematica, sono ispirate ai neuroni, e proprio per questo si parla di *Neural Networks*, ovvero reti di neuroni.

Le *ANN* vengono inoltre usate per identificare in tempo utile un tumore o un cancro, si possono applicare nel mondo della finanza, nella *Computer Vision* (in particolare le *CNN* sono ampiamente usate in questo campo) o per prevedere crimini.

## Reti Neurali

Le reti neurali fanno parte del *Deep Learning*, un sotto-insieme del *Machine Learning*.

Le reti neurali sono una risposta unica e globale a tanti problemi di svariati campi (come *Computer Vision* *CV*, ovvero elaborare le immagini da parte di un computer, *Algorithmical Trading*, ovvero l'analisi finanziaria e il *Natural Language Processing* (*NLP*), come gli assistenti digitali (*Alexa*, *Cortana*) o gli *screen reader*) che erano affrontati in modi diversi con algoritmi diversi (per esempio nel *trading* possiamo pensare all'algoritmo *SVR*).

Si può suddividere la "nascita" di una rete neurale in 6 fasi:

1. Raccolta dati 
   * Raccogliere i dati di una certa realtà (crimini suddivisi per quartiere in una città; data, luogo di terremoti e magnitudo).
2. Esaminazione dati
   * Assicurarsi che i dati siano privi di errori e che siano coerenti tramite algoritmi statistici semplici come varianza, regressione lineare o *SVR.*
3. Normalizzazione o standardizzazione dei dati
   * Si applicano trasformazioni matematiche di statistica chiamate normalizzazione o standardizzazione che "spostano" i valori all'interno del *range* -1; 1 o 0; 1.
4. Preparare i dati in un formato leggibile dal linguaggio di programmazione
   * Spesso si usa il formato aperto .CSV (*Comma Separated Values*) in quanto è semplice da leggere, sia da un umano che da un programma, e si comporta come una tabella; inoltre nel linguaggio di programmazione *Python* è facile da leggere, senza implementare un *parser*, grazie alla libreria *Pandas*. Esistono però altri formati utilizzabili come .XML o .JSON.
     È importante suddividere i dati in due *dataset*, uno di *training* ed uno di *testing*, si mantiene convenzionalmente una percentuale attorno al 80% (*training* / *testing*).
5. Elaborazione dei dati da parte dell'IA - Fase di *training*
   * Nella fase di *training* il programma cerca di comprendere da solo quale logica è dietro al variare dei dati, questo avviene tramite la regolazione dei "pesi" (*weights*) che sono i collegamenti, assieme alla loro importanza, tra i vari nodi.
   * In questa fase avviene anche il calcolo dell'errore che deve avvicinarsi allo 0 ma non essere uguale a 0 in quanto se succedesse si incorrerebbe in un problema: *Overfitting*, ovvero la copia da *input* ad *output*, rendendo le previsioni non corrette (per esempio se un'IA deve effettuare un OCR, ovvero esaminare un documento scritto a mano, e se si incorre nel *Overfitting* allora l'IA non riconoscerà lo stesso carattere se viene scritto leggermente diversamente, cosa normale durante la scrittura a mano).
6. Previsione, scrittura del *output* - Fase di *Prevision*
   * Finito il *training* si possono fare delle previsioni su altri dati in input, per esempio delle nuove immagini, senza dover effettuare il *training* di nuovo.

Grazie a *TensorFlow* è inoltre possibile creare una "sintesi" del *training*, questa sintesi è una collezione di file che mantiene le "deduzioni" della rete neurale e consente di effettuare future previsioni senza grosso peso computazione, permettendo a tutti gli effetti di eseguire programmi che si appoggiano sulle *ANN* su elaboratori più deboli come quelli degli smartphone.

<div style="page-break-after: always;"></div>

## RNN Recurrent Neural Network

Le *RNN* sono utilizzate per l'analisi di serie temporali, un esempio può essere l'andamento del valore di un titolo in borsa, fa parte del *Supervised Machine Learning* ed è ispirato principalmente dal *Temporal Lobe* del cervello (Lobo Temporale), ovviamente riguarda anche il resto del cervello e altri lobi ma quello temporale rimane il più importante.

L'algoritmo *LSTM*, che fa parte dell'insieme delle *RNN* e che ho usato per il mio programma, è stato teorizzato da un ricercatore svizzero ed uno tedesco (*Jürgen Schmidhuber* e *Sepp Hochreiter*) ed è oggi molto usato anche da compagnie come Google.

### Schema di una RNN

![](assets/RNN.png)

<div style="page-break-after: always;"></div>

### Singolo Neurone

![](assets/RNN_1.png)

Di conseguenza, analizzando una RNN con un input layer, un hidden layer e un output layer possiamo schematizzare così:

![RNN_2](assets/RNN_2.png)

<div style="page-break-after: always;"></div>

### Alcuni esempi di RNN

Le *RNN* si possono implementare in diversi modi.

Per esempio si possono avere più o meno *input, hidden* od *output layer*; combinandoli si hanno 4 possibili opzioni:

![examples](assets/examples.png)

#### I Esempio One-To-Many

Nel primo esempio (*One to Many*) abbiamo un singolo *input* (un'immagine), la *CNN* identifica il cane e la barra, ma la *RNN* si occupa di comprendere cosa sta facendo, cosa farà e dove arriverà, conclude quindi che il cane sta saltando e che si dovrà preparare ad atterrare. Questa è la stessa tecnologia che viene usata nelle auto a guida autonoma, infatti la *CNN* identifica prima i pedoni o altri oggetti presenti sulla strada, tramite delle telecamere poste nel paraurti anteriore, e in seguito la *RNN* cerca di prevedere le prossime azioni o percorsi di questi oggetti.

In verità ad esser precisi le auto a guida autonoma di oggi usano altre tecnologie un po' più avanzate. Però le prime sperimentate usavano la tecnica appena spiegata.

#### II Esempio Many-To-One

Nel secondo esempio l'obiettivo è la *Sentiment Analysis*, ovvero analizzare e comprendere quello che sta provando chi sta scrivendo, si tratta di un esempio *Many-To-One* perché *l'output* è in realtà un booleano (supponiamo che abbia valore *True* se la recensione è positiva, viceversa se è negativa *False*).

C'è disponibile in **Keras** il *dataset* IMDb con le varie recensioni dei film, è utile al fine di comprendere se una recensione scritta (o anche testi di altra tipologia) è positiva o negativa, lo stesso avviene nell'esempio: abbiamo una frase di cui possiamo identificare delle parole chiave(come *thanks, great, enjoyed*) che identificano un sentimento positivo.

<div style="page-break-after: always;"></div>

#### III e IV Esempio Many-To-Many

Supposto che a livello di *Many-To-Many* si possono identificare una certa quantità di *Input, Hidden* e *Output Layer*; possiamo avere una certa quantità di *Input* minore degli *Output* o viceversa, questo si vede proprio nella traduzione delle lingue, prendiamo per esempio **Google Translate** o **Bing Translate**; alcune lingue usano più o meno parole per esprimere le stesse cose.

Per esempio possiamo prendere in considerazione l'Italiano e l'Inglese.

| Input in Italiano | Output in Inglese |
| ----------------- | ----------------- |
| Come stai?        | How Are You?      |

Vediamo di conseguenza che dal "Stai" vengono isolate due elementi, soggetto e verbo. Il soggetto implicito "Tu" non può essere implicito nell'Inglese, infatti il soggetto deve sempre essere esplicito e quindi lo traduce con "You", l'altro elemento da "Stai" è il verbo "Are" (essere). Mentre "Come" viene tradotto senza problemi con "How".

In Tedesco invece "Come stai?" viene tradotto in "Wie Geht's?".

Oggi grazie alle intelligenze artificiali possiamo avere anche una traduzione simultanea quasi perfetta consentendoci di abbattere la barriera della conoscenza della lingua nei viaggi, potremmo immaginare di poter viaggiare in qualunque parte del mondo e comunicare con altre persone entrando in contatto con altre culture senza dover imparare le centianaia di lingue parlate oggi.

Un esempio invece di quando gli *Input* corrispondono direttamente agli *Output* è nell'analisi video, dove ogni *frame* (immagine) può essere isolato ed elaborato; possiamo, ad esempio, applicare dei filtri particolari, anche artistici, a un video dandogli un effetto alla "Van Gogh" o alla "Munch". Una foto è un caso *One-To-One* e quindi un video, che è una sequenza di immagini, è un *Many-To-Many* dove ogni frame ha un frame modificato in output.

| La stessa immagine con 4 stili diversi       | Una foto a cui viene applicato lo stile di Van Gogh |
| -------------------------------------------- | --------------------------------------------------- |
| ![](assets/deep-learning-painting-style.jpg) | ![](assets/deep-learning-behind-prisma-4-638.jpg)   |



Però iniziano a sorgere dei problemi etici, che bisogna porsi.

<div style="page-break-after: always;"></div>

## I Problemi etici delle IA

### Porsi il problema prima che diventi critico

La tecnologia ha sempre avuto due facce, pensiamo ai robot ed al nucleare; nel caso dei robot possiamo osservare i grandi progressi compiuti dalla Boston Dynamics che sta cercando di costruire dei robot che non possono cadere, camminano su terreni difficili, o riescono ad alzarsi dopo esser caduti.

Però la Boston Dynamics è finanziata principalmente dal Pentagono, e di conseguenza è evidente l'interesse nella potenzialità militare di questi robot; allo stesso modo l'energia nucleare si può usare per scopi pacifici, come la produzione di energia elettrica, campo in cui tra l'altro ci stiamo avvicinando alla fusione nucleare che permetterà di produrre energia nucleare senza scarti oppure per scopi militari, come furono le due bombe sul Giappone.

Bisogna di conseguenza porsi il problema di capire le potenzialità negative delle IA prima che queste possano effettivamente fare danni come la bomba nucleare.

Possiamo identificare dei problemi più immediati rispetto ad altri, supposto che comunque la Singolarità è ancora lontana, anche se non più di tanto (alcune previsioni indicano il 2045, altre il 2029).

Dobbiamo identificare i problemi etici e le possibili ripercussioni della scoperta e dell'avanzamento delle tecnologia ben prima che queste arrivino, sappiamo anche che la tecnologia cresce esponenzialmente e per questo abbiamo sempre meno tempo per considerare il fattore etico.

Servono conversazioni e discussioni subito perché la rivoluzione scientifica delle IA è già in atto. 

Però le domande etiche non sono del campo scientifico ma di quello filosofico perché sono domande che riguardano i nostri valori morali e sociali.

Le IA pongono molte questioni etiche, e ne elenco alcune, senza ovviamente dare una risposta che è compito della società.

<div style="page-break-after: always;"></div>

#### Dataset errati - IA discriminatorie

Gli algoritmi di *machine learning* imparano dal *dataset* che gli viene assegnato, senza considerare eventuali considerazioni scorrette di questo. Questo vuol dire che, se nei dati ci sono considerazioni anche con pregiudizi sessisti o razzisti (o anche altre discriminazioni), allora questi algoritmi possono mettere in luce e ingrandire queste stesse.

È già successo che le predizioni di questi algoritmi hanno dato del gorilla a dei neri, per esempio.

![img](assets/1-iv20BEfICgTuhXdjmDaSGg.png)

Allo stesso modo dobbiamo considerare che questi algoritmi sono usati attualmente per determinare chi è degno di ricevere un prestito o di essere assunto e, considerato che la società di oggi è particolarmente sessista, razzista e piena di discriminazioni, è evidente che chi non rientrerà nell'immagine comune di "Persona modello" avrà degli svantaggi gravi.

Come possiamo assicurarci che gli algoritmi siano giusti ed equi? Sopratutto considerando che sono usati dalle corporazioni e non sono accessibili al pubblico.

Sono state anche sperimentate delle IA che dovrebbero sostituire i giudici, il risultato è prevedibile date le considerazioni appena fatte, però c'è chi supporta la sostituzione dei giudici umani con questi "Giudici Artificiali" sostenendo che sarebbero molto più imparziali.

| Crimini precedenti  | Rischio di compiere ulteriori crimini |
| ------------------- | ------------------------------------- |
| ![](assets/bak.png) | ![](assets/dataset2cr1.png)           |

<div style="page-break-after: always;"></div>

#### Fake News

Consideriamo l'immagine sotto esposta.

![img](assets/0-cigfPW5eldLH22Ca.jpg)

Riusciamo a identificare il falso? Probabilmente no.

La persona in questione è Emma González, una sopravvissuta alla sparatoia di Parkland. l'immagine a destra è quella falsa dove lei straccia la Costituzione Americana, quest'immagine è diventata virale.

![img.](assets/U4UMLXRW6ZEP3PCKAZKZYZAQWY.jpg) 

Un problema importante posto negli ultimi tempi è identificare le notizie false, **Facebook** sta cercando di creare degli algoritmi per fare un "hunt and destroy" delle *fake news*, senza grande successo attualmente.

Il grosso problema è che le persone tendono a condividere e a credere più allle notizie false che a quelle vere, un esempio è stato lo scandalo **Cambridge Analytica** che ha creato e diffuso notizie false per influenzare le ultime elezioni americane o la storia di Emma.

Si possono anche falsificare i video, imponendo espressioni facciali ad un'altra persona, Face2Face è un esempio.

![](assets/face2face.jpg)

Se sappiamo che addirittura i video possono essere falsificati, cosa può essere accettato giuridicamente come prova? Come si può fermare l'espandersi delle fake news? Ma sopratutto chi o cosa deve decidere cos'è vero e cos'è falso?

<div style="page-break-after: always;"></div>

#### Armamenti autonomi

I ricercatori IA sostengono che in meno di un decennio potranno sviluppare dei sistemi letali autonomi di armi, sotto forme di droni che essendo autonomi decideranno da soli chi uccidere.

![https://cdn-images-1.medium.com/max/2000/0*eOynoFpo4Sl77k3o.png](assets/0-eOynoFpo4Sl77k3o.png)

Un recente video mostra come dei piccoli droni autonomi potrebbero esser usati per compiere genocidi, quasi 4000 ricercatori hanno firmato una lettera chiedendo di bandire le armi autonome offensive.

Considerando quello detto precedentemente sulle Reti Neurali (il riassunto di *TensorFlow*) si può capire come possa esser possibile "iniettare" le "deduzioni" della rete neurale che ha fatto un *training* su un altro elaboratore, possibilmente un *super-computer* o *HPC*.

Su che basi vanno bandite e vietate questi tipi di armi? Quando alcune nazioni potrebbero e vorrebbero usarle? Se venissero bandite come potremmo assicurarci che il divieto venga rispettato?

<div style="page-break-after: always;"></div>

#### Privacy e Sicurezza

La crescente presenza delle telecamere di sorveglianza e del riconoscimento facciale porta nuove domande da porsi attorno alla sorveglianza a cui è necessario rispondere per evitare una distopia Orwelliana.

Le telecamere a circuito chiuso (CCTV) sono già in grado di rintracciare e trovare le persone per strada; prima che venisse inventato il riconoscimento facciale, rimaneva comunque impossibile rintracciare tutti in qualunque momento, mentre ora è possibile grazie alle IA ed a una serie di telecamere.

![https://cdn-images-1.medium.com/max/1200/0*8bhPDVuYtXgCGYmB.png](assets/0-8bhPDVuYtXgCGYmB.png)

In Cina stanno iniziando ad usare le telecamere per monitorare i cittadini, alcuni agenti di polizia hanno anche ricevuto occhiali ed uno smartphone che permettono di individuare la singola persona in tempo reale tramite il riconoscimento facciale.

| Poliziotto con occhiali dotati di una telecamera collegata ad uno smartphone | Smartphone con le informazioni sulla persona riconosciuta |
| ------------------------------------------------------------ | --------------------------------------------------------- |
| <img src="assets/915205050.jpg.0.jpg">                       | <img src="assets/0__1_.jpeg">                             |


Dovrebbe esserci una regolamentazione contro queste tecnologie? Quale è il confine tra sicurezza e diritto alla privacy?

<div style="page-break-after: always;"></div>

## Compozr 0.44

### Gli obiettivi del programma

Compozr dovrebbe diventare un programma che prende uno o più file midi di uno stile simile, come può essere il genere musicale Barocco con composizioni di Pianoforte, e partendo da un *Dataset* già presente ed allenato con le regole musicali di base (per esempio scale, chiavi e accordi) tirar fuori uno spartito, sempre in file midi, dello stile richiesto in *Input*.

### How, il come

L'idea è di sfruttare le informazioni "pure", quindi le singole note, in particolare posizione e durata, in *Input*, elaborarle all'interno di una *RNN* e infine, terminato il *Training* dell'IA, tirar fuori uno spartito.

In particolare sono state effettuate le seguenti scelte:

* Usare Python come linguaggio, ben diffuso nella communità delle IA.
* Sfruttare le librerie Keras, che si basa su Tensorflow [^1] rendendo più immediata la programmazione, ed ottimizzare e velocizzare il training dell'IA.
* Usare mido per leggere e scrivere file midi estrapolando le singole note e poi scriverle nel buffer midi
* Usare SKLearn per il preprocessing dei dati (normalizzazione)

[^1]: Keras velocizza e ottimizza grazie ad un abstraction layer Tensorflow della Google, che è già più di basso livello e intricato dato che sfrutta le librerie OpenCL, Intel MKL o Nvidia CUDA. Keras ci dà anche un'altra possibilità, ovvero usare la libreria Theano.

### Problemi riscontrati e note

Il programma oggi come oggi non è completo, ecco perché la versione non è ancora come "1.x", infatti i suoi obiettivi non sono stati raggiunti.

In particolare ad oggi l'implementazione contiene:

* Un solo file midi in input
* Analisi minimale dello spartito, in particolare scarta tutti i metadata (quindi anche la chiave e il tempo)
* Un solo file in output che però è particolarmente inascoltabile

Sono stati già modificati più volte i parametri degli *Hidden Layer* (epochs, dropout, varie loss function) senza però avere risultati accettabili.

L'esperimento non risulta del tutto fallito perché il programma riesce a leggere un qualunque file midi e scrive comunque un file midi correttamente. Va trovato il modo di dargli delle conoscenze di teoria musicale di base perché nello stato attuale impara solo dagli spartiti unici senza conoscenze pregresse rendendo ovviamente più difficile l'apprendimento.

La documentazione è pressocché assente al di fuori del codice, anche a causa della semplicità del programma; manca la documentazione utente essendo un programma non completo. 

Nello stato attuale il modello è così costituito:

* Activation Function Sinusoidal: f(x) = sin(x)
* 2 Layer LSTM (Long-Short Term Memory)
* 2 Dropout al 20% in seguito ad ogni Layer
* Loss Function Mean Squared Error
* Optimizer Function RMSprop

Il modello costruito inizia poi a prevedere singolarmente ogni nota in base alla precedente, essendo però costituito da *layer LSTM* questo dovrebbe essere in grado di "ricordare" anche le precedenti note, teoricamente migliorando la previsione.

La predizione si basa su un *array* di 3 elementi che rappresentano i dati necessari per creare una nota midi, questi elementi vengono anche controllati per vedere se sono ragionevoli.

#### Conclusione

È stato un progetto interessante da sviluppare e, pur non raggiungendo l'obiettivo finale, rimane un passo avanti verso un'IA che riesce a comporre, ad oggi esistono altri progetti che han raggiunto un maggiore successo come **DeepJazz** da cui potrei trarre ispirazione.

<div style="page-break-after: always;"></div>

## Un nuovo tipo di IA: ARS

### Introduzione

Il 20 Marzo 2018 è stato inventato un nuovo algoritmo, chiamato *Augmented Random Search*, si tratta di un algoritmo del tipo *reinforcement learning* dell'insieme *Shallow Learning*. Si tratta di una rielaborazione del *Simple Random Search*.

Brevemente, *ARS* è differente dagli altri algoritmi trattati nella tesina, infatti sono stati trattati algoritmi di *Deep Learning*, perché quelli hanno un *Hidden Layer* che fa calcoli ed elaborazioni complesse; questo non avviene con *ARS* perché non ha nessun *Hidden Layer* e di conseguenza tutti i calcoli sono effettuati sul *weight* che collega i nodi; quando questo avviene si parla di *Perceptron*.

Questo porta *ARS* ad essere un algoritmo molto piu efficiente e veloce rispetto a quelli di *Deep Learning* ma non è applicabile a tutti i campi.

In particolare i ricercatori hanno usato **MuJoCo** per testare il proprio algoritmo, si tratta di un ambiente virtuale di simulazione dove si incarica un'IA ad imparare a rimanere in equilibrio e camminare, a questo fine si regola un parametro, chiamato *reward*, più tempo mantiene l'equilibrio e più tempo cammina maggiore è il *reward*.

### Implementazione, risultati ed il futuro

Al fine di provare i risultati sorprendenti, ho implementato l'algoritmo in Python, usando però come ambiente fisico **PyBullet** e non **MuJoCo** in quanto quest'ultimo non è gratuito, in particolare ho sfruttato:

* **NumPy** per il calcolo matriciale
* **ffmpeg** per elaborare il video in output
* **OpenAI Gym** per l'ambiente virtuale
* **PyBullet** per la simulazione fisica

I risultati sono davvero interessanti e strabilianti considerato il tempo di *training* che gli ho dato (all'incirca 10 ore). La mia implementazione sarà disponibile sul mio account GitLab(https://gitlab.com/users/omniait/projects) con documentazione utente e del codice.

In futuro prevedo di implementare questo algoritmo in un altro linguaggio, come *Rust* o *C++*, e successivamente con supporto al *GPU Computing* tramite la libreria *Vulkan* o *OpenCL*, al fine di migliorare le prestazioni perché attualmente il programma non sfrutta al massimo la potenza del processore del computer.

Nella presentazione, che fa da supporto a questa tesina, sono illustrati i risultati.

## Fonti

Si elencano di seguito tutte le fonti, libri, corsi, notizie e conferenze, raggruppate in categorie, che hanno consentito il compiersi di questa tesina.

### Teoria sulle reti neurali, IA moderne e RNN

Libri e conferenze che mi hanno aiutato a comprendere l'intuizione e l'idea dietro le Reti Neurali e l'IA moderna.

**Titolo e Autori:** Deep Learning di *Ian Goodfellow, Yoshua Bengio, Aaron Courville*

**Editore:** MIT Press (18 novembre 2016)

**Lingua:** Inglese

**ISBN-13:** 978-0262035613

**Titolo e Autori:** Fundamentals of Deep Learning - Designing Next-Generation Artificial Intelligence Algorithms di *Nikhil Buduma*

**Editore:** O'Reilly Media (7 luglio 2017)

**Lingua:** Inglese

**ISBN-13:** 978-1491925614

**Titolo e Autori:** Deep Learning with Python di *Francois Chollet*

**Editore:** Manning Pubblishing Company (28 ottobre 2017)

**Lingua:** Inglese

**ISBN-13:** 978-1617294433

Utile è stata la conferenza Devox (8 Novembre 2016): [Tensorflow and deep learning - without a PhD](https://www.youtube.com/watch?v=vq2nnJ4g6N0) di *Martin Görner*.

### Augmented Random Search

[Simple random search provides a competitive approach to reinforcement learning](https://arxiv.org/pdf/1803.07055.pdf)

Ricerca di *Horia Mania, Aurelia Guy & Benjamin Recht* (University of California, Berkeley - Department of Electrical Engineering and Computer Science)

### Sviluppo applicativo

Libri e corsi che mi hanno aiutato a compiere dal lato pratico i programmi che sfruttano *Deep Learning* con vari fini (analisi dei titoli in borsa e simulazione di guida autonoma per esempio).

**Titolo e Autori**: Deep Learning with Keras - Implementing deep learning models and neural networks with the power of Python di *Antonio Gulli, Sujit Pal*

**Editore:** Packt Publishing (26 aprile 2017)

**Lingua:** Inglese

**ISBN-13:** 978-1787128422

**Titolo e Autori**: Python Deep Learning di *Gianmario Spacagna, Daniel Slater, Valentino Zocca, Peter Roelants*

**Editore:** Packt Publishing (28 aprile 2017)

**Lingua:** Inglese

**ISBN-13:** 978-1786464453

**Titolo e Autori:** Hands-On Machine Learning With Scikit-Learn and Tensorflow: Concepts, Tools, and Techniques to Build Intelligent Systems di *Aurelien Geron*

**Editore:** O'Reilly Media (31 gennaio 2017)

**Lingua:** Inglese

**ISBN-13:** 978-1491962299

**Titolo e Autori:** Machine Learning With Python Cookbook: Practical Solutions from Preprocessing to Deep Learning di *Chris Albon*

**Editore:** O'Reilly Media (31 marzo 2018)

**Lingua:** Inglese

**ISBN-13:** 978-1491989388

Assieme ai corsi online: 

* Stanford University
  * CS231n  Convolutional Neural Networks for Visual Recognition 
  * CS224d Deep Learning for Natural Language Processing 
* Udacity 
  * Deep Learning 
  * Convolutional Neural Networks for Visual Recognition

Che hanno contribuito a creare altri applicativi che sfruttano IA aiutandomi nell'apprendimento.

### News

#### Dataset Errati

[When Discrimination Is Baked Into Algorithms](https://www.theatlantic.com/business/archive/2015/09/discrimination-algorithms-disparate-impact/403969/)

Articolo di *Lauren Kirchner* - **The Atlantic** del 06 - 09 - 2015.

[Machine Bias, There’s software used across the country to predict future criminals. And it’s biased against blacks.](https://www.propublica.org/article/machine-bias-risk-assessments-in-criminal-sentencing)

Ricerca di: *Julia Angwin, Jeff Larson, Surya Mattu e Lauren Kirchner* - **Propublica** del 23 - 05 - 2016.

[How to Upgrade Judges with Machine Learning](https://www.technologyreview.com/s/603763/how-to-upgrade-judges-with-machine-learning/)

Articolo di *Tom Simonite* - **Technology Review** del 06 - 03 - 2017 .

####Fake News

[No, Emma Gonzalez did not tear up a photo of the Constitution](https://edition.cnn.com/2018/03/26/us/emma-gonzalez-photo-doctored-trnd/index.html)

Articolo di *Gianluca Mezzofiore -* **CNN** del 26 - 03 - 2018.

[Fake videos are on the rise](http://www.latimes.com/business/technology/la-fi-tn-fake-videos-20180219-story.html)

Articolo di *David Pierson* - **LA Times** del 19 - 02 - 2018.

[Cambridge Analytica Execs Caught Discussing Extortion and Fake News](https://www.wired.com/story/cambridge-analytica-execs-caught-discussing-extortion-and-fake-news/)

Articolo di *Issie Lapowsky* - **Wired** del 19 - 03 - 2018.

[Ricerca e realizzazione di Face2Face](http://niessnerlab.org/projects/thies2016face.html)

Ricerca di *Justus Thies e Matthias Nießner* (Technical University of Munich), *Michael Zollhöfer* (Stanford University), *Marc Stamminger* (University of Erlangen-Nuremberg), *Christian Theobalt* (Max Planck Institute for Informatics)

#### Armi Autonome

["Slaughterbot" Autonomous Killer Drones | Technology](https://www.youtube.com/watch?v=ecClODh4zYk)

**Conferenza** di *Stuart Russell*, professore dell'Università di Berkley. 17 - 09 - 2017.

[Autonomous Weapons: an Open Letter from AI & Robotics Researchers](https://futureoflife.org/open-letter-autonomous-weapons/)

La lettera firmata da *Stuart Russell* assieme ad altri ricercatori dal 28 - 07 - 2015.

#### Privacy e Sicurezza

[Chinese Police Add Facial-Recognition Glasses to Surveillance Arsenal](https://www.wsj.com/articles/chinese-police-go-robocop-with-facial-recognition-glasses-1518004353?mod=e2tw)

Articolo di Josh Chin - **Wall Street Journal** del 07 - 02 - 2018.

[Welcome To The Surveillance State: China’s AI Cameras See All](https://www.huffingtonpost.com/entry/china-surveillance-camera-big-brother_us_5a2ff4dfe4b01598ac484acc)

Articolo di Ryan Grenoble - **Huffington Post** del 12 - 12 - 2017.

Questo documento è stato realizzato in Markdown ed impaginato in LaTeX.

