# Inetto

L'inetto è colui che è inadatto alla vita, ciò si vede nel "Una vita", 
la quale opera in realtà in principio doveva chiamarsi "Un inetto" a simboleggiare proprio il significato di questa parola.
Alfonso Nitti - Una Vita
Zeno Cosini - Coscienza di Zeno (Zeno viene da Xeno = Diverso)

# Coscienza di Zeno

La trama non segue un ordine cronologico in quanto il romanzo in realtà si apre con una dichiarazione dello psichiatra, chiamato Dottor S.; 
il libro tratta della vita di un malato mentale e pertanto tutto ciò che viene scritto è "inaffidabile".

La coscienza di zeno è quindi un libro autobiografico del paziente. Il tema principale riguarda la malattia e comunque la psicanalisi.

# Ultima Sigaretta

A vent’anni Zeno si accorge di odiare il fumo e si ammala, ma nonostante la malattia decide di fumare un’ultima sigaretta; 
ed è qui che si evidenzia per la prima volta la vera malattia psicoanalitica del protagonista. 
Inizialmente il fumo è per Zeno una reazione al rapporto con il padre 
poi si allarga a forma di difesa verso la realtà circostante e il mondo intero.

# Finale Coscienza di Zeno

Il finale del romanzo è un finale apocalittico, Zeno sostiene che attraverso gli ordigni 
esplosivi si potrà cancellare tutto quello che c'è permettendo alla Terra di tornare alla polvere e di rinascere.

# Poetica Umorista e saggio del 1908

La poetica dell'umorismo sostiene che l'umorismo porta ad una riflessione profonda, ci sono varie fasi, 
la prima è vedere lo strano (come una donna anziana vestita da giovane), ridere di questo (quindi reazione comica), 
ma in seguito pensare, ragionare, e raggiungere una conclusione profonda.

Il saggio usa per l'esempio della donna anziana, dove la riflessione porta a pensare che forse quella signora sia in realtà preoccupata di apparire ancora giovane, 
non vuole invecchiare forse.

# Strappo nel Cielo di Carta e Lanterninosofia

Lo strappo nel cielo di carta ne Il Fu Mattia Pascal è un momento del romanzo dove c'è un esempio riferito al teatro, 
praticamente se la sceneggiatura si strappasse (quindi lo strappo del finto cielo) 
gli attori del teatro rimarrebbero instraniti iniziando a ragionare capendo che la realtà è molto più ampia rispettto al loro teatro.
Capire che come è finzione il teatrino delle marionette anche la vita è una finzione.

La premessa riguarda la natura umana, gli umani sono coscienti di vivere.
La Lanterninosofia è una spiegazione di come le varie epoche e correnti di pensieri si alternino.
Praticamente ognuno di noi ha una "laternina" che usiamo per cercare una verità nel buio eterno, 
però ciò che ci consente di vedere questa laternina è sempre un piccolo pezzo rispetto alla realtà dell'universo.

Ogni tanto ci sono dei momenti nel tempo in cui vi sono dei lanternoni grandi, 
che illuminano più degli altri e quindi le varie persone iniziano a raggiungerla, 
questi lanternoni sono le correnti di pensiero come l'illuminismo o il positivismo.

# Meta teatro o teatro nel teatro

Il Meta-teatro è una situazione in cui gli attori rappresentano dei personaggi che però sono attori a loro volta, questo significa che il "quarto muro" viene rotto, 
i personaggi passano continuamente di ruolo tra essere persone vere e proprie ad essere attori, indossando "maschere" diverse.

# Relativismo Conoscitivo

Il relativismo conoscitivo indica il fatto che non esiste una realtà, scientifica, unica, universale, ma ne esistono tante che ognuno guarda da punti diversi.

Ne "Così è, se vi pare" già il titolo indica un'incertezza della realtà (Se vi pare), per indicare ai lettori proprio questo pensiero.

Tutta l'opera teatrale gira intorno a questa irrangiungibilità della verità unica, 
infatti nessuno dei paesani sanno chi siano davvero i personaggi Frola e Ponza cosa che incuriosisce, 
ma neanche alla fine si raggiunge ad una conclusione, lasciando quindi La verità nascosta.

Forme e Maschere
Cercare di rinunciare alle forme
