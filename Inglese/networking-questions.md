What is a network?

A network is two or more devices that are linked together, they are able to share resources both hardware and software.

What is a LAN, a WAN, a MAN?

A LAN is a small network located in a building that doesn't go through public space, when this network goes through public space it becomes a MAN.

When many MANs are connected together they form up a WAN that is big as a continent or the whole globe.

What kind of network configurations do you know?

Mainly we have 2 categories: Client-Server and Peer-To-Peer (P2P).

In a Client-Server architecture we have a main Server that has all the resources shared in the network, every device connected to the server can access its resources.

In a P2P Architecture we have a lot of devices, called peers, that are interconnected among them, this means that the network can slow down easily when some peers request resources from other peers.

What are the main protocols used in networks?

The most used protocol is TCP/IP which has some subset rules, for example TCP and UDP.

What are the advantages and disadvantages of networks?

The advantages