1922 - Formazione dell'URSS (Unione Repubbliche Socialiste Sovietiche).

1924 - Inizia la lotta per il potere tra Trotsky e Stalin, muore Lenin.

1929 - Esilio di Trotsky

1940 - Assassinio di Trotsky

1928 - Inizio del Piano Quinquennale: industrializzazione e pianificazione economica.

Sovchoz e Kolchoz - Imprese di stato agricole.

Kulaki - Piccoli/Medi propetari che si sono arricchiti durante la NEP.

1930 - Iniziano le repressioni.

Stachanov - Minatore che estrarse più di 100 tonnellate di carbone in un giorno, diventò modello della propaganda sovietica.

1936 - Entra in atto la costituzione di Stalin, si pone una struttura federalista.

Inizia nello stesso anno la Grande Purga.