# Algoritmi

Problema: Sommare i primi 10 numeri naturali
$$
S = 1+2+3+4+5+6+7+8+9+10
$$
Si possono usare i cicli.

```python
S = 0
for i in range(1,10):
    S += i
```

Si può usare la legge delle successioni
$$
S = \frac{1}{2} n(a_1+a_n) = \frac{1}{2}10(1+10)
$$
O si può usare la somma di Gauss
$$
S = \frac{n(n+1)}{2} = \frac{10(10+1)}{2}
$$
Ogni algoritmo ha un costo di risorse e tempo.

Il problema sorge nel calcolo del tempo, non si può usare il tempo "solare", quindi in secondi o millisecondi.

Ma va usato il "Tick", ovvero ogni singola operazione.

```python
# N := numero di elementi del vettore
for i in range (1,N):
    S += array[i]
```

Di conseguenza il costo computazionale di questo algoritmo è:
$$
3 * N = O(N)
$$
Però in altri algoritmi si presentano i Worst Case, Mean Case e Best Case scenario, che rappresentano i vari casi.

## Complessità

Supposto di avere due algoritmi che risolvono lo stesso problema:
$$
f(N) = 3 N \\
h(N) = 2N \\

C = \lim_{N\rightarrow\inf} = \frac{f(N)}{h(N)}
$$
Questi algoritmi si possono confrontare in quanto sono entrambi nella stessa classe. Ma quando non sono nella stessa classe.

Quando l'ordine o la classe sono totalmente diversi il limite C assume valore o 0 o infinito rendendo il confronto tra i due algoritmi complesso.