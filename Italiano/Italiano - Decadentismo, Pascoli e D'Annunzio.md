# Italiano - Decadentismo, Pascoli e D'Annunzio

[TOC]

## Decadentismo

Il Decadentismo nasce e si evolve principalmente in Francia; nel 1883 su "Le Chat Noir" Paul Verlaine pubblica il sonetto Languer, in cui afferma che si identificava nell'atmosfera alla decadenza dell'Impero Romano, incapace di passioni e di azioni, immerso nel vuoto e nella noia.

Questo sonetto rispecchiava uno stato d'animo diffuso, il senso di disfacimento e di fina di tutta una civiltà.

Ma in ogni caso queste idee appartenevano solo a circoli d'avanguardia che si contrapponevano alla borghesia ispirandosi al modello di Baudelaire (morto nel 1867, opera importante: le fleur du mal 1857).

Nel 1883 Verlaine presenta le personalità di spicco di questo circolo, i "Poeti Maledetti" (poetes maudits) che sono: Corbiere, Rimbaud e Mallarmé.

Huysmans scrive nel 1884 A Rebours, romanzo che fissò un codice con motivi e atteggiamenti del movimento decadente.

Il Decadentismo è quindi principalmente un movimento letterario con concetti poi ripresi in altre correnti come il Simbolismo e l'Estetismo.

## Decadentismo e Positivismo

Se da una parte hanno una continuità, ovvero la ricerca di una verità dietro il mistero della realtà visibile, i due movimenti usano strumenti opposti:

| Positivismo                              | Decadentismo                             |
| ---------------------------------------- | ---------------------------------------- |
| Esiste una realtà oggettiva, scopribile solo con la scienza e la ragione. | Esistono tante realtà, si possono esplorare solo attraverso i collegamenti analogici nascosti alla ragione. |

Ne consegue che la conoscenza decadentista si può raggiungere solo tramite strumenti irrazionali (quindi follia, allucinazioni), questo spiega anche il motivo per cui i decadenti facevano uso di sostanze come alcol e droghe.

## Estetismo

Un altro strumento per raggiungere la conoscenza è l'arte, questo cambia il valore degli artisti da "esecutori di un'arte" a veri e propri cultisti, sacerdoti, profeti; questo ha dato la nascita alla corrente dell'Estetismo.

L'esteta è colui che ha come unico principio la bellezza, la ricerca di essa e il mantenimento, valori morali, bene e male, giusto e sbagliato, vengono tutti sostituiti dal bello.

Tutta la realtà e la vita stessa diventano "Arte", l'esteta deve fare della propria vita un'arte e il fine dell'arte è l'arte stessa.

Oscar Wilde : "Ho messo l'arte nella mia vita, nelle opere solo il talento".

Questi principi vengono teorizzati in Regno Unito e in Francia e che verranno sviluppati ulteriormente da Oscar Wilde e D'Annunzio anche attraverso le loro stesse vite.

## Simbolismo

Per la poesia bisogna però cambiare totalmente il linguaggio al fine di trovare, appunto, nuovi strumenti per raggiungere la conoscenza.

Ne consegue che il linguaggio poetico tende a "pesare" ogni parola, ogni parola non è più uno strumento comunicativo ma uno strumento di ricerca dell'ignoto e dell'irrazionale.

Si dà conto alla musicalità del verso (Pascoli - Fonosimbolista), dato che la musica è considerata l'arte suprema, gli schemi sintattici e metrici iniziano a decadere (D'Annunzio - Verso Libero).

Il linguaggio diventa analogico, si usano sinestesie e metafore che sono simboli di qualcosa di mistico e remoto.

Il linguaggio poetico però inizia così a diventare comprensbile solo da un'aristocrazia, eliminando la borghesia, considerata volgare e si oppone alla cultura di massa che offre al pubblico "prodotti in serie".

Questo crea un conflitto aperto fra artista e la società.

Nota: Si considera il Decadentismo come una seconda fase del romanticismo tedesco e inglese in quanto molto simili i preconcetti.

## Decadentismo e Naturalismo

I due movimenti sono fenomi parallaleli e compresenti espressivi di gruppi intellettuali diversi.

| Decadentismo                             | Naturalismo                              |
| ---------------------------------------- | ---------------------------------------- |
| Autori che rifiutano le contraddizioni del sistema, rifiutano l'ordine esistente | Autori integrati nella borghesia, accettanno le regole positiviste. |

## D'Annunzio

Nasce nel 1863 da una famiglia borghese, si trasferisce a Roma e inizia una carriera da giornalista.

Acquisì notorietà grazie agli scandali riguardanti sia gli scritti sia la sua vita, spesa tra avventure, lusso e duelli; la fama gli consentì d'esser pagato ma i suoi continui esagerati sprechi richiedevano somme sempre più cospicue.

Fondata la propria vita sulla sua immagine contraddittoria di esteta è costretto però a fuggire in Francia a causa dei debiti, ci tornerà nel 1915, inizio della guerra.

Le opere più importanti sono:

* Il Piacere parla di un esteta di debole volontà che seguendo i principi dell'estetismo però si mette in dubbio e crolla.
* Le Vergini delle Rocce, dove si parla di un eroe forte, sicuro, che esprime a fondo l'ideologia d'Annunziana reazionaria, imperialista e aristocratica. Assieme a "Il fuoco" si riesce ad ottenere l'idea del superuomo di d'Annunzio
* Le Laudi, è importante in quanto è stato introdotto il verso libero in italiano. Nel primo libro si parla di un viaggio simile a quello d'Ulisse ma compiuto in epoca moderna, ci si immerge in un passato mitico e in un presente industriale che è impregnato di potenzialità.
* Alcyone, terzo libro delle Laudi, tema fondamentale è il panismo (pioggia nel pineto).

## Pascoli

Pascoli nasce nel 1855, da una famiglia borghese rurale, il padre viene assassinato quanto ha 12 anni (X Agosto); la morte del padre segna l'inizio di una serie di lutti in famiglia e l'inizio dei problemi economici.

Nel 1868 muoiono la madre e la sorella maggiore, negli anni successi anche due fratelli.

Frequentò l'università di Bologna grazie ad una borsa di studio, subì il fascino dell'ideologia socialista (rivoluzionaria) e partecipò a delle manifestazioni na dopo l'arresto del '79 si distacca dalla politica militante pur rimanendo attaccato all'ideologia socialista.

La vita travagliata dai lutti ebbe un forte impatto sulla poetica di Pascoli, si trovano infatti spesso i temi della morte (X Agosto, Ultimo Sogno, Novembre) e della solitudine (Lavandare).

Pascoli si colloca nel simbolismo innovandolo però con un'attenzione importante verso il suono delle parole e proprio per questo con Pascoli si parla di "Fono-Simbolismo" (Il temporale).

Pascoli esprime l'obiettivo e il contenuto della propria poetica nel saggio Il Fanciullino, pubblicato nel '97.

Tornando ad uno stato infantile si riescono a dimenticare le proprie esperienze e a trovare un nuovo mezzo di espressione che è carico di stupore e curiosità, se la natura è una foresta di simboli il fanciullino nella natura si riesce a stupire e incuriosire continuamente scoprendo analogie sempre più complesse; il fanciullino rimpicciolisce e ingrandisce per capire meglio.

Questo significa che la poesia stessa diventa uno strumento di conoscenza ma alogica, come si sostiene nel Decadentismo.

L'ideologia si riflette anche in parte sulla semantica di Pascoli che accosta parole appartenenti a registri diversi e campi diversi, importante è ad esempio l'insieme delle parole botaniche che sono precise e tecniche abbattendo così le "classi dei termini".

Pascoli è a favore dell'intervento militare in Libia, poteva favorire l'Italia dando delle terre e lavoro a chi non ne aveva: "La grande Proletaria s'è mossa" Ovvero l'Italia aveva iniziato ad avere mire Imperialiste.

A livello di sintassi Pascoli usa la coordinazione, questo anche perché la stessa Myricae (tratto dalla IV Bucolica di Virgilio), vuole essere una poesia "umile" come le tamerici. I componimenti di Pascoli sono infatti brevi e ritraggono la vita rurale con un gusto impressionistico. Ma la lettura più approfondita mostra significati profondi.

Le opere di Pascoli principali sono:

* Myricae (poesia, 1900)
* Il Fanciullino (saggio)
* Poemetti (stile più vicino ai romanzi, ne fa parte Italy)
* I Canti di Castelvecchio
* Carmina e altre opere in latino

