# Lezioni Costituzione Italiana

* Lunedi 	12/02/2018
* Venerdi  16/02/2018
* Lunedi    19/02/2018
* Venerdi   23/02/2018


# Dante Alighieri

## Canto VI

Tutti i canti VI sono canti Politici.

| **INFERNO**    | **CIACCO**      | **FIRENZE**                 |
| -------------- | --------------- | :-------------------------- |
| **PURGATORIO** | **SORDELLO**    | **ITALIA**                  |
| **PARADISO**   | **GIUSTINIANO** | **IMPERO ROMANO D'ORIENTE** |

### Paradiso

Dopo che Costantino ebbe condotto l'aquila in direzione opposta rispetto al corso del Sole (Ha spostato la sede dell'Impero Romano a Costantinopoli, da est a ovest) che invece Enea fece al contrario (da Troia a Roma).

*Aquila imperiale* - Lo stendardo, il simbolo dell'impero.

L'uccello Divino - L'aquila - Rimasta per duecent'anni.

Costantino concedette la libertà di culto ai cristiani.

Versi da imparare: 1 - 27; 97 - 111.

Elimina dalle leggi il "troppo" e il "vano": Mette a lavorare dei giuristi che lavorano al "Corpus Iuris Civilis"; conserva le leggi che fan parte della legge civile romana; lavorando selezionano le leggi più importanti.

Ci si riferisce all'Impero Romano d'Oriente anche come Impero Bizantino.

Monofisismo - "Una sola natura" - Eresia perché i cristiani hanno nella propria dottrina di Trinità.

Agapito I - Papa riuscì a convincere Giustiniano alla Trinità.

Non appena rientrai in seno alla Chiesa, Dio volle per sua grazia ispirarmi l'alta opera (il Corpus iuris civilis) e io mi dedicai anima e corpo ad esso; e affidai le armi al mio generale Belisario, che fu assistito dal cielo a tal punto che ciò fu segno che io dovessi fermarmi.

Nei versi seguenti si ripercorre tutta la storia romana fino al Sacro Romano Impero.

#### Versi 97 - 111

Ormai puoi giudicare la condotta di quelli che ho accusato prima e le loro colpe (Ghibellini), che sono causa di tutti i vostri mali.


Gli uni (i Guelfi) oppongono al simbolo imperiale i gigli gialli della casa di Francia, e gli altri (i Ghibellini) se ne appropriano per la loro parte politica, così che è arduo stabilire chi sbagli di più.

I Ghibellini facciano la loro politica sotto un altro simbolo, giacché chi lo separa sempre dalla giustizia ne fa un cattivo uso; e non creda di abbatterlo coi suoi Guelfi Carlo II d'Angiò, ma abbia timore dei suoi artigli che scuoiarono leoni più feroci di lui.


Molte volte i figli hanno già pagato per le colpe dei padri, e quindi non creda Carlo che Dio cambi il proprio simbolo con i suoi gigli!