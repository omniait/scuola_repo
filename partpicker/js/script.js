// My rounding function
function myRound(value, decimals) 
{
    return Number(Math.round(value + 'e' + decimals) + 'e-' + decimals);
}

// Initialise a variable to take the sum
var sum = 0.0;

// Select all the table rows but not the head
var rows = $("tbody tr"); // should be equivalent to var rows = document.getElementsByTagName("tbody tr");
// From object to array
rows = rows.toArray();
// Drop last row
rows.pop();

// Get the refresh button
var btnRefresh = $("#btnRefresh");

// Onclick recalculate total
btnRefresh.click(function(){
    sum = 0.0;
    // Iterate over all rows
    rows.forEach(row => {
        //console.log(row);
        // Get columns
        var cols = jQuery(row).children();
        // Convert columns to array
        cols = cols.toArray();
        //console.log(cols);
        // Checkboxes
        //console.log(cols[0]);
        //console.log(cols[0].children);
        //console.log(cols[0].children[0]);
        //console.log(cols[0].children[0].value);
        // Prices
        //console.log(cols[4]);
        //console.log(cols[4].children);
        //console.log(cols[4].firstChild);
        //console.log(cols[4].firstChild.data);
        if (jQuery(cols[0].children[0]).is(":checked"))
        {
            // Parse from "xx.xx" to a floating point number
            var val = parseFloat(cols[4].firstChild.data);
            // Round it (there can be problems on some browsers)
            val = myRound(val, 2);
            // Sum it up
            sum += val;
            //console.log(sum);
        }
    });
    // Round again
    sum = myRound(sum, 2);
    $("#total").html(sum);
});