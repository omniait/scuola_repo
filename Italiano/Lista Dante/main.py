def write_base_files():
    f = open('files/studenti.csv', 'w')
    f.write('Nome e Cognome, Data di Nascita, Scuola, Classe, EMail, \n')
    f = open('files/docenti.csv', 'w')
    f.write('Nome e Cognome, EMail \n')

def write_student(name_surname, birth_date, school, sclass, mail):
    f = open('files/studenti.csv', 'a')

    i = 0
    while i < len(name_surname):
        f.write(name_surname[i] + ',' + birth_date[i] +
                ',' + school[i] + ',' + sclass[i] + ',' + mail[i] + '\n')
        i += 1
    

def students_list():
    f = open('files/studenti.txt', 'r')
    name_surname = []
    birth_date = []
    school = []
    sclass = []
    mail = []

    name_places = list(range(0, 435, 7)) # 0, 7, 14, ...
    birth_date_places = list(range(1, 436, 7))  # 1, 8, 15, ...
    school_places = list(range(2, 437, 7))  # 2, 9, 16, ...
    sclass_places = list(range(3, 438, 7))  # 3, 10, 17, ...
    mail_places = list(range(4, 439, 7))  # 4, 11, 18, ...

    line_i = 0

    # if isinstance(e, list):
    for line in f:
        if line_i in name_places:
            name_surname.append(line[:-1])

        if line_i in birth_date_places:
            birth_date.append(line[:-1])

        if line_i in school_places:
            school.append(line[:-1])
            
        if line_i in sclass_places:
            sclass.append(line[:-1])

        if line_i in mail_places:
            mail.append(line[:-1])

        #print(line_i)   
        line_i += 1
                  
    write_student(name_surname, birth_date, school, sclass, mail)


def write_teacher(name_surname, mail):
    f = open('files/docenti.csv', 'a')

    i = 0
    while i < len(name_surname):
        f.write(name_surname[i] + ',' + mail[i] + '\n')
        i += 1

def teachers_list():
    f = open('files/docenti.txt', 'r')

    name_surname = []
    mail = []

    name_places = list(range(0, 49, 4))  # 0, 4, 8, ...
    mail_places = list(range(1, 50, 4))  # 1, 5, 9, ...

    line_i = 0
    # if isinstance(e, list):
    for line in f:
        if line_i in name_places:
            name_surname.append(line[:-1])

        if line_i in mail_places:
            mail.append(line[:-1])
        line_i += 1

    write_teacher(name_surname, mail)

def main():
    write_base_files()
    students_list()
    teachers_list()

if __name__ == '__main__':
    main()
