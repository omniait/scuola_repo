# OOP PHP

Definiamo un carrello:

Abbiamo:

| Nome                | Descrizione                                      |
| ------------------- | ------------------------------------------------ |
| $prodotto           | array di prodotti                                |
| $quantita           | array di quantità dei prodotti                   |
| **Metodi**          | **Descrizione**                                  |
| aggiungi_carrello() | metodo che aggiunge un prodotto al carrello      |
| togli_carrello()    | metodo che toglie un prodotto dal carrello       |
| aggiorna()          | metodo che aggiorna la quantità di un prodotto   |
| stampa()            | metodo che fa la lista dei prodotti del carrello |

