# Luigi Pirandello

Nasce a Girgenti (Agrigento attuale), 1867, da una famiglia borghese, vede l'ascesa del fascismo iscrivendosi al partito e del nazismo.

Studia a Roma e in seguito Bonn (Germania) dove si laurea.

Pirandello è conosciuto principalmente come drammaturgo; è stato il primo ad integrare il senso della frammentazione dell'Io in Uno, nessuno e centomila.

Questa è una grande differenza dal romanzo dell'800 dove si studiava scientificamente la realtà.

Nel 1934 diventa premio nobel per la letteratura.

Muore nel 1936.

| Romanzi                  | Novelle             | Teatro (Maschere Nude)           |
| ------------------------ | :------------------ | -------------------------------- |
| L'esclusa                | Novelle per un anno | Sei personaggi in cerca d'Autore |
| Il fu Mattia Pascal      |                     |                                  |
| Uno, nessuno e centomila |                     |                                  |

## Poetica

### Umorismo

1908 - Saggio su l'umorismo.

Poetica - Concezione di un autore che ha nella scrittura, concezione filosofica, ideologica, morale della propria arte.

Umorismo - Avvertimento del contrario, la signora è **il contrario** di quello che dovrebbe essere.

Dall'umorismo nasce la comicità ma nasce da essa la riflessione ed il sentimento del contrario, che interrompe la comicità.

```mermaid
graph TD	
A[Avvertimento del Contrario] -->B[Comicità]
B --> C[Riflessione]
C --> D[Sentimento del contrario]
D --> E[Umorismo]
```

#### Confronto sintetico con altri artisti

Poetica di Manzoni:

* Vero - come oggetto
* Utile - come scopo
* Interessante - come mezzo

Poetica di Pascoli:

* Il fanciullino
* Il poeta è colui che sa riconnettersi con la parte più autentica e infantile di sé

La differenza forte con Svevo è che Svevo si occupa proprio di inetti, di una vera e propria malattia.

### Relatismo Conoscitivo

Non esiste un'unica verità ma esistono tante verità a seconda del punto di vista con cui si osserva la realtà.

Questo filosoficamente si collega alla nuova scoperta scientifica, il forte esempio è la fisica quantistica.

## Il Treno ha Fischiato (Novelle per un anno)

Dalla raccolta "La Trappola" del 1915 pubblicata sul "Corriere della Sera".

Belluca : Computista, non si ribella mai all'esser maltrattato dai colleghi, ha da mantenere 12 persone, 3 cieche e le figlie vedove coi nipoti, fa lavoro in più per mantenere la famiglia.

Un giorno arriva in ritardo, ilare e non fa niente al lavoro, inizia a ripetere "Il treno ha fischiato", che l'ha portato in posti lontani, dopo esser stato ripreso dal capo ufficio.

Il narratore sa qualcosa di più ed è presente nella storia, è il vicino di casa del protagonista, il quale spiega la situazione del protagonista.

Belluca una notte che non riesce a dormire immagina grazie al fischio di un treno e riesce a viaggiare con la mente.

## Ciàula Scopre la Luna (Novelle per un anno)

### Ambiente

Si tratta di una cava di zolfo con una raffineria che dà zolfo puro.

### Personaggi

* Cacciagallina: Sorvegliante dei minatori
* Zi'Scarda: Un vecchio, mezzo cieco (da un occhio)
* Ciàula: Manovale/Garzone di Zi'Scarda

#### Gerarchia

```mermaid
graph LR	

A[Cacciagallina] --> B[Zi'Scarda]
B --> C[Ciàula]

```

### Riassunto

Una sera, in una miniera siciliana, il sorvegliante Cacciagallina minaccia con un revolver i lavoratori e ordina loro di lavorare tutta la notte per finire il carico della giornata.

Il sorvegliante aggredisce Zi' Scarda che è l'unico a rimanere, un vecchio cieco da un occhio, percuotendolo, questi non risponde sapendo che il sorvegliante doveva in qualche modo sfogarsi, e che a sua volta aveva comunque qualcuno sotto di lui con cui sfogarsi: Ciàula, il suo manovale abituato ormai al buio della cava e che si comporta istintivamente, come un animale.

Zi'Scarda dice al sorvegliante che sarebbe stato lui a riempire il "calcherone", ovvero la fornace.

Zi'Scarda è costretto a lavorare allo sfinimento in quanto nella sua famiglia è rimasto l'unico uomo oltre a Ciàula di casa (come Belluca) dopo la morte del figlio Calicchio che camminò su una mina, all'esplosione assistirono Zi'Scarda, che ha perso la vista da un occhio, e Ciàula, che rimase segnato dall'evento perché da quel momento iniziò ad aver paura del buio della notte.

Zi'Scarda carica le spalle di Ciàula al limite, tanto che riesce a malapena a muoversi, inizia a camminare verso l'uscita terrorizzato ma si ferma ad un certo punto sfinito, lasciando il carico e sedendosi sopra.

Ciàula scopre la Luna, la vede che illumina tutta la valle e la paura e il terrore svaniscono.

### Confronto col Verismo

| Rosso Malpelo (Verga 1878)                                   | Ciàula Scopre la Luna (Pirandello 1912)                      |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| Cava di sabbia                                               | Cava di zolfo                                                |
| Reietto                                                      | Inetto                                                       |
| Eclisse dell'autore                                          | L'autore è presente                                          |
| Registro Linguistico basso <br />(regressione del personaggio) | Registro Linguistico elevato                                 |
| Rosso Malpelo è "sano di mente", <br />Analizza la verità scientificamente | Ciàula è un minorato mentale/ebete, <br />Vive come un animale |

Ciàula viene confortato dalla luce della Luna così come Belluca si conforta con l'immaginazione.

L'apparizione della Luna diventa come un'apparizione mitica.

Le novelle di Verdi (Novelle rusticane, ) sono il riferimento per Pirandello.

## Romanzi

### Marta Ajala

Ha ancora elementi naturalisti pur iniziando ad allontanarsi. 

### Fu Mattia Pascal

Non ha ancora scritto il saggio sull'umorismo pur avendo comunque all'interno il concetto dell'umorismo.

È la storia di un piccolo Borghese, Mattia Pascal, che è intrappolato.

Mette in luce la **Crisi Dell'Identità**, l'io ha più sfaccettature e non più intero.

#### Tema del Relativismo

La personalità è una costruzione fittizzia, al sotto della quale non c'è nulla, la stessa realtà è una costruzione fittizzia.

#### Tema dell'Inettitudine 

Mattia Pascal è un inetto, incapace di vivere e sconfitto dalla vita; tenta in ogni modo di tenersi lontano dalle regole e dalle norme, questo cambia con l'innamoramento con Adriana.

#### Tema della Forma

È un involucro nel quale si concentrano le nostre sicurezze, come famiglia e lavoro.

Il caso fa cambiare forma a Mattia Pascal che diventa Adriano Meis.

#### Lo strappo nel cielo di carta

Avviene dopo la perdità di un'identità di Mattia Pascal.

Tutta la vita è come un teatro di burattini e basta uno squarcio nel "cielo di carta" (riferito di nuovo al teatro di burattini), un'intuizione che mette in crisi una certezza per distruggere ogni certezza.

A differenza degli alberi ci sentiamo vivere, la realtà è un buio grande e ognuno di noi ha delle lanterne che illumina il resto.

### Uno, Nessuno e Centomila

Si ricollega al Fu Mattia Pascal, l'uomo del novecento non si riconosce più in un'identità. Esprime perfettamente questa crisi.

Il protagonista è Vitangelo Moscarda.

## Teatro

Dal 1915 si interessa anche al teatro.

Capita che Pirandello tratta dalle novelle delle trasposizioni teatrali.

La Barriera del naturalismo, che rappresenta l'epoca moderna del '800, si spazza via entrando nel mondo contemporaneo, che porta all'impossibilità del conoscere.

### Così è (se vi pare)

Dramma teatrale composto nel 1917 tratto da La Signora Frola e Il Signor Ponza.

Non esiste un'unica verità ma esistono tante verità e quindi è già inserito nel **relativismo conoscitivo**.

### Sei Personaggi in cerca d'Autore

* Personaggi - Immutabili a differenza delle persone
* Abbattere la quarta parete, rappresentando "il teatro all'interno del teatro" chiamato anche meta-teatro

