# Proposta per la visita didattica a Milano

## Premesse

1. Data la natura di una visita didattica
2. Data la necessità di non sottovalutare le capacità interpersonali
3. Dato il fattore ludico dell'attività che proponiamo
4. Viste le applicazioni delle competenze interdisciplinari
5. Data la necessità di avere un ambiente sociale più favorevole e amichevole in classe
6. Data la vicinanza e la realizzabilità dell'attività che proponiamo
7. Dato il potenziale sviluppo di capacità logiche non tradizionali

Proponiamo ufficialmente un'attività chiamata "Escape Room".

## In cosa consiste

La "Escape Room" è un'attività ludica svolta in una stanza o una serie di stanze collegate che consiste nel risolvere enigmi e puzzle al fine di scappare (Escape) da questa.

Si ha un tempo limite ed è necessaria la collaborazione e la cooperazione al fine di aver successo.

## Perché è utile in ambiente didattico

Visti i punti sovraelencati si possono espandere come segue:

1. Una visita didattica deve anzitutto far imparare qualcosa, sviluppare skills personali e di gruppo, accrescere la propria cultura e mettere in pratica le conoscenze teoriche.
2. Le capacità interpersonali consentono di creare un ambiente di lavoro più amichevole migliorando la produttività, migliorarle consente quindi una migliore aspettita lavorativa sopratutto in ambito informatico dove comunque è vitale esprimersi e conversare con altri collaboratori del progetto.
3. Un'attività divertente può rilassare le tensioni, considerando comunque che la classe è in anno di maturità e che questo ha portato molto stress e a piccoli episodi di nervosismo è fondamentale riuscire a distendere le tensioni.
4. La natura di questa attività riguarda più discipline a causa della natura degli enigmi da risolvere, per esempio sono utili matematica e informatica per la logica e per il pensiero computazionale che può portare ad una facile risoluzione dei problemi.
   Un'altra cosa importante che questa attività può aiutare è lo sviluppo di una metodologia per affrontare problemi più o meno complessi con altri colleghi, cosa che si ripercuote sull'aspettativa di lavoro.
5. Per il punto 3 e considerando la presenza di alunni con problemi specifici si rende necessario un trattamento peculiare, è un'occasione per questi alunni di guadagnare fiducia nelle proprie capacità e aiutarli a superare la timidezza in quanto l'attività di gruppo può porre problemi che non si riescono a risolvere.
6. L'attività si può svolgere in diversi posti: Get Me Out in via Ripamonti 15 è vicino alla Università Bocconi ed alla stazione "Milano Porta Romana", "La Casa Maledetta" in via Angelo Brunetti 14 è molto vicina alla stazione "Milano Certosa".
   Vi sono altri luoghi dove poter svolgere questa attività.
   Il costo raggiunge un massimo di 35€.
7. Come già spiegato nel punto 4 a volte è necessario affrontare problemi che richiedono un approccio non tradizionale, questo può portare ad una risoluzione non immediata e non "visibile" a tutti.
   Questo vuol dire che sarà necessaria la cooperazione, la collaborazione e la discussione, cosa che ha ovviamente effetti positivi.

## Conclusione

Riteniamo quindi che la proposta di effettuare questa attività didattica possa dare un enorme beneficio a tutta la classe oltre a rendere la visita didattica unica e speciale rimanendo scolpita nei ricordi a lungo.

