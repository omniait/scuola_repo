'''
plot the SLR Model
'''
import matplotlib.pyplot as plt
import numpy as np

def slr(x_p):
    return 1.743 * x_p + 8.3995

if __name__ == '__main__':
    x = np.linspace(0, 15, 100, endpoint = True)
    y = slr(x)

    plt.plot(x,y)
    plt.grid(True)

    plt.show()
