# Giuseppe Ungaretti

Nasce nel 8 Febbraio 1888 ad Alessandria d'Egitto, muore nel 1970.

## Opere

* 1916 - Il porto sepolto (riferimento al porto sepolto di Alessandria d'Egitto)
* 1919 - L'Allegria dei naufragi (ossimoro)
* 1931 - L'Allegria
* 1933 - Sentimento del tempo
* 1947 - Il dolore

## Fiumi

Ne i Fiumi compaiono i seguenti fiumi:

* Isonzo, primo e ultimo
* Serchio
* Nilo
* Senna

# Salvatore Quasimodo

Nasce nel 1901, muore nel 1968. Nobel per la letteratura assieme a Montale, Pirandello, Carducci.

Poesia emblematica dell'ermetismo:

> Ognuno sta solo sul cuor della terra 
> trafitto da un raggio di Sole: 
> ed è subito sera

# Eugenio Montale

Nasce a Genova nel 1896, muore nel 1981. Firma nel 1925 il manifesto Croce (degli intellettuali antifascisti), nel 1938 è destituito dal Gabinetto Vieusseux.

## Opere

* 1925 - Ossi di seppia (aridità)
* 1939 - Le Occasioni (maggior uso del correlativo oggettivo teorizzato da Eliot)
* 1956 - La bufera e altro
* 1971 - Satura