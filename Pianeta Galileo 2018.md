# Pianeta Galileo 13/04/2018

## Primo incontro con la scienza

Professori:

* Fabrizio Luccio - "Algoritmi, divinità e gente comune", "Storia e matematica"
* Paolo Ferragi - Algoritmi e Crittografia

Pensiero Computazionale come abilità, grazie anche all'organizzazione.

### Motori di Ricerca

Sia Google (basato sulle keyword) che Shazam (basato su frammenti di audio) sono motori di ricerca.

Lo stesso vale anche per Tripadvisor, e il navigatore, che usano entrambi tecnologie di geolocalizzazione.

Ogni word ha una indicizzazione fatta dal crawler.

#### Rilevanza di un documento

La rilevanza non è definibile, vengono fornite degli esempi, per ogni pagina si prendono una certa quantità di caratteristiche. 

Frequenza parole, posizione del titolo, URL, etc.

Si sfruttano gli algoritmi di Unsupervised Machine Learning.

Knowledge Graph - Ogni "conoscenza" è un nodo, ogni "relazione" è un arco.

La maggiore e miglior fonte rimane Wikipedia ed anche le Notizie.

Il motore di ricerca lavora sia a livello sintattico che semantico, ogni nodo viene esaminato.

"Player Rank" - Disponibile pubblicamente (CNN + Genetic Algorithm?).

### Crittografia

Per esempio per aprire un lucchetto a combinazione il numero di tentativi è:
$$
10^n \\
n = cifre
$$
Questo porta ad una crescita esponenziale.

L'operazione permessa (inserire il numero) deve richiedere un tempo ragionevole.

L'operazione vietata (crackare il lucchetto) deve richiedere un tempo esponenziale.

Ogni codice segreto (la chiave) non dev'essere mantenuto troppo a lungo.

Frequence Analysis per "crackare" il cifrario di Cesare e di Vigenerè.

Il cifrario di Leon Battista Alberti prevede anche un cambiamento della chiave tramite uno "shift" dato dal numero.

#### Invertire una funzione (biunivoca)

$$
f(x) = c * x \\
g(x) = f(x) / c
$$

Invertire la moltiplicazione è facilmente invertibile.

Al contrario il quadrato in modulo non è facilmente invertibile in quanto bisogna provare tutti i possibili valori per la x.
$$
f(x) = x^2 * mod (n) \\
g(x) = \sqrt{f(x)} * mod (n) \\
$$

Learning:

* Michael Nilsen - Reti Neurali (ANN)
* Libro universitario di Pisa per il corso di Crittografia

