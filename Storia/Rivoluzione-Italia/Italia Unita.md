# Italia Unita

1861 - Unità d'Italia - Mazzini, Cavour e Garibaldi

Si forma l'Italia con la bandiera tricolore sotto Vittorio Emanuele II di Savoia.

## Guerre di indipendenza

1a Guerra d’Indipendenza – 1859 – Guerra Franco-Austriaca: Italia si allea con la Francia concedendo Nizza e Savoia in cambio della Lombardia.

2a Guerra d’Indipendenza – 1860 – Spedizione dei mille di Garibaldi, parte dalla Liguria, che concede poi il meridione al Regno di Savoia.

3a guerra d’Indipendenza – 1866 – Sconfitta Nissa e Custoza, mare e terra, Il Veneto viene annesso grazie al passaggio alla Prussia.