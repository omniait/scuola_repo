# Italo Svevo

## La coscienza di Zeno 1923

Importante è la parola "inetto" che vuol dire "qualcuno incapace di vivere".

Inettitudine e Malattia (deformazione della realtà dal punto di vista psicologico).

La società ha il suo peso, il ruolo dell'impiegato nel terziario è centrale.

Ci fa vedere una figura che si ricollega alle precedenti, essendo inetto il personaggio, ma allo stesso tempo è inattendibile e la realtà è pertanto filtrata dalla follia.

Il personaggio appartiene alla Ricca Borghesia Commerciale, ambiente di Svevo.

Il Narratore è Zeno Cosini, personaggio, protagonista e tutto è dal suo punto di vista.

### Psicoanalisi

Nella premessa c'è un dottore analista che ha dato come obiettivo al paziente di scrivere un'autobiografia.

Svevo viene a contatto con la Psicanalisi a causa di un suo parente che non è funzionata.

Zeno interrompe la Terapia, mette in dubbio la psicanalisi.

Lo stesso Svevo è incerto sulla psicanalisi dovuta proprio alla sua esperienza personale.

### Finale

La coscienza di Zeno finisce con una profezia dell'Apocalisse, spezza completamente rispetto a tutto il libro e si parla di malattia anche civile. 

