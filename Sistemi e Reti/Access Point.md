# Firma e Certificato Digitale

Cifrare un messaggio è un processo spesso lento e pesante e spesso non serve la segretezza ma basta l'autenticazione e la certezza che il messaggio non venga modificato, per esempio le pagine web.

La firma digitale si basa sulla crittografia asimmetrica e consente:

* La sottoscrizione di un documento
* La verifica, da parte del destinatario, dell'identità del mittente
* La certezza che l'informazione del documento non sia stata manomessa

La firma digitale si basa su un dispositivo rilasciato da enti certificatori che accertano l'identità del richiedente.

L'utente viene dotato anche di un PIN da usare.

Il risultato di un documento firmato è un file in formato p7m che consente di firmare qualunque documento informatico.

La Smart Card è il supporto più diffuso.

È fondamentale calcolare il Fingerprint con una funzione Hash, le più comuni sono l'MD5 e SHA.

Assieme al documento viene infatti spedito il fingerprint (cifrato) a fine messaggio col certificato del firmatario.

Il destinatario dovrà decifrare con la chiave pubblica di A e l'integrità si verifica tramite il Fingerprint.


# Access Point

## Configurazione

* SSID : Il nome che può essere mandato o meno in broadcast (ottenuto attraverso un frame periodico chiamato "Beacon") un AP ha almeno un SSID.
* Potenza: max 100mW - 2.4GHz; max 1W - 5GHz.
* Canale: Ovvero la banda di frequenza occupata, per il 2.4GHz sono da 1 a 13 canali(regola del 5).
* Crittografia: Il cifrario standard è simmetrico, specificato nella 802.11 IEEE e chiamato WEP, usa chiave a 10 cifre esadecimali (40 bit+24 bit) o da 26 cifre esadecimali (104 bit+24 bit) per la crittografia a 64, 128.
* Incapsulamento: Dipende dall'uso dell'AP, si può impostare il protocollo transport per i frame come: PPPoA (Point to Point over ATM) o PPPoE (Point to Point over Ethernet).
* NAT DHCP: In alcuni casi occorre attivare questi due servizi, DHCP per assegnare indirizzi IP dinamicamente ai client. NAT invece nasconde gli host interni non indirizzando a loro il traffico proveniente dall'esterno. Sono necessari solo se l'AP deve fare da router.

Esiste anche la modalità WLAN ad-hoc (MANET) dove una scheda di rete wireless 802.11 comunica con un'altra scheda creando così una rete IBSS in modalità Peer-to-Peer (P2P).

## Sicurezza nel Wireless

Data la natura delle reti wireless nascono problemi di sicurezza.

Esistono vari attacchi:

* Sniffing: Intercettazione dei pacchetti che transitano in una rete, il dispositivo software o hardware è chiamato Sniffer.
* Accessi non Autorizzati: Accesso previa esplicita autorizzazione, si può compiere attraverso degli RAP (Rogue Access Point) che riguarda anche le reti Wired, sono dispositivi fisici che possono realizzare una backdoor (è possibile accedere dall'esterno ad una rete interna). È nato anche il termine Wardriving che indica l'atto di intercettare le reti wireless insicure costruendo così una mappa con coordinate GPS di reti accessibili muovendosi in una determinata area.
* Spoofing: Indica la sostituzione del SID (secure identity) assegnato ad ogni utente che permette autorizzazioni speciali, sfruttando un Rogue Peer (O Wireless Terminal Rogue come nelle schede). Il Rogue Peer sfrutta il protocollo ARP mandando MAC e IP di un host all'AP. 
  Il protocollo SARP riesce a contrastare questi attacchi dato che fornisce un tunnel protetto tra ogni host e AP ma necessita dell'installazione di un software su ogni client.
* DoS (Denial Of Service): È un attacco che tenta di paralizzare i servizi offerti da una rete. A livello di wireless tenta di interrompere la connettività della rete per un periodo di tempo inviando tantissimi pacchetti ed è quindi un attacco Brute Force. 
  Un attacco DoS è facile da identificare a causa della facilità di identificazione della fonte e bloccarla. 
  È anche possibile cercare di inviare segnali radio forti che si sovrappongono a quelli dell'AP e del WT, questo è nello specifico un attacco "Jamming".


### WEP

WEP (Wired Equivalent Privacy) è una tecnica in cui è cifrato il Payload (I dati del frame) usando l'RC4 a livello MAC (Network), chiave lunga da 40 a 256 bit, essendo un cifrario a chiave simmetrica la chiave dev'essere identica per tutti gli host.

### TKIP (Temporal Key Integrity Protocol) o WEP2

Si tratta dell'evoluzione di WEP. Viene usata una chiave temporale a 128bit condivisa con l'indirizzo MAC del WT e aggiunge un vettore di inizializzazione a 128bit.

La chiave temporale è rigenerata ad ogni burst o ad ogni pacchetto il ché rende la connessione con WEP difficile da violare.

È possibile aggiornare gli AP tramite una patch del firmware al fine di supportare il WEP2.

### AES

Lo standard 802.11i include il cifrario AES molto più sicuro rispetto a WEP e WEP2. Lo svantaggio risiede nell'elaborazione molto più pesante rispetto ai precedenti cifrari e non tutti gli AP lo supportano.

### Autenticazione

Un metodo per risolvere i problemi di sicurezza è usare l'autenticazione degli utenti connessi. L'autenticazione però dev'essere bilaterale per rendere più difficile l'intrusione.

L'autenticazione si può applicare nei seguenti metodi:

* Filtro MAC (più sicuro ma vulnerale a spooping)
* Tramite SSID dell'AP (molta vulnerabilità a causa del broadcast)
* Protocollo EAP del standard 802.1x

#### EAP

Esiste un server di autenticazione, può essere RADIUS, il protocollo gira sia su wireless sia wired ed è implementato a livello Application (software specifico o OS).

L'autenticazione avviene tramite pacchetti EAP, usando un algoritmo specifico per verificare l'identità.

Ci sono 3 tipi di EAP:

* EAP TLS (Transport Layer Security)
* EAP TTLS (Tunneled Transport Layer Security)
* LEAP (Lightweight EAP CISCO)

